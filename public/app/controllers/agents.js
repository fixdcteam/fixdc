app.controller('agentsController', function($scope, $rootScope, $http, API_URL, $location, $stateParams, $state, services, Notification) {

    $('.loading-loader').hide();
    $('.error-loading-container').hide();

    if ($state.current.name == "agent-list") {
        $scope.agents = [];
        $scope.headerTitle = "Agent List";
        $scope.libraryTemp = {};
        $scope.totalItemsTemp = {};
        $scope.currentPage = 1;
        $scope.paramPageNumber = "?page=1";
        $scope.paramSearch = "";
        $scope.paramAgencyAdmin = "";
        $scope.totalItems = 0;

        if ($rootScope.isAgentAdmin != 1) {
            getResultsPage("?page=1");
        } else {
            $scope.paramAgencyAdmin = "&agent-user-id=" + $rootScope.currentUser;
            getResultsPage("?page=1" + $scope.paramAgencyAdmin);
        }

        $scope.pageChanged = function (newPage) {
            $scope.paramPageNumber = "?page=" + newPage;
            $scope.currentPage = newPage;
            var param = $scope.paramPageNumber + $scope.paramAgencyAdmin + $scope.paramSearch;
            getResultsPage(param);
        };

        $scope.searchDB = function () {
            if ($scope.searchText.length >= 1) {
                if ($.isEmptyObject($scope.libraryTemp)) {
                    $scope.libraryTemp = $scope.agents;
                    $scope.totalItemsTemp = $scope.totalItems;
                    $scope.agents = {};
                }
                $scope.paramSearch = "&search=" + $scope.searchText;
                var param = $scope.paramPageNumber + $scope.paramAgencyAdmin + $scope.paramSearch;
                getResultsPage(param);
            } else {
                if (!$.isEmptyObject($scope.libraryTemp)) {
                    $scope.agents = $scope.libraryTemp;
                    $scope.totalItems = $scope.totalItemsTemp;
                    $scope.libraryTemp = {};
                }
                $scope.paramSearch = "";
                var param = $scope.paramPageNumber + $scope.paramAgencyAdmin + $scope.paramSearch;
                getResultsPage(param);
            }
        }

        function getResultsPage(param) {
            $http.get(API_URL + 'agents' + param, {beforeSend: $('.loading-loader').show()})
                .success(function (data) {
                    $('.loading-loader').hide();
                    $scope.agents = data.data;
                    $scope.totalItems = data.total;
                })
                .error(function (response) {
                    $('.loading-loader').hide();
                    $('.error-loading-container').show();
                    console.log(response);
                });
        }
    }


    if ($state.current.name == "agent-edit" || $state.current.name == "agent-profile" ) {
        var url = API_URL + 'agents/';
        var field = "agent_id";
        if ($state.current.name == "agent-edit") {
            var agentID = ($stateParams.userID) ? parseInt($stateParams.userID) : 0;
            url += agentID;
        } else {
            var agentID = localStorage.getItem('user');
            url += agentID + '?field=user_id';
            var field = "user_id";
        }
        $http.get(url, {beforeSend : $('.loading-loader').show()})
            .success(function (response) {
                $('.loading-loader').hide();
                console.log(response);
                $scope.agent = response;
            })
            .error(function (response) {
                $('.loading-loader').hide();
                $('.error-loading-container').show();
                console.log(response);
            });

        //retrieve agency to fill select option
        var urlAgency = "";
        if ($rootScope.isAdminUser ==1 ) {
            urlAgency = "agencies?all=1";
            $http.get(API_URL + urlAgency)
                .success(function (response) {
                    $scope.agencyData = {
                        agencies: response,
                    };
                });
        } else {
            $http.get(API_URL + "agents/" + localStorage.getItem('user') + "?field=user_id")
                .success(function (response) { 
                    urlAgency = "agencies/" + response.agency_id;;
                    $http.get(API_URL + urlAgency)
                        .success(function (response) {
                            $scope.agencyData = {
                                agencies: {response},
                            };
                        });
                });
        }
    }

    $scope.go = function ( hash ) {
        $location.path( hash );
    };

    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.form_title = "Add New Agent";
                $scope.go('/agents/edit/0' );
                break;

            case 'edit':
                $scope.form_title = "Agent Details";
                $scope.id = id;
                $scope.go('/agents/edit/' + id );

                break;
            default:
                break;
        }
    }

    //save new record / update existing record
    $scope.save = function() {
        var url = API_URL + 'agents';

        if (agentID != '0') {
            url += '/' + agentID;
            if (field == "user_id") {
                $scope.agent.field = 'user_id';
            }
        }

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.agent),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            beforeSend: $('.loading_loader').show()
        }).success(function(response) {
            if ($state.current.name != "agent-profile") {
                if (response.status !== null && response.status == "success") {
                    Notification({message: response.message, title: 'Success'});
                    $state.reload();
                } else {
                    Notification.error({message: response.message, delay: 5000});
                }
            } else {
                Notification({message: response.message, title: 'Success'});
                $state.reload();
            }
            $('.loading-loader').hide();
            console.log(response);
        }).error(function(response) {
            $('.loading-loader').hide();
            console.log(response);
            Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});
        });
    }

    //delete record
    $scope.confirmDelete = function(id, val) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'agents/' + id
            }).
            success(function(response) {
                if (response.status !== null && response.status == "success") {
                    Notification({message: response.message, title: 'Success'});
                    $state.reload();
                } else {
                    Notification.error({message: response.message, delay: 5000});
                }
                console.log(response);
            }).
            error(function(response) {
                console.log(response);
                Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});
            });
        } else {
            return false;
        }
    }

    //deactivate record
    $scope.confirmDeactivate = function(id, active) {

        var confirmMsg = '';
        if (active) {
            confirmMsg = 'Are you sure you want to deactivate this record?';
        } else {
            confirmMsg = 'Are you sure you want to activate this record?';
        }
        var isConfirmDeactivate = confirm(confirmMsg + ' ');
        if (isConfirmDeactivate) {
            var val = {
                "id":id ,
                "deactivate":active
            };

            $http({
                method: 'POST',
                url: API_URL + 'agents/deactivate/' + id,
                data: val,
                headers: {'Content-Type': 'application/json'}

            }).
            success(function(response) {
                if (response.status !== null && response.status == "success") {
                    Notification({message: response.message, title: 'Success'});
                    $state.reload();
                } else {
                    Notification.error({message: response.message, delay: 5000});
                }
                console.log(response);
            }).
            error(function(response) {
                console.log(response);
                Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});
            });
        } else {
            return false;
        }
    }

    //make agent an admin to agency
    $scope.makeAdmin = function(id, isAdmin) {

        var confirmMsg = '';
        var makeAdmin = !(isAdmin);
        if (makeAdmin) {
            confirmMsg = 'Are you sure you want to make this agent an admin to agency?';
        } else {
            confirmMsg = 'Are you sure you want to remove the admin rights of this agent?';
        }
        var isMakeAdmin = confirm(confirmMsg + ' ');
        if (isMakeAdmin) {
            var val = {
                "id":id ,
                "makeAdmin":makeAdmin
            };
            $http({
                method: 'POST',
                url: API_URL + 'agents/make-admin/' + id,
                data: val,
                headers: {'Content-Type': 'application/json'}
            }).
            success(function(response) {
                if (response.status !== null && response.status == "success") {
                    Notification({message: response.message, title: 'Success'});
                    $state.reload();
                } else {
                    Notification.error({message: response.message, delay: 5000});
                }
                console.log(response);
            }).
            error(function(response) {
                console.log(response);
                Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});
            });
        } else {
            return false;
        }
    }

});