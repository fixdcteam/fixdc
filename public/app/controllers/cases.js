app.controller('casesController', function($scope, $rootScope, $http, API_URL, $location, $stateParams, $state, $route, services, uiGmapGoogleMapApi, Notification) {

    $('.loading-loader').hide();
    $('.error-loading-container').hide();
	$scope.headerTitle = "FixDC Reports";
    $scope.cases = [];
    $scope.case = {};
    $scope.cat = {};
    $scope.paramPageNumber = "?page=1";
	$scope.paramStatus = "";
	$scope.paramPrivacy = "";
	$scope.paramAgency = "";
	$scope.paramCitizen = "";
    $scope.libraryTemp = {};
    $scope.totalItemsTemp = {};
    $scope.totalItems = 0;
	$scope.map = {};
	$scope.catData = "";
    $scope.catForUpdate = "";
    $scope.agencyBoxDisabled = false;
    $scope.openReportsCount = 0;
    $scope.paramAgencyCount = '0';
	
	if ($state.current.name == "citizen-reports" ) { 
		var citizenName = ($stateParams.citizenName);
		$scope.headerTitle = "FixDC Reports by " + citizenName.replace("-", " ");
		$scope.paramCitizen = "&citizen-id=" + ($stateParams.userID) ;
	}

    if ($state.current.name == "case-list" || $state.current.name == "citizen-reports") {

        if (localStorage.getItem('4u7h_735b')) {
            $scope.paramAgency = "&agency-id=" + localStorage.getItem('4u7h_735b');
            $scope.paramAgencyCount = localStorage.getItem('4u7h_735b');
            getResultsPage("?page=1" + $scope.paramAgency + $scope.paramCitizen);

        } else {
            getResultsPage("?page=1" + $scope.paramCitizen);
        }
        

        // $http.get(API_URL + 'authenticate/get-user-type/' + $rootScope.currentUser)
        // .success(function(response) {
        //     if (response.user_type == 'agent') {
        //         $http.get(API_URL + 'agents/get-agent-agency-id/' + $rootScope.currentUser)
        //         .success(function(response) {
        //             $scope.paramAgency = '&agency_id=' + response.agency_id;
        //             $scope.paramAgencyCount = /*'?agency_id=' +*/ response.agency_id;
        //             getResultsPage("?page=1" + $scope.paramAgency + $scope.paramCitizen);
        //         });
        //     } else {
        //         getResultsPage("?page=1" + $scope.paramCitizen);
        //     }
        // });

        $scope.pageChanged = function(newPage) {
            $scope.paramPageNumber = "?page=" + newPage;
            alert("")
            var param = $scope.paramPageNumber + $scope.paramStatus + $scope.paramPrivacy + $scope.paramAgency + $scope.catData.model + $scope.paramCitizen;
            getResultsPage(param);
        };
        
        $scope.applyFilter = function() {
            $scope.paramPageNumber = "?page=1";
            var param = $scope.paramPageNumber + $scope.paramStatus + $scope.paramPrivacy + $scope.paramAgency + $scope.catData.model + $scope.paramCitizen;
            getResultsPage(param);
        };

        $http.get(API_URL + 'reportcases/get-reports-count/' + $scope.paramAgencyCount)
            .success(function(data) {
                $scope.openReportsCount = data[0].case_count;
                //alert("lol");
            })
            .error(function(response) {
                console.log(response);
            });

        function getResultsPage(param) {
            if(! $.isEmptyObject($scope.libraryTemp)){
                /*dataFactory.httpRequest('/items?search='+$scope.searchText+'&page='+pageNumber).then(function(data) {
                 $scope.data = data.data;
                 $scope.totalItems = data.total;
                 }); */
            } else {
                $http.get(API_URL + 'reportcases'+param, {beforeSend : $('.loading-loader').show()})
                .success(function(data) {
                    $('.loading-loader').hide();
                    $scope.cases = data.data;
                    $scope.totalItems = data.total;
                    //alert("lol");
                })
                .error(function(response) {
                    $('.loading-loader').hide();
                    $('.error-loading-container').show();
                    console.log(response);
                });
            }
        }

    }
	

	if ($state.current.name == "case-edit" ) { 

		var caseID = ($stateParams.userID) ? parseInt($stateParams.userID) : 0;
		$http.get(API_URL + 'reportcases/' + caseID)
			.success(function(response) {
				$('.loading-loader').hide();
				//console.log(response);
                if (caseID != 0) {
                    $scope.case = response;
                } else {
                    $scope.case.citizen_id = '7';
                    $scope.case.case_address = 'Davao City';
                    $scope.case.case_loc_long = '0.000000000';
                    $scope.case.case_loc_lat = '0.000000000';
                    $scope.case.case_isPrivate = 0;
                    $scope.case.case_noOfConcerns = 0;
                    $scope.case.case_status = 1;
                }

				for (index = 0; index < $scope.case.images.length; ++index) {
					$scope.case.images[index].url = $scope.case.images[index].img_directory + $scope.case.images[index].img_name + "." + $scope.case.images[index].img_ext;
					$scope.case.images[index].thumbUrl = $scope.case.images[index].url;
                    $scope.case.images[index].img_description = $scope.case.images[index].img_description;
					$scope.case.images[index].caption = "lol";
				}
				
				$scope.map = { center: { latitude: $scope.case.case_loc_lat /*geocoded.lat*/, longitude: $scope.case.case_loc_long }, zoom: 19, options: {                    
						streetViewControl: false,
						mapTypeControl: false,
						scaleControl: false,
						rotateControl: false,
						zoomControl: false
					}
				}; 
				$scope.coordsUpdates = 0;
				$scope.dynamicMoveCtr = 0;
				
				$scope.marker = {
				  id: 0,
				  coords: {
					latitude: $scope.case.case_loc_lat, //geocoded.lat,
					longitude: $scope.case.case_loc_long //geocoded.lng
				  },
				  options: { draggable: true },
				  events: {
					dragend: function (marker, eventName, args) {
					  $log.log('marker dragend');
					  var lat = marker.getPosition().lat();
					  var lon = marker.getPosition().lng();
					  $log.log(lat);
					  $log.log(lon);

					  $scope.marker.options = {
						draggable: true,
						labelContent: "lat: " + $scope.marker.coords.latitude + ' ' + 'lon: ' + $scope.marker.coords.longitude,
						labelAnchor: "100 0",
						labelClass: "marker-labels"
					  };
					}
				  }
				};
				console.log($scope.marker);
				uiGmapGoogleMapApi.then(function(maps) {
					console.log($scope.map);
				});
			})
			.error(function(response) {
				$('.loading-loader').hide();
				$('.error-loading-container').show();
				console.log(response);
			}); 
            
		$scope.openImageViewer = function (index) {
			$('#imageviewer').modal('show');
			$scope.imageModalName = $scope.case.images[index].img_description;
			$scope.imageModalSrc = $scope.case.images[index].url;
		};

	}

    if ($state.current.name == "case-edit" ||  $state.current.name == "manage-categories") { 
        urlAgency = "agencies?all=1";
        $http.get(API_URL + urlAgency)
            .success(function (response) {
                $scope.agencyData = {
                    agencies: response,
                };
            });

        if (localStorage.getItem('4u7h_735t') != 1) {
            $scope.agencyBoxDisabled = true;
        }
    }

    var catParams = '';
    if ($rootScope.agencyID != null ) {
        catParams = '?agency-id=' + $rootScope.agencyID;
    }

	$http.get(API_URL + 'categories' + catParams)
		.success(function (response) {
            if ($state.current.name == "case-edit" || $state.current.name == "manage-categories" ) {
                $scope.categoryData = {
                    categories: response,
                };
            } else {
                $scope.catData = {
                    model : "",
                    categories: response,
                };
            }
		});

    $scope.dateObject = "";
    $scope.dateString = function(date_string) {
        $scope.dateObject =  date_string.replace(" ", "T");
    }
	
	$scope.go = function ( hash ) {
        $location.path( hash );
    };

    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.form_title = "Add New Case";
                $scope.go('/cases/edit/0' );
                break;
            case 'edit':
                $scope.form_title = "Report Case Detail";
                $scope.id = id;
                $scope.go('/cases/edit/' + id );
                break;
            default:
                break;
        }
    }

    //save new record / update existing record
    $scope.save = function() {
        var url = API_URL + "reportcases";

        //append admins id to the URL if the form is in edit mode
        if (caseID != '0'){
            url += "/" + caseID;
        }

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.case),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            beforeSend: $('.loading_loader').show()
        }).success(function(response) {
            $('.loading-loader').hide();
            if (response.status !== null && response.status == "success") {
                Notification({message: response.message, title: 'Success'});
                $state.reload();
            } else {
                Notification.error({message: response.message, delay: 5000});
            }
            console.log(response);
        }).error(function(response) {
            console.log(response);
            Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});
        });
    }

    //delete record
    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'reportcases/' + id
            }).
            success(function(response) {
                if (response.status !== null && response.status == "success") {
                    Notification({message: response.message, title: 'Success'});
                    $state.reload();
                } else {
                    Notification.error({message: response.message, delay: 5000});
                }
                console.log(data);
            }).
            error(function(data) {
                console.log(data);
                Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});
            });
        } else {
            return false;
        }
    }

    //setstatus cases
    $scope.setStatus = function(id, status) {
        var isConfirmMsg = '';
        var alertMsg = '';
		
        if (status == 3) {
            isConfirmMsg = 'Are you sure you want to mark this case as resolved?';
            alertMsg = 'Unable to mark as resolved.';
        } else if (status == 1){
            isConfirmMsg = 'Are you sure you want to reopen this case?';
            alertMsg = 'Unable to reopen the case.';
        } else if (status == 4){
            isConfirmMsg = 'Are you sure you want to mark this case as in-progress?';
            alertMsg = 'Unable to mark as in-progress.';
        } else {
            isConfirmMsg = 'Are you sure you want to mark this case invalid?';
            alertMsg = 'Unable to mark this case as invalid.';
        }
        var confirmAction = confirm(isConfirmMsg + '');
        if (confirmAction) {
            var val = {
                "id":id ,
                "setStatus":status
            };
            $http({
                method: 'POST',
                url: API_URL + 'reportcases/setStatus/' + id,
                data: val,
                headers: {'Content-Type': 'application/json'}
            }).
            success(function(response) {
                if (response.status !== null && response.status == "success") {
                    Notification({message: response.message, title: 'Success'});
                    $state.reload();
                } else {
                    Notification.error({message: response.message, delay: 5000});
                }
                console.log(response);
            }).
            error(function(response) {
                console.log(response);
                Notification.error({message: alertMsg, delay: 5000});
            });
        } else {
            return false;
        }
    }

    //setIsPrivate case
    $scope.setIsPrivate = function(id, isPrivate) {
        var isConfirmMsg = '';
        var alertMsg = '';
        var isPrivate = !(isPrivate);
        if (isPrivate) {
            isConfirmMsg = 'Are you sure you want to make this case private?';
            alertMsg = 'Unable to mark as resolved.';
        } else {
            isConfirmMsg = 'Are you sure you want to make this case public?';
            alertMsg = 'Unable to reopen the case.';
        }
        var confirmAction = confirm(isConfirmMsg + '');
        if (confirmAction) {
            var val = {
                "id":id ,
                "setIsPrivate":isPrivate
            };
            $http({
                method: 'POST',
                url: API_URL + 'reportcases/setIsPrivate/' + id,
                data: val,
                headers: {'Content-Type': 'application/json'}
            }).
            success(function(response) {
                if (response.status !== null && response.status == "success") {
                    Notification({message: response.message, title: 'Success'});
                    $state.reload();
                } else {
                    Notification.error({message: alertMsg + 'Please refresh the page and try again.', delay: 5000});
                }
                console.log(response);
            }).
            error(function(data) {
                console.log(data);
                Notification.error({message: alertMsg + 'Please refresh the page and try again.', delay: 5000});
            });
        } else {
            return false;
        }
    }

    $scope.saveCategory = function() {
        var url = API_URL + "categories";
        var catParams = '';
        if ($rootScope.agencyID != null ) {
           // url += '?agency-id=' + $rootScope.agencyID;
           $scope.cat['agency-id'] = $rootScope.agencyID;
        }

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.cat),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            beforeSend: $('.loading_loader').show()
        }).success(function(response) {
            if (response.status !== null && response.status == "success") {
                $('.loading-loader').hide();
                Notification({message: response.message, title: 'Success'});
                $state.reload();
            } else {
                 Notification.error({message: response.message, delay: 5000});
            }  
        }).error(function(response) {
            console.log(response);
            Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});
        });
    }

    $scope.updateCategoryModal = function(cat) {
        $scope.catForUpdate = cat;
        $('#updateCategoryModal').modal('show');
        $scope.modal_title = "Update Category Record";
    }

    $scope.updateCategory = function() {
        var url = API_URL + "categories/" + $scope.catForUpdate.cat_id;

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.catForUpdate),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            beforeSend: $('.loading_loader').show()
        }).success(function(response) {
            $('#updateCategoryModal').modal('hide');
            if (response.status !== null && response.status == "success") {
                $('.loading-loader').hide();
                Notification({message: response.message, title: 'Success'});
                $state.reload();
            } else {
                Notification.error({message: response.message, delay: 5000});
            }  
        }).error(function(response) {
            console.log(response);
            Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});
        });
    }

    //delete record
    $scope.confirmDeleteCategory = function(id) {
        var isConfirmDelete = confirm('Are you sure you want this category?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'categories/' + id
            }).
            success(function(response) {
                if (response.status !== null && response.status == "success") {
                    $('.loading-loader').hide();
                    Notification({message: response.message, title: 'Success'});
                    $state.reload();
                } else {
                    Notification.error({message: response.message, delay: 5000});
                }  
            }).
            error(function(data) {
                console.log(data);
                Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});
            });
        } else {
            return false;
        }
    }

    //upload image
    $scope.uploadImage = function(files, id) {
        var fd = new FormData();
        //Take the first selected file
        fd.append("file", files[0]);

        $http.post(API_URL + "reportcases/upload-image/" + id + "?admin-upload=1", fd, {
          withCredentials: true,
          headers: {'Content-Type': undefined },
          transformRequest: angular.identity
        }).
        success(function(response) {
            if (response.status !== null && response.status == "success") {
                $('.loading-loader').hide();
                Notification({message: response.message, title: 'Success'});
                $state.reload();
            } else {
                Notification.error({message: response.message, delay: 5000});
            }  
        }).
        error(function(data) {
            console.log(data);
            Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});
        });
    }

    $scope.pageChanged = function(newPage) {
        $scope.paramPageNumber = "?page=" + newPage;
        var param = $scope.paramPageNumber + $scope.paramStatus + $scope.paramPrivacy + $scope.paramAgency + $scope.catData.model + $scope.paramCitizen;
        getResultsPage(param);
    };
});