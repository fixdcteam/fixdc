app.controller('citizensController', function($scope, $rootScope, $http, API_URL, $location, $stateParams, $state, services, Notification) {

    $('.loading-loader').hide();
    $('.error-loading-container').hide();

    if ($state.current.name == "citizen-list") {
        $scope.headerTitle = "Registered Citizens";
        $scope.citizens = [];
        $scope.libraryTemp = {};
        $scope.totalItemsTemp = {};
        $scope.currentPage = 1;
		$scope.paramPageNumber = "?page=1";
		$scope.paramStatus = "";
		$scope.paramWithInvalid = "";
		$scope.paramBlocked = "";
		$scope.paramSearch = "";
        $scope.totalItems = 0;
		
		getResultsPage("?page=1");
		
		$scope.pageChanged = function(newPage) {
			$scope.paramPageNumber = "?page=" + newPage;
			$scope.currentPage = newPage;
			var param = $scope.paramPageNumber + $scope.paramStatus + $scope.paramWithInvalid + $scope.paramBlocked + $scope.paramSearch;
			getResultsPage(param);
		};
		
		$scope.applyFilter = function() {
			$scope.paramPageNumber = "?page=1";
			var param = $scope.paramPageNumber + $scope.paramStatus + $scope.paramWithInvalid + $scope.paramBlocked + $scope.paramSearch;
			getResultsPage(param);
		};
  
        function getResultsPage(param) {
			$http.get(API_URL + 'citizens' + param, {beforeSend: $('.loading-loader').show()})
				.success(function (data) {
					$('.loading-loader').hide();
					$scope.citizens = data.data;
					$scope.totalItems = data.total;
				})
				.error(function (response) {
					$('.loading-loader').hide();
					$('.error-loading-container').show();
					console.log(response);
				});
        }

        $scope.searchDB = function () {
            if ($scope.searchText.length >= 1) {
                if ($.isEmptyObject($scope.libraryTemp)) {
                    $scope.libraryTemp = $scope.citizens;
                    $scope.totalItemsTemp = $scope.totalItems;
                    $scope.citizens = {};
                }
                $scope.paramSearch = "&search=" + $scope.searchText;
                var param = $scope.paramPageNumber + $scope.paramStatus + $scope.paramWithInvalid + $scope.paramBlocked + $scope.paramSearch;
                getResultsPage(param);
            } else {
                if (!$.isEmptyObject($scope.libraryTemp)) {
                    $scope.citizens = $scope.libraryTemp;
                    $scope.totalItems = $scope.totalItemsTemp;
                    $scope.libraryTemp = {};
                }
                $scope.paramSearch = "";
                var param = $scope.paramPageNumber + $scope.paramStatus + $scope.paramWithInvalid + $scope.paramBlocked + $scope.paramSearch;
                getResultsPage(param);
            }
        }
    }

    if ($state.current.name == "citizen-edit") {
        var citizenID = $stateParams.userID; // ? parseInt($stateParams.userID) : 0;
        $http.get(API_URL + 'citizens/' + citizenID)
            .success(function (response) {
                $('.loading-loader').hide();
                console.log(response);
                $scope.citizen = response;
                $scope.citizen.citizen_DOB = new Date($scope.citizen.citizen_DOB);
            })
            .error(function (response) {
                $('.loading-loader').hide();
                $('.error-loading-container').show();
                console.log(response);
            });
    }

    $scope.dateObject = "";
    $scope.dateString = function(date_string) {
        $scope.dateObject =  date_string.replace(" ", "T");
    }

    $scope.go = function ( hash ) {
        $location.path( hash );
    };
    
    $scope.showSendNoteModal = function () {
        $('#SendNoteModal').modal('show');
        $scope.modal_title = Send ;
        $scope.modal_content = response.modal_content;
    };

    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.form_title = "Add New Citizen";
                $scope.go('/citizens/edit/0' );
                break;

            case 'edit':
                $scope.form_title = "Citizen Detail";
                $scope.id = id;
                $scope.go('/citizens/edit/' + id );

                break;
            default:
                break;
        }
    }

    //save new record / update existing record
    $scope.save = function() {
        var url = API_URL + 'citizens';

        //append citizen id to the URL if the form is in edit mode
        if (citizenID != '0'){
            url += '/' + citizenID;
        }

        var month = $scope.citizen.citizen_DOB.getMonth() + 1;
        var day = $scope.citizen.citizen_DOB.getDate();
        var year = $scope.citizen.citizen_DOB.getFullYear();
        $scope.citizen.citizen_DOB = year + '-' + month + '-' + day;

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.citizen),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            beforeSend: $('.loading_loader').show()
        }).success(function(response) {
            if (response.status !== null && response.status == "success") {
                Notification({message: response.message, title: 'Success'});
                $state.reload();
            } else {
                Notification.error({message: response.message, delay: 5000});
            }
            console.log(response);
            $('.loading-loader').hide();
        }).error(function(response) {
            console.log(response);
            Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});
        });
    }

    //delete record
    $scope.confirmDelete = function(id, val) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'citizens/' + id
            }).
            success(function(response) {
                if (response.status !== null && response.status == "success") {
                    Notification({message: response.message, title: 'Success'});
                    $state.reload();
                } else {
                    Notification.error({message: response.message, delay: 5000});
                }
                console.log(response);
            }).
            error(function(response) {
                console.log(response);
                Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});
            });
        } else {
            return false;
        }
    }

    //deactivate record
    $scope.confirmDeactivate = function(id, active) {

        var confirmMsg = '';
        if (active) {
            confirmMsg = 'Are you sure you want to deactivate this record?';
        } else {
            confirmMsg = 'Are you sure you want to activate this record?';
        }
        var isConfirmDeactivate = confirm(confirmMsg + ' ');
        if (isConfirmDeactivate) {
            var val = {
                "id":id ,
                "deactivate":active
            };

            $http({
                method: 'POST',
                url: API_URL + 'citizens/deactivate/' + val,
                data: val,
                headers: {'Content-Type': 'application/json'}
            }).
            success(function(response) {
                if (response.status !== null && response.status == "success") {
                    Notification({message: response.message, title: 'Success'});
                    $state.reload();
                } else {
                    Notification.error({message: response.message, delay: 5000});
                }
                console.log(response);
            }).
            error(function(response) {
                console.log(response);
                Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});
            });
        } else {
            return false;
        }
    }

    //block citizen record.
    $scope.confirmBlocked = function(id, blocked) {

        var confirmMsg = '';
        var block = !(blocked);
        if (block) {
            confirmMsg = 'Are you sure you want to block this record?';
        } else {
            confirmMsg = 'Are you sure you want to unblock this record?';
        }

        var isConfirmDeactivate = confirm(confirmMsg + ' ');
        if (isConfirmDeactivate) {
            var val = {
                "id":id ,
                "block":block
            };
            $http({
                method: 'POST',
                url: API_URL + 'citizens/block/' + id,
                data: val,
                headers: {'Content-Type': 'application/json'}
            }).
            success(function(response) {
                if (response.status !== null && response.status == "success") {
                    Notification({message: response.message, title: 'Success'});
                    $state.reload();
                } else {
                    Notification.error({message: response.message, delay: 5000});
                }
                console.log(response);
            }).
            error(function(response) {
                console.log(response);
                Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});
            });
        } else {
            return false;
        }
    }

    $scope.sendNote = function (id) {
        $http({
            method: 'POST',
            url: API_URL + 'citizens/send-note/' + id,
            headers: {'Content-Type': 'application/json'},
            beforeSend: $('.loading_loader').show()
        }).
        success(function(response) {
            if (response.status !== null && response.status == "success") {
                Notification({message: response.message, title: 'Success'});
            } else {
                Notification.error({message: response.message, delay: 5000});
            }
            console.log(response);
            $('.loading-loader').hide();
        }).
        error(function(response) {
            console.log(response);
            $('.loading-loader').hide();
            Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});
        });
    };



});