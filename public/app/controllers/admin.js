app.controller('adminController', function($scope, $rootScope, $http, API_URL, $location, $stateParams, $state, services, Notification) {

    $('.loading-loader').hide();
    $('.error-loading-container').hide();

    if ($state.current.name == "admin-list") {
        $scope.headerTitle = "Admin List";
        $scope.admins = [];
        $scope.libraryTemp = {};
        $scope.totalItemsTemp = {};
        $scope.newForm = 0;
        $scope.currentPage = 1;

        $scope.totalItems = 0;
        $scope.pageChanged = function (newPage) {
            getResultsPage(newPage);
        };

        getResultsPage(1);
        function getResultsPage(pageNumber) {
            $scope.currentPage = pageNumber;
            if (!$.isEmptyObject($scope.libraryTemp)) {
                $http.get(API_URL + 'admins?search=' + $scope.searchText + '&page=' + pageNumber)
                    .success(function (data) {
                        $('.loading-loader').hide();
                        $scope.admins = data.data;
                        $scope.totalItems = data.total;
                    })
                    .error(function (response) {
                        $('.loading-loader').hide();
                        $('.error-loading-container').show();
                        console.log(response);
                    });
            } else {
                $http.get(API_URL + 'admins?page=' + pageNumber, {beforeSend: $('.loading-loader').show()})
                    .success(function (data) {
                        $('.loading-loader').hide();
                        $scope.admins = data.data;
                        $scope.totalItems = data.total;
                        //alert("lol");
                    })
                    .error(function (response) {
                        $('.loading-loader').hide();
                        $('.error-loading-container').show();
                        console.log(response);
                        //alert('This is embarassing. An error has occured. Please check the log for details');
                        //alert(response);
                    });
            }
        }

        $scope.searchDB = function () {
            if ($scope.searchText.length >= 3) {
                if ($.isEmptyObject($scope.libraryTemp)) {
                    $scope.libraryTemp = $scope.admins;
                    $scope.totalItemsTemp = $scope.totalItems;
                    $scope.admins = {};
                }
                getResultsPage(1);
            } else {
                if (!$.isEmptyObject($scope.libraryTemp)) {
                    $scope.admins = $scope.libraryTemp;
                    $scope.totalItems = $scope.totalItemsTemp;
                    $scope.libraryTemp = {};
                }
                //getResultsPage(1);
            }
        }
    }

    //retrieve admins listing from API
    /*
    $http.get(API_URL + "admins")
        .success(function(response) {
            $scope.admins = response;
        });
    */
    if ($state.current.name == "admin-edit" || $state.current.name == "admin-profile" ) {
        var url = API_URL + 'admins/';
        var field = "id";
        if ($state.current.name == "admin-edit") {
            var adminID = ($stateParams.userID) ? parseInt($stateParams.userID) : 0;
            url += adminID;
        } else {
            var adminID = localStorage.getItem('user');
            url += adminID + '?field=user_id';
            var field = "user_id";
        }
        if (adminID == 0) {
            $scope.newForm = 1;
        } else {
            $scope.newForm = 0;
        }
        $http.get(url, {beforeSend : $('.loading-loader').show()})
            .success(function(response) {
                $('.loading-loader').hide();
                $scope.admin = response;

            })
            .error(function(response) {
                $('.loading-loader').hide();
                $('.error-loading-container').show();
                console.log(response);
            });
    }

    $scope.go = function ( hash ) {
        $location.path( hash );
    };

    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.form_title = "Add New Admin";
                $scope.go('/admins/edit/0' );
                break;

            case 'edit':
                $scope.form_title = "Admin Detail";
                $scope.id = id;
                $scope.go('/admins/edit/' + id );

                //$location.path("/employees/edit/" + id);
                break;
            default:
                break;
        }
    }

    //save new record / update existing record
    $scope.save = function() {
        var url = API_URL + "admins";

        //append admins id to the URL if the form is in edit mode
        if (adminID != '0'){
            url += "/" + adminID;
            if (field == "user_id") {
                $scope.admin.field = 'user_id';
            }
        }

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.admin),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            beforeSend: $('.loading-loader').show()
        }).success(function(response) {
            if ($state.current.name != "admin-profile") {
                if (response.status !== null && response.status == "success") {
                    Notification({message: response.message, title: 'Success'});
                    $state.reload();
                } else {
                    Notification.error({message: response.message, delay: 5000});
                }
            } else {
                Notification({message: 'Profile successfully updated.', title: 'Success'});
                $state.reload();
            }
            $('.loading-loader').hide();
            console.log(response);
        }).error(function(response) {
            $('.loading-loader').hide();
            console.log(response);
            Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});

        });
    }

    //delete record
    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'admins/' + id
            }).
            success(function(response) {
                if (response.status !== null && response.status == "success") {
                    Notification({message: response.message, title: 'Success'});
                    $state.reload();
                } else {
                    Notification.error({message: response.message, delay: 5000});
                }
                console.log(response);
            }).
            error(function(data) {
                console.log(response);
                Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});
            });
        } else {
            return false;
        }
    }

    //deactivate admin record
    $scope.confirmDeactivate = function(id, active) {
        var isconfirmDeactivateMsg = '';
        var alertMsg = '';
        if (active) {
            isconfirmDeactivateMsg = 'Are you sure you want to deactivate this record?';
            alertMsg = 'Unable to deactivate';
        } else {
            isconfirmDeactivateMsg = 'Are you sure you want to activate this record?';
            alertMsg = 'Unable to activate';
        }
        var confirmAction = confirm(isconfirmDeactivateMsg + '');
        if (confirmAction) {
            var val = {
                "id":id ,
                "deactivate":active
            };
            $http({
                method: 'POST',
                url: API_URL + 'admins/deactivate/' + id,
                data: val,
                headers: {'Content-Type': 'application/json'}
            }).
            success(function(response) {
                if (response.status !== null && response.status == "success") {
                    Notification({message: response.message, title: 'Success'});
                    $state.reload();
                } else {
                    Notification.error({message: response.message, delay: 5000});
                }
                console.log(response);
            }).
            error(function(response) {
                console.log(response);
                Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});
            });
        } else {
            return false;
        }
    }
});