app.controller('employeesController', function($scope, $rootScope, $http, API_URL, $location, $routeParams, services, $timeout, uiGmapGoogleMapApi) {

    $scope.latestReportCount = {};
    $scope.latestCitizenSignups = {};
    $scope.latestUsersSignup = {};
    $scope.paramAgency = "";

    if (localStorage.getItem('4u7h_735b')) {
        $scope.paramAgency = "?agency-id=" + localStorage.getItem('4u7h_735b');
    }

	$http.get(API_URL + "home" + $scope.paramAgency)
        .success(function(response) {
            $scope.cases = response.case_report_list;
            $scope.caseStatus = response.case;
            $scope.caseTotal = response.total_cases;
            $scope.unResolvedCases =  $scope.caseTotal[0].case_count - $scope.caseStatus[1].case_count;
            $scope.citizenTotal = response.total_citizens;
            $scope.reportIssues = response.report_issues;
            $scope.recentCitizens = response.recent_citizens;

            $scope.latestReportCount = {
                chart: {
                    caption: "Reported Cases",
                    subCaption: "Cases reported for the last 15 days",
                    decimals: "0",
                    forcedecimals: "0",
                    placeValuesInside: "0",
                    yAxisValueDecimals: "0",
                    forceYAxisValueDecimals: "0",
                    adjustDiv: "0",
                    numdivlines: "3",
                    yAxisMinValue: "10",
                    theme: "ocean"
                },
                data: response.reports_count
            }

            $scope.latestCitizenSignups = {
                chart: {
                    caption: "Citizen Signups",
                    subCaption: "Citizen signups for the last 15 days",
                    decimals: "0",
                    forcedecimals: "0",
                    placeValuesInside: "0",
                    yAxisValueDecimals: "0",
                    forceYAxisValueDecimals: "0",
                    adjustDiv: "0",
                    numdivlines: "3",
                    yAxisMinValue: "10",
                    theme: "ocean"
                },
                data: response.citizens_count
            }

            $scope.latestUsersSignup = {
                chart: {
                    caption: "User Signups",
                    subCaption: "User signups for the last 15 days",
                    decimals: "0",
                    forcedecimals: "0",
                    placeValuesInside: "0",
                    yAxisValueDecimals: "0",
                    forceYAxisValueDecimals: "0",
                    adjustDiv: "0",
                    numdivlines: "3",
                    yAxisMinValue: "10",
                    theme: "ocean"
                },
                data: response.users_count
            }

            $scope.markers = [];

            $scope.map = { center: { latitude: '7.07', longitude: '125.6' }, zoom: 13, options: {                  
                    streetViewControl: true,
                    mapTypeControl: false,
                    scaleControl: false,
                    rotateControl: false,
                    zoomControl: false, 
                    markers: {
                        selected: {}
                    },
                    cluster: {
                        minimumClusterSize : 3,
                        zoomOnClick: true,
                        averageCenter: true,
                        clusterClass: 'cluster-icon'
                    }
                }
            }; 

            $scope.coordsUpdates = 0;
            $scope.dynamicMoveCtr = 0;

            var infoWindow = new google.maps.InfoWindow;
            //var infowincontent = [];

            for (index = 0; index < $scope.cases.length; ++index) {
                //infowincontent.push(index);
                var infowincontent = document.createElement('div');
                var strong = document.createElement('strong');
                strong.textContent = $scope.cases[index].case_name;
                infowincontent.appendChild(strong);
                infowincontent.appendChild(document.createElement('br'));

                var text = document.createElement('text');
                text.textContent = $scope.cases[index].case_address;
                infowincontent  .appendChild(text);

                $scope.markers.push({
                  id: index,
                  coords: {
                    latitude: $scope.cases[index].case_loc_lat, //geocoded.lat,
                    longitude: $scope.cases[index].case_loc_long //geocoded.lng
                  },
                  options: { draggable: true },
                  events: {
                    dragend: function (marker, eventName, args) {
                      console.log('marker dragend ' + index);
                      var lat = marker.getPosition().lat();
                      var lon = marker.getPosition().lng();
                      console.log(lat);
                      console.log(lon);

                      // $scope.marker.options = {
                      //   draggable: true,
                      //   labelContent: "lat: " + $scope.marker.coords.latitude + ' ' + 'lon: ' + $scope.marker.coords.longitude,
                      //   labelAnchor: "100 0",
                      //   labelClass: "marker-labels"
                      // };
                    }
                  }

                });

                var marker = new google.maps.Marker($scope.markers[index]);

                marker.addListener('click', function() {
                    infoWindow.setContent(infowincontent);
                    infoWindow.open(map, marker);
                  });

                //console.log(infowincontent);
            } //for


            console.log($scope.markers);
            uiGmapGoogleMapApi.then(function(maps) {
                console.log($scope.map);
            });
        });


    $scope.go = function ( hash ) {
        $location.path( hash );
    };

});