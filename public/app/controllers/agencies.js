app.controller('agenciesController', function($scope, $rootScope, $http, API_URL, $location, $stateParams, $state, services, Notification) {

    $('.loading-loader').hide();
    $('.error-loading-container').hide();

    if ($state.current.name == "agency-list") {
        $scope.agencies = [];
        $scope.headerTitle = "Agency List";
        $scope.libraryTemp = {};
        $scope.totalItemsTemp = {};
        $scope.currentPage = 1;

        $scope.totalItems = 0;
        $scope.pageChanged = function (newPage) {
            getResultsPage(newPage);
        };

        getResultsPage(1);
        function getResultsPage(pageNumber) {
            $scope.currentPage = pageNumber;
            if (!$.isEmptyObject($scope.libraryTemp)) {
                $http.get(API_URL + 'agencies?search=' + $scope.searchText + '&page=' + pageNumber, {beforeSend: $('.loading-loader').show()})
                    .success(function (data) {
                        $('.loading-loader').hide();
                        $scope.agencies = data.data;
                        $scope.totalItems = data.total;
                    })
                    .error(function (response) {
                        $('.loading-loader').hide();
                        $('.error-loading-container').show();
                        console.log(response);
                    });
            } else {
                $http.get(API_URL + 'agencies?page=' + pageNumber, {beforeSend: $('.loading-loader').show()})
                    .success(function (data) {
                        $('.loading-loader').hide();
                        $scope.agencies = data.data;
                        $scope.totalItems = data.total;
                        //alert("lol");
                    })
                    .error(function (response) {
                        $('.loading-loader').hide();
                        $('.error-loading-container').show();
                        console.log(response);
                    });
            }
        }

        $scope.searchDB = function () {
            if ($scope.searchText.length >= 1) {
                if ($.isEmptyObject($scope.libraryTemp)) {
                    $scope.libraryTemp = $scope.agencies;
                    $scope.totalItemsTemp = $scope.totalItems;
                    $scope.agencies = {};
                }
                getResultsPage(1);
            } else {
                if (!$.isEmptyObject($scope.libraryTemp)) {
                    $scope.agencies = $scope.libraryTemp;
                    $scope.totalItems = $scope.totalItemsTemp;
                    $scope.libraryTemp = {};
                }
                //getResultsPage(1);
            }
        }
    }

    if ($state.current.name == "agency-edit" || $state.current.name == "agency-profile") {
        var url = API_URL + 'agencies/';
        var field = "agency_id";
        if ($state.current.name == "agency-edit") {
            var agencyID = ($stateParams.userID) ? parseInt($stateParams.userID) : 0;
            url += agencyID;
        } else {
            var agencyID = $rootScope.agencyID;
            url += $rootScope.agencyID;
        }
        $http.get(url, {beforeSend : $('.loading-loader').show()})
            .success(function (response) {
                $('.loading-loader').hide();
                console.log(response);
                $scope.agency = response;
            })
            .error(function (response) {
                $('.loading-loader').hide();
                $('.error-loading-container').show();
                console.log(response);
            });
    }

    $scope.go = function ( hash ) {
        $location.path( hash );
    };

    $scope.toggle = function(modalstate, id) {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.form_title = "New Agency Record";
                $scope.go('/agencies/edit/0' );
                break;

            case 'edit':
                $scope.form_title = "agency Details";
                $scope.id = id;
                $scope.go('/agencies/edit/' + id );

                break;
            default:
                break;
        }
    }

    //save new record / update existing record
    $scope.save = function() {
        var url = API_URL + 'agencies';

        if (agencyID != '0'){
            url += '/' + agencyID;
        }

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.agency),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            beforeSend: $('.loading_loader').show()
        }).success(function(response) {
            if (response.status !== null && response.status == "success") {
                Notification({message: response.message, title: 'Success'});
                $state.reload();
            } else {
                Notification.error({message: response.message, delay: 5000});
            }
            console.log(response);
            $('.loading-loader').hide();
        }).error(function(response) {
            $('.loading-loader').hide();
            console.log(response);
            Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});
        });
    }

    //delete record
    $scope.confirmDelete = function(id) {
        var isConfirmDelete = confirm('Are you sure you want this record?');
        if (isConfirmDelete) {
            $http({
                method: 'DELETE',
                url: API_URL + 'agencies/' + id
            }).
            success(function(response) {
                if (response.status !== null && response.status == "success") {
                    Notification({message: response.message, title: 'Success'});
                    $state.reload();
                } else {
                    Notification.error({message: response.message, delay: 5000});
                }
                console.log(response);
            }).
            error(function(response) {
                console.log(response);
                Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});
            });
        } else {
            return false;
        }
    }

    //deactivate record
    $scope.confirmDeactivate = function(id, active) {

        var confirmMsg = '';
        if (active) {
            confirmMsg = 'Are you sure you want to deactivate this record?';
        } else {
            confirmMsg = 'Are you sure you want to activate this record?';
        }
        var isConfirmDeactivate = confirm(confirmMsg + ' ');
        if (isConfirmDeactivate) {
            var val = {
                "id":id ,
                "deactivate":active
            };

            $http({
                method: 'POST',
                url: API_URL + 'agents/deactivate/' + id,
                data: val,
                headers: {'Content-Type': 'application/json'}

            }).
            success(function(response) {
                if (response.status !== null && response.status == "success") {
                    Notification({message: response.message, title: 'Success'});
                    $state.reload();
                } else {
                    Notification.error({message: response.message, delay: 5000});
                }
                console.log(response);
            }).
            error(function(response) {
                console.log(response);
                Notification.error({message: 'Something went wrong. Please refresh the page and try again.', delay: 5000});
            });
        } else {
            return false;
        }
    }

});