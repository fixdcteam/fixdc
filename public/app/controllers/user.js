app.controller('userController', function($scope, $rootScope, $http, API_URL, $location, $stateParams, $state, services) {

	$scope.user = "";

	$scope.updatePassword = function() {
		var url = API_URL + "user/change-password/" + localStorage.getItem('user');

		$http({
			method: 'POST',
			url: url,
			data: $.param($scope.user),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			//beforeSend: $('.loading_loader').show()
		}).success(function(response) {
			//$('.loading-loader').hide();
			if (response.modal_title != "") {
				$('#myModal').modal('show');
				$scope.modal_title = response.modal_title;
				$scope.modal_content = response.modal_content;
				$scope.user = "";
			}
			console.log(response);
			$location.path( 'account-settings' );
		}).error(function(response) {
			console.log(response);
			//alert('This is embarassing. An error has occured. Please check the log for details');
			alert(response);
		});
		
	}
});

