
(function() {

    'use strict';

    angular
        .module('fixDCApp')
        .controller('AuthController', AuthController);


    function AuthController($auth, $state, $http, $rootScope, $scope, API_URL, $location, $stateParams, $timeout) {

        var vm = this;

        vm.loginError = false;
        vm.loginErrorText;
        vm.resetCredentials = "";
        $rootScope.loginFailed = 0;

        //$state.go('auth');

        vm.login = function() {

            var credentials = {
                email: vm.email,
                password: vm.password
            }

            $auth.login(credentials).then(function() {

                return $http.get(API_URL + 'authenticate/user').then(function(response) {

					if ((response.data.user.type =='admin' || response.data.user.type =='agent') 
						&& response.data.user.isActive =='1' && response.data.user.deleted =='0') {

                        var user = JSON.stringify(response.data.user.id);
                        localStorage.setItem('user', user);
                        $rootScope.authenticated = true;
                        $rootScope.currentUser = response.data.user;
                        $rootScope.loginFailed = 0;

                        if (response.data.user.type =='admin') {
							localStorage.setItem('4u7h_735t', 1);
							$rootScope.isAdminUser = 1
							getAdminDetails(user); 
						} else {
							localStorage.setItem('4u7h_735t', 0);
							$rootScope.isAdminUser = 0;
							getAgentDetails(user); 
						}

                        $timeout(function(){ 
                            $state.go('home');
                        }, 1000)

                    } else {
						vm.loginError = true;
						vm.loginErrorText = "User credentials don't have access to the web system."
						
						if (response.data.user.isActive =='0') {
							vm.loginErrorText = "User has been deactivated. Please contact system admin or your agency admin.";
						}
						
						if (response.data.user.deleted =='1') {
							vm.loginErrorText = "User credentials has been deleted. Please contact system admin or your agency admin.";
						}
                    }

                });

                // Handle errors
            }, function(error) {
                vm.loginError = true;
                vm.loginErrorText = "Invalid Credentials."
                $rootScope.loginFailed += 1;
                if ($rootScope.loginFailed >= 4) {
                    vm.loginErrorText = "You provide an invalid password 4 times already. One more mistake will deactivate your account.";
                }

                if ($rootScope.loginFailed >= 5) {
                    vm.forgotPassword(1);
                }

            });
        }

        vm.logout = function() {

            $auth.logout().then(function() {
                localStorage.removeItem('user');
                localStorage.removeItem('admin');
				localStorage.removeItem('4u7h_735t');
                localStorage.removeItem('4u7h_735a');
                localStorage.removeItem('4u7h_735b');
                $rootScope.authenticated = false;
                $rootScope.currentUser = null;
				$state.go('auth');
            });
        }

        vm.isActive  = function(viewLocation) {
            return viewLocation === $location.path();
        }

        vm.forgotPassword  = function(blocked) {
            var credentials = {
                "email": vm.email,
                "blocked": blocked
            };
            var url = API_URL + 'authenticate/forgot-password/' + vm.email;
            $http({
                method: 'POST',
                url: url,
                data: credentials,
                headers: {'Content-Type': 'application/json'}
            }).success(function(response) {
                console.log(response);
                if (response.status == "success") {
                    if (blocked != 1) {
                        $location.path( '/auth/confirm-email-change' );
                    } else {
                        $location.path( '/auth/confirm-email-change-blocked' );
                    }
                } else {
                    vm.loginError = true;
                    vm.loginErrorText = response.message;
                }

            }).error(function(response) {
                console.log(response);
                //alert('This is embarassing. An error has occured. Please check the log for details');
                alert(response);
            });
        }

        if ($state.current.name == "password-reset") {
            var email = ($stateParams.email);
        }

        vm.resetPassword  = function() {
            if (vm.resetCredentials.password == vm.resetCredentials.retypePassword) {
                var password = {
                    "password": vm.resetCredentials.password
                };
                $http({
                    method: 'POST',
                    url: API_URL + 'authenticate/reset-password/' + email,
                    data: $.param(vm.resetCredentials),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(response) {
                    console.log(response);
                    if (response.status == "success") {
                        alert(response.message);
                        $location.path( '/auth' );
                    } else {
                        vm.loginError = true;
                        vm.loginErrorText = response.message;
                        vm.password = "";
                        vm.retypePassword = "";
                    }

                }).error(function(response) {
                    console.log(response);
                    //alert('This is embarassing. An error has occured. Please check the log for details');
                    alert(response);
                });
            } else {
                vm.loginError = true;
                vm.loginErrorText = "Password should match. Please retype."
                vm.password = "";
                vm.retypePassword = "";
            }

        }


        if ($state.current.name == "check-request") {
            var token = ($stateParams.token);
            var payload = {
                "token": token
            };
            var url = API_URL + 'authenticate/check/' + token;
            $http({
                method: 'POST',
                url: url,
                data: payload,
                headers: {'Content-Type': 'application/json'}
            }).success(function(response) {
                console.log(response);
                if (response.status == "success") {
                    $location.path( '/auth/password-reset/' + response.email );
                } else {
                    vm.loginError = true;
                    vm.loginErrorText = response.message;
                }

            }).error(function(response) {
                console.log(response);
                //alert('This is embarassing. An error has occured. Please check the log for details');
                alert(response);
            });
        }

        if ($state.current.name == "check-activation") {
            var token = ($stateParams.token);
            var payload = {
                "token": token
            };
            var url = API_URL + 'authenticate/check-activation/' + token;
            $http({
                method: 'POST',
                url: url,
                data: payload,
                headers: {'Content-Type': 'application/json'}
            }).success(function(response) {
                console.log(response);
                if (response.status == "success") {
                    $location.path( '/auth/check-activation-success');
                } else {
                    vm.loginError = true;
                    vm.loginErrorText = response.message;
                }

            }).error(function(response) {
                console.log(response);
                //alert('This is embarassing. An error has occured. Please check the log for details');
                alert(response);
            });
        }

        vm.go = function ( hash ) {
            $location.path( hash );
        };


        function getAdminDetails(user_id) {
            $http.get(API_URL + 'admins/' + user_id + '?field=user_id')
                .success(function(response) {
                    $rootScope.adminUser = response.first_name;
                    localStorage.setItem('admin', response.first_name);
                })
        }
		
		function getAgentDetails(user_id) {
            $http.get(API_URL + 'agents/' + user_id + '?field=user_id')
                .success(function(response) {
                    $rootScope.adminUser = response.agent_firstName;
                    localStorage.setItem('admin', response.agent_firstName);
                    $rootScope.isAgentAdmin = response.agent_isAdmin;
                    localStorage.setItem('4u7h_735a', response.agent_isAdmin);
                    $rootScope.agencyID = response.agency_id;
                    localStorage.setItem('4u7h_735b', response.agency_id);

                })
        }

    }

})();