app.config(
    function($routeProvider, $locationProvider, $stateProvider) {

        $stateProvider
            .state('auth', {
                url: '/auth',
                templateUrl: '../resources/views/auth/login.blade.php',
                controller: 'AuthController as auth'
            })
            .state('password-reset', {
                url: '/auth/password-reset/:email',
                templateUrl: '../resources/views/auth/password-reset.blade.php',
                controller: 'AuthController as auth'
            })
            .state('forgot-password', {
                url: '/auth/forgot-password',
                templateUrl: '../resources/views/auth/forgot-password.blade.php',
                controller: 'AuthController as auth'
            })
            .state('confirm-email-change', {
                url: '/auth/confirm-email-change',
                templateUrl: '../resources/views/auth/confirm-email-change.blade.php',
                controller: 'AuthController as auth'
            })
            .state('confirm-email-change-blocked', {
                url: '/auth/confirm-email-change-blocked',
                templateUrl: '../resources/views/auth/confirm-email-change-blocked.blade.php',
                controller: 'AuthController as auth'
            })
            .state('check-request', {
                url: '/auth/check-request/:token',
                templateUrl: '../resources/views/auth/check-request.blade.php',
                controller: 'AuthController as auth'
            })
            .state('check-activation', {
                url: '/auth/check-activation/:token',
                templateUrl: '../resources/views/auth/check-activation.blade.php',
                controller: 'AuthController as auth'
            })
            .state('check-activation-success', {
                url: '/auth/check-activation-success',
                templateUrl: '../resources/views/auth/check-activation-success.blade.php',
                controller: 'AuthController as auth'
            })
            .state('users', {
                url: '/users',
                templateUrl: '../resources/views/userView.html',
                //controller: 'UserController as user'
            })

            .state('home', {
                url: '/home',
                templateUrl: '../resources/views/common/home.blade.php',
                controller: 'employeesController'
            })
            .state('admin-list', {
                url: '/admins/list',
                templateUrl: '../resources/views/admins/index.blade.php',
                controller: 'adminController'
            })
            .state('admin-edit', {
                url: '/admins/edit/:userID',
                title: 'Update Admin Record',
                templateUrl: '../resources/views/admins/edit-admin.blade.php',
                controller: 'adminController'
            })
            .state('citizen-list', {
                url: '/citizens/list',
                title: 'Everyone Citizens',
                templateUrl: '../resources/views/citizens/index.blade.php',
                controller: 'citizensController'
            })
            .state('citizen-edit', {
                url: '/citizens/edit/:userID',
                title: 'Add You for the rest of the lives',
                templateUrl: '../resources/views/citizens/edit-citizen.blade.php',
                controller: 'citizensController'
            })
			.state('citizen-reports', {
                url: '/citizens/cases/:userID/:citizenName',
                title: 'FixDC - Reports',
                templateUrl: '../resources/views/cases/index.blade.php',
                controller: 'casesController'
            })
            .state('agent-list', {
                url: '/agents/list',
                title: 'FixDC - Agents',
                templateUrl: '../resources/views/agents/index.blade.php',
                controller: 'agentsController'
            })
            .state('agent-edit', {
                url: '/agents/edit/:userID',
                title: 'FixDC - Edit Agent Record',
                templateUrl: '../resources/views/agents/edit-agent.blade.php',
                controller: 'agentsController'
            })
            .state('case-list', {
                url: '/cases/list',
                title: 'FixDC - Reports',
                templateUrl: '../resources/views/cases/index.blade.php',
                controller: 'casesController'
            })
            .state('case-edit', {
                url: '/cases/edit/:userID',
                title: 'FixDC - Update Case Report',
                templateUrl: '../resources/views/cases/edit-case.blade.php',
                controller: 'casesController'
            })
            .state('manage-categories', {
                url: '/cases/manage-categories',
                title: 'FixDC - Manage Case Categories',
                templateUrl: '../resources/views/cases/manage-categories.blade.php',
                controller: 'casesController'
            })
            .state('agency-list', {
                url: '/agencies/list',
                title: 'FixDC - Agencies',
                templateUrl: '../resources/views/agencies/index.blade.php',
                controller: 'agenciesController'
            })
            .state('agency-edit', {
                url: '/agencies/edit/:userID',
                title: 'FixDC - Update Agency Record',
                templateUrl: '../resources/views/agencies/edit-agency.blade.php',
                controller: 'agenciesController'
            })
			.state('account-settings', {
                url: '/account-settings',
                title: 'FixDC - Update Agency Record',
                templateUrl: '../resources/views/common/account-settings.blade.php',
                controller: 'userController'
            })
            .state('admin-profile', {
                url: '/admins/profile',
                title: 'FixDC - Admin Profile',
                templateUrl: '../resources/views/admins/profile.blade.php',
                controller: 'adminController'
            })
            .state('agent-profile', {
                url: '/agents/profile',
                title: 'FixDC - Agent Profile',
                templateUrl: '../resources/views/agents/profile.blade.php',
                controller: 'agentsController'
            })
            .state('agency-profile', {
                url: '/agencies/profile',
                title: 'FixDC - Agency Profile',
                templateUrl: '../resources/views/agencies/profile.blade.php',
                controller: 'agenciesController'
            })



        ;

/*
        $routeProvider.
        when('/', {
            templateUrl: '../resources/views/home.blade.php',
            controller: 'employeesController'
        })
        .when('/home', {
            templateUrl: '../resources/views/home.blade.php',
            controller: 'employeesController'
        });*/

    });

