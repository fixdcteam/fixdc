var app = angular.module('fixDCApp', ['ngRoute', 'ui.router', 'ui-notification', 'satellizer', 'ui.bootstrap', 'ui.bootstrap.tpls', 'angularUtils.directives.dirPagination', 'bootstrapLightbox', 'uiGmapgoogle-maps', 'ng-fusioncharts'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
    })

    .run(function($rootScope, $state) {

        $rootScope.$on('$stateChangeStart', function(event, toState) {
            $rootScope.isAdminUser = localStorage.getItem('4u7h_735t');
            $rootScope.isAgentAdmin = localStorage.getItem('4u7h_735a');
			$rootScope.agencyID = localStorage.getItem('4u7h_735b');
			var user = localStorage.getItem('user');
			
            if(user) {
                $rootScope.authenticated = true;
                $rootScope.currentUser = user;
                $rootScope.adminUser = localStorage.getItem('admin');

                if(toState.name == "auth" || toState.name == "password-reset" || toState.name == "forgot-password" ||
                    toState.name == "confirm-email-change" || toState.name == "check-request") {
                    event.preventDefault();
                    $state.go('home');
                }
            } else {
                $rootScope.authenticated = false;
                
                if((toState.name != "auth") &&(toState.name == "password-reset" || toState.name == "forgot-password" ||
                    toState.name == "confirm-email-change" || toState.name == "check-request" )) {
                    //event.preventDefault();
                    $state.go('auth');
                }
            }
        });

    })

    .config(function($stateProvider, $routeProvider, $urlRouterProvider, $authProvider, $httpProvider, $provide, uiGmapGoogleMapApiProvider, $compileProvider) {

        function redirectWhenLoggedOut($q, $injector) {

            return {

                responseError: function(rejection) {

                    // Need to use $injector.get to bring in $state or else we get
                    // a circular dependency error
                    var $state = $injector.get('$state');

                    // Instead of checking for a status code of 400 which might be used
                    // for other reasons in Laravel, we check for the specific rejection
                    // reasons to tell us if we need to redirect to the login state
                    var rejectionReasons = ['token_not_provided', 'token_expired', 'token_absent', 'token_invalid'];

                    // Loop through each rejection reason and redirect to the login
                    // state if one is encountered
                    angular.forEach(rejectionReasons, function(value, key) {

                        if(rejection.data.error === value) {

                            // If we get a rejection corresponding to one of the reasons
                            // in our array, we know we need to authenticate the user so
                            // we can remove the current user from local storage
                            localStorage.removeItem('user');

                            // Send the user to the auth state so they can login
                            $state.go('auth');
                        }
                    });

                    return $q.reject(rejection);
                }
            }
        }

        // Setup for the $httpInterceptor
        $provide.factory('redirectWhenLoggedOut', redirectWhenLoggedOut);

        // Push the new factory onto the $http interceptor array
        $httpProvider.interceptors.push('redirectWhenLoggedOut');

        //$httpProvider.defaults.withCredentials = true;

        // Satellizer configuration that specifies which API
        // route the JWT should be retrieved from
        $authProvider.loginUrl = '/api/v1/authenticate';

        // Redirect to the auth state if any other states
        // are requested other than users
        //$urlRouterProvider.otherwise('/auth');
		
		uiGmapGoogleMapApiProvider.configure({
			key: 'AIzaSyCbYocb7a5JpgzstSaOhRtBofcQs7PvZ7o',
			v: '2.3.2', //defaults to latest 3.X anyhow
			libraries: 'weather,geometry,visualization'
		});

        $compileProvider.debugInfoEnabled(true);

        $routeProvider.when ("/", {redirectTo: "/auth"});


    })
	
	
    .constant('API_URL', 'http://localhost/fixdc/public/api/v1/');




app.directive('script', function() {
    return {
        restrict: 'E',
        scope: false,
        link: function(scope, elem, attr) {
            if (attr.type === 'text/javascript-lazy') {
                var code = elem.text();
                var f = new Function(code);
                f();
            }
        }
    };
});