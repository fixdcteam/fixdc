<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use Illuminate\Http\Request;
use App\Mail\NewAdmin;
use Illuminate\Support\Facades\Input;
/*
header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');
header('Access-Control-Allow-Credentials:  true');
*/

$API_URL = 'api/v1';

Route::get('/', function () {
    return view('app');
});


Route::auth();
/*
    Route::get('/home', 'HomeController@index');

    Route::get('/tasks', 'TaskController@index');
    Route::post('/task', 'TaskController@store');
    Route::delete('/task/{task}', 'TaskController@destroy');
/*
    Route::get('/employees/fuck', function () {
        return view('employees.index');
    });*/

Route::group(['prefix' => 'api'/*, 'middleware' => 'cors'*/], function() {
    Route::resource('v1/authenticate', 'AuthenticateController', ['only' => ['index']]);
    Route::post('v1/authenticate', 'AuthenticateController@authenticate');
    //Route::options('v1/authenticate', 'AuthenticateController@authenticate');
    Route::get('v1/authenticate/user', 'AuthenticateController@getAuthenticatedUser');
	Route::get('v1/authenticate/get-user-type/{id}', 'AuthenticateController@getUserType');
    Route::post('v1/authenticate/forgot-password/{email}', 'AuthenticateController@forgotPassword');
    Route::post('v1/authenticate/check/{token}', 'AuthenticateController@check');
    Route::post('v1/authenticate/check-activation/{token}', 'AuthenticateController@checkActivation');
    Route::post('v1/authenticate/reset-password/{email}', 'AuthenticateController@resetPassword');
    Route::post('v1/authenticate/save-firebase', 'AuthenticateController@saveFirebaseToken');
    Route::delete('v1/authenticate/delete-firebase', 'AuthenticateController@deleteFirebaseToken');
});

Route::get($API_URL . '/user/get-password', 'AuthenticateController@getUserPassword');
Route::post($API_URL . '/user/change-password/{id}', 'AuthenticateController@changePassword');

Route::get('protected', ['middleware' => ['auth', 'admin'], function() {
    return "this page requires that you be logged in and an Admin";
}]);

Route::group(['middleware' => ['web']], function () {
    Route::resource('admins', 'AdminController');
});

Route::get($API_URL . '/admins/{id?}', 'AdminController@index');
Route::post($API_URL . '/admins', 'AdminController@store');
Route::post($API_URL . '/admins/{id}', 'AdminController@update');
Route::delete($API_URL . '/admins/{id}', 'AdminController@destroy');
Route::post($API_URL . '/admins/deactivate/{id?}', 'AdminController@deactivate');
Route::get($API_URL . '/admins/check-access/{id?}', 'AdminController@checkAccess');

Route::get($API_URL . '/citizens/{id?}', 'CitizenController@index');
Route::post($API_URL . '/citizens', 'CitizenController@store');
Route::post($API_URL . '/citizens/{id}', 'CitizenController@update');
Route::delete($API_URL . '/citizens/{id}', 'CitizenController@destroy');
Route::post($API_URL . '/citizens/deactivate/{id}', 'CitizenController@deactivate');
Route::post($API_URL . '/citizens/block/{id?}', 'CitizenController@block');
Route::post($API_URL . '/citizens/send-note/{id?}', 'CitizenController@sendNote');
Route::get($API_URL . '/citizens/getIsActive/{id?}', 'CitizenController@getCitizenIsActive');
Route::get($API_URL . '/citizens/getIsBlocked/{id?}', 'CitizenController@getCitizenisBlocked');
Route::get($API_URL . '/citizens/get-citizen-details/{id?}', 'CitizenController@getCitizenDetails');
Route::post($API_URL . '/citizens/upload-image/{id?}', 'CitizenController@uploadImage');

Route::get($API_URL . '/reportcases/{id?}', 'ReportCaseController@index');
Route::post($API_URL . '/reportcases', 'ReportCaseController@store');
Route::post($API_URL . '/reportcases/{id}', 'ReportCaseController@update');
Route::delete($API_URL . '/reportcases/{id}', 'ReportCaseController@destroy');
Route::post($API_URL . '/reportcases/set-no-of-concern/{id}', 'ReportCaseController@setNoOfConcern');
Route::post($API_URL . '/reportcases/setIsPrivate/{id}', 'ReportCaseController@setIsPrivate');
Route::post($API_URL . '/reportcases/setStatus/{id}', 'ReportCaseController@setStatus');
Route::get($API_URL . '/reportcases/getstatus/{id}', 'ReportCaseController@getstatus');
Route::get($API_URL . '/reportcases/getIsPrivate/{id}', 'ReportCaseController@getIsPrivate');
Route::get($API_URL . '/reportcases/getCasesFromCitizen/{id}', 'ReportCaseController@getCasesFromCitizen');
Route::post($API_URL . '/reportcases/save-category', 'ReportCaseController@saveCategory');
Route::post($API_URL . '/reportcases/update-category/{id?}', 'ReportCaseController@updateCategory');
Route::post($API_URL . '/reportcases/upload-image/{id?}', 'ReportCaseController@uploadImage');
Route::get($API_URL . '/reportcases/get-reports-count/{id?}', 'ReportCaseController@getReportsCount');
Route::get($API_URL . '/reportcases/get-all-case-reports/{id?}', 'ReportCaseController@getAllCaseReports');

Route::get($API_URL . '/agents/{id?}', 'AgentController@index');
Route::post($API_URL . '/agents', 'AgentController@store');
Route::post($API_URL . '/agents/{id}', 'AgentController@update');
Route::delete($API_URL . '/agents/{id}', 'AgentController@destroy');
Route::post($API_URL . '/agents/deactivate/{id}', 'AgentController@deactivate');
Route::post($API_URL . '/agents/make-admin/{id}', 'AgentController@makeAdmin');
Route::get($API_URL . '/agents/getAgentIsActive/{id}', 'AgentController@getAgentIsActive');
Route::get($API_URL . '/agents/get-agent-agency-id/{id}', 'AgentController@getAgencyID');

Route::get($API_URL . '/agencies/{id?}', 'AgencyController@index');
Route::post($API_URL . '/agencies', 'AgencyController@store');
Route::post($API_URL . '/agencies/{id}', 'AgencyController@update');
Route::delete($API_URL . '/agencies/{id}', 'AgencyController@destroy');

Route::get($API_URL . '/categories/{id?}', 'CaseCategoryController@index');
Route::post($API_URL . '/categories', 'CaseCategoryController@store');
Route::post($API_URL . '/categories/{id}', 'CaseCategoryController@update');
Route::delete($API_URL . '/categories/{id}', 'CaseCategoryController@destroy');

Route::get($API_URL . '/home', 'HomeController@index');
/*Route::get($API_URL . '/home/total-cases', 'HomeController@totalCases');
Route::get($API_URL . '/home/total-citizens', 'HomeController@totalCitizens');
Route::get($API_URL . '/home/report-issues', 'HomeController@reportIssues');
Route::get($API_URL . '/home/get-reports-count', 'HomeController@getLatestReportsCount');
Route::get($API_URL . '/home/get-citizen-signups', 'HomeController@getLatestCitizenSignups');
*/


Route::get($API_URL . '/send-mail', function (Request $request) {
    if ($request->get('to')) {
        Mail::to($request->get('to'))->send(new NewAdmin);
        return "true";
        //return view('welcome');
    }
});

Route::post($API_URL . '/file-upload', function()
{
    //I am storing the image in the public/images folder 
    $destinationPath = env('UPLOAD_IMAGES', '../storage/app/public/img');

    $newImageName='MyImage.jpg';

    //Rename and move the file to the destination folder 
    Input::file('file')->move($destinationPath,$newImageName);

    return array("status" => "success", "message" => "Image successfully uploaded.");

});






Route::get($API_URL . '/employees/{id?}', 'Employees@index');
Route::post($API_URL . '/employees', 'Employees@store');
Route::post($API_URL . '/employees/{id}', 'Employees@update');
Route::delete($API_URL . '/employees/{id}', 'Employees@destroy');