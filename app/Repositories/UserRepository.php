<?php

namespace App\Repositories;

use App\User;

class UserRepository
{
    /**
     * Get all of the tasks for a given user.
     *
     * @param  User  $user
     * @return Collection
     */
    public function forUser(User $user)
    {
        return $user->users()
                    ->orderBy('created_at', 'asc')
                    ->get();
    }
}


?>