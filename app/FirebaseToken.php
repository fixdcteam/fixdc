<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

class FirebaseToken extends Model
{

    protected $table = 'users_firebase';
    protected $primaryKey = 'id';
    protected $foreignKey = 'user_id';
    public $timestamps = false;

    protected $fillable = array('id', 'user_id', 'token');



}

