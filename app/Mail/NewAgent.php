<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewAgent extends Mailable
{
    use Queueable, SerializesModels;

    public $__agent;
    public $__app_url;
    public $__email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($agent, $email)
    {
        $this->__agent = $agent;
        $this->__app_url = config('app.url') . '/#/auth';
        $this->__email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $from = config('mail.from');
        $subject = 'Welcome to FixDC';

        return $this->view('email.new-agent')
            ->from($from['address'], $from['name'])
            ->cc($from['address'], $from['name'])
            ->bcc($from['address'], $from['name'])
            ->replyTo($from['address'], $from['name'])
            ->subject($subject);
    }
}
