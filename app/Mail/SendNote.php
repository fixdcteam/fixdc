<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNote extends Mailable
{
    use Queueable, SerializesModels;

    public $citizen;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($citizen)
    {
        $this->citizen = $citizen;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $from = config('mail.from');
        $subject = 'FixDC Admin Sent You a Note';

        return $this->view('email.send-note')
            ->from($from['address'], $from['name'])
            ->cc($from['address'], $from['name'])
            ->bcc($from['address'], $from['name'])
            ->replyTo($from['address'], $from['name'])
            ->subject($subject);
    }
}
