<?php
namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $__admin;
    public $__app_url;
    public $__email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($admin, $email)
    {
        $this->__admin = $admin;
        $this->__app_url = config('app.url') . '/#/auth';
        $this->__email = $email;
    }

    public $total = 30;
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $from = config('mail.from');
        $subject = 'Welcome to FixDC';

        return $this->view('email.new-admin')
                    ->from($from['address'], $from['name'])
                    ->cc($from['address'], $from['name'])
                    ->bcc($from['address'], $from['name'])
                    ->replyTo($from['address'], $from['name'])
                    ->subject($subject);
    }
}