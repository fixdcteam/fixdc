<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PasswordResetBlocked extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $app_url;
    public $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $token)
    {
        $this->user = $user;
        $this->app_url = config('app.url') . '/#/auth/check-request/' . $token;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $from = config('mail.from');
        $subject = 'Your Account was Deactivated';

        return $this->view('email.password-reset-blocked')
            ->from($from['address'], $from['name'])
            ->cc($from['address'], $from['name'])
            ->bcc($from['address'], $from['name'])
            ->replyTo($from['address'], $from['name'])
            ->subject($subject);
    }
}
