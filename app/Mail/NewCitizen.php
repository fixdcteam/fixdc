<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewCitizen extends Mailable
{
    use Queueable, SerializesModels;

    public $__citizen;
    public $__app_url;
    public $__email;
    public $__token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($citizen, $email, $token)
    {
        $this->__citizen = $citizen;
        $this->__app_url = config('app.url') . '/#/auth/check-activation/' . $token;
        $this->__email = $email;
        $this->__token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $from = config('mail.from');
        $subject = 'Welcome to FixDC';

        return $this->view('email.new-citizen')
            ->from($from['address'], $from['name'])
            ->cc($from['address'], $from['name'])
            ->bcc($from['address'], $from['name'])
            ->replyTo($from['address'], $from['name'])
            ->subject($subject);
    }
}
