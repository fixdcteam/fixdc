<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class CaseCategory extends Model
{

    protected $table = 'case_category';
    protected $primaryKey = 'cat_id';
    //protected $foreignKey = 'agency_id';
    public $timestamps = false;

    protected $fillable = array('cat_id', 'agency_id', 'cat_code', 'cat_name');

    public function report_case() {
        return $this->hasMany('App\ReportCase');
    }

    public function agency() {
        return $this->belongsTo('App\Agency');
    }


}

