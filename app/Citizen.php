<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Citizen extends Eloquent
{
    use SoftDeletes;

    protected $table = 'citizen';
    protected $primaryKey = 'citizen_id';
    public $timestamps = false;

    protected $fillable = array('citizen_id', 'user_id', 'img_id', 'citizen_firstName', 'citizen_lastName', 'citizen_gender', 'citizen_DOB', 'citizen_email',
        'citizen_username', 'citizen_password', 'citizen_dateTimeReg', 'citizen_isActive', 'citizen_isBlocked', 'deleted_at');

    protected $dates = ['deleted_at'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function report_case() {
        return $this->hasMany('App\ReportCase');
    }

    public function image() {
        return $this->belongsTo('App\Image', 'img_id');
    }

    public function concerned_case() {
        return $this->hasOne('App\CaseImage');
    }



}

