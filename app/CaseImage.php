<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

class CaseImage extends Model
{
    protected $table = 'case_image';
    protected $primaryKey = 'case_image_id';
    //protected $foreignKey = 'agency_id';
    public $timestamps = false;

    protected $fillable = array('case_image_id', 'case_id', 'img_id', 'admin_uploaded');

    public function case() {
        return $this->belongsTo('App\ReportCase');
    }

    public function img() {
        return $this->belongsTo('App\Image');
    }


}

