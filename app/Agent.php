<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agent extends Model
{
    use SoftDeletes;

    protected $table = 'agent';
    protected $primaryKey = 'agent_id';
    public $timestamps = false;

    protected $fillable = array('user_id', 'agency_id', 'agent_id', 'agent_firstName', 'agent_lastName', 'agent_gender',
        'agent_cont1', 'agent_cont2', 'agent_posTitle', 'agent_sysPos', 'agent_isAdmin', 'deleted_at');

    protected $dates = ['deleted_at'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function agency() {
        return $this->belongsTo('App\Agency');
    }


}

