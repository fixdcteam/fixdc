<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

class ReportCase extends Model
{
    protected $table = 'report_case';
    protected $primaryKey = 'case_id';
    public $timestamps = false;

    protected $fillable = array('case_id', 'cat_id', 'citizen_id', 'agency_id', 'case_datetime', 'case_description', 'case_name', 'case_address',
        'case_loc_long', 'case_loc_lat', 'case_noOfConcerns', 'case_isPrivate', 'case_status', 'case_adminNote');

    public function citizen() {
        return $this->belongsTo('App\Citizen');
    }

    public function cat() {
        return $this->belongsTo('App\CaseCategory');
    }

    public function agency() {
        return $this->belongsTo('App\Agency');
    }

    public function case_image() {
        return $this->hasMany('App\CaseImage', 'case_id');
    }

    public function images() {
        return $this->belongsToMany('App\Image', 'case_image', 'case_id', 'img_id');
    }

    public function image() {
        return $this->belongsTo('App\Image', 'img_id');
    }

    public function concerned_case() {
        return $this->hasMany('App\ConcernedCase', 'case_id');
    }

    public function concerned_cases() {
        return $this->belongsToMany('App\Citizen', 'concerned_case', 'case_id', 'citizen_id');
    }

    

}

