<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

class ConcernedCase extends Model
{
    protected $table = 'concerned_case';
    protected $primaryKey = 'concern_id';
    //protected $foreignKey = 'agency_id';
    public $timestamps = false;

    protected $fillable = array('concern_id', 'citizen_id', 'case_id');

    public function case() {
        return $this->belongsTo('App\ReportCase');
    }

    public function citizen() {
        return $this->belongsTo('App\Citizen');
    }


}

