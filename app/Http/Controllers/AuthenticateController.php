<?php
namespace App\Http\Controllers;

use App\User;
use App\FirebaseToken;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\PasswordReset;
use App\Mail\PasswordResetBlocked;
use Illuminate\Support\Facades\Input;


class AuthenticateController extends Controller
{
    public function __construct()
    {

        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth', ['except' => ['authenticate', 'getUserType', 'forgotPassword', 'check', 'checkActivation', 'resetPassword', 'saveFirebaseToken', 'deleteFirebaseToken']]);
    }
    /**
     * Return the user
     *
     * @return Response
     */
    public function index()
    {
        // Retrieve all the users in the database and return them
        $users = User::all();
        return $users;
    }
    /**
     * Return a JWT
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        /*if ($request->isMethod('OPTIONS'))
        {
            if (null !== Request::server('HTTP_ACCESS_CONTROL_REQUEST_METHOD') && (
                Request::server('HTTP_ACCESS_CONTROL_REQUEST_METHOD') == 'POST')
                ) {
                Request::header('Access-Control-Allow-Origin: *');
                Request::header('Access-Control-Allow-Credentials: true');
                Request::header('Access-Control-Allow-Headers: X-Requested-With');
                Request::header('Access-Control-Allow-Headers: Content-Type');
                Request::header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT');
                Request::header('Access-Control-Max-Age: 86400');
            }
        }*/

        $credentials = $request->only('email', 'password');
        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        // if no errors are encountered we can return a JWT
		$token = response()->json(compact('token'));
		/*$user = $this->getAuthenticatedUser();
		$user = json_decode($user);
		if ($user['deleted'] != 1 ) {*/
			return $token; //response()->json(compact('token'));
		//}
    }

    public function getAuthenticatedUser()
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function changePassword(Request $request, $id) {
        $user = User::find($id);
        if ($request->input('password') != $request->input('confirm_password')) {
            return '{"modal_title":"Password does not match", "modal_content":"Passwords does not match. Please ensure you have entered the password correctly."}';
        } elseif (!Hash::check($request->input('current_password'), $user->password)) {
            return '{"modal_title":"Invalid Password", "modal_content":"Current password is wrong."}';
            //return '{"modal_title":"' . bcrypt($request->input('current_password')) . '", "modal_content":"' . $user->password . '"}';
        } elseif (null !== $request->input('password')) {
            $user->password = bcrypt( $request->input('password') );
        }
        $user->save();
        return '{"modal_title":"Success", "modal_content":"Success updating your password."}';
        //return "Success updating user #" . $user->id;
    }

    /**
     * Forgot password
     *
     * @param  Request  $request
     * @param  string  $email
     * @return Response
     */
    public function forgotPassword(Request $request, $email) {
        $blocked = json_decode($request->json('blocked'));
        $user = User::where('email', $email)->first();
        if ($user) {
            $token = hash("sha256", time());

            DB::table('password_resets')->insert(
                ['email' => $user->email, 'token' => $token]
            );

            if ($blocked != 1) {
                Mail::to($user->email)->send(new PasswordReset($user, $token));
            } else {
                $user->isActive = 0;
                $user->save();
                Mail::to($user->email)->send(new PasswordResetBlocked($user, $token));
            }

            return '{"status":"success", "message":""}';
        }

        return '{"status":"failed", "message":"Email is not registered on the system. Please ensure the email is correct."}';
    }

    /**
     * Check token
     *
     * @param  Request  $request
     * @param  string  $token
     * @return Response
     */
    public function check(Request $request, $token) {
        $password_reset = DB::table('password_resets')->where('token', $token)->first();

        if ($password_reset) {
            return '{"status":"success", "email":"'. $password_reset->email . '"}';
        }
        return '{"status":"failed", "message":"Token is invalid."}';
    }

    /**
     * Check token for activations
     *
     * @param  Request  $request
     * @param  string  $token
     * @return Response
     */
    public function checkActivation(Request $request, $token) {
        $password_reset = DB::table('password_resets')->where('token', $token)->first();

        if ($password_reset) {
            $user = User::where('email', $password_reset->email)->first();
            if ($user) {
                $user->isActive = 1;
                $user->save();
            }

            return '{"status":"success", "email":"'. $password_reset->email . '"}';
        }
        return '{"status":"failed", "message":"Token is invalid."}';
    }

    /**
     * Reset Password
     *
     * @param  Request  $request
     * @param  string  $email
     * @return Response
     */
    public function resetPassword(Request $request, $email) {
        $user = User::where('email', $email)->first();
        if ($user) {
            $user->password = bcrypt( $request->input('password') );
            $user->isActive = 1;
            $user->save();
            return '{"status":"success", "message":"Successfully changed password. Please go to log in page."}';
        }
        return '{"status":"failed", "message":"Failed to change your password. Please check."}';
    }

    /**
     * Save firebase token for user
     * 
     * id - user id
     * token - firebase token
     * 
     * @param  Request  $request
     * @return Response
     */
    public function saveFirebaseToken(Request $request) {
        $fbtoken = new FirebaseToken;
        $fbtoken->user_id = json_decode($request->json('id'));
        $fbtoken->token = Input::get('token'); //json_decode($request->json('token'));
        $fbtoken->save();

        return '{"status":"success", "message":"Successfully saved fb token."}';
    }

    /**
     * Delete firebase token for user
     * 
     * id - user id
     * token - firebase token
     * 
     * @param  Request  $request
     * @return Response
     */
    public function deleteFirebaseToken(Request $request) {
        $fbtoken = FirebaseToken::where('user_id', json_decode($request->json('id')))
                                ->where('token', Input::get('token'))
                                ->first();
        $fbtoken->delete();

        return '{"status":"success", "message":"Successfully deleted fb token."}';
    }


	
	/**
     * Check usertype.
     *
     * @param int  $id
     * @return Response
     */
    public function getUserType($id = null) {
        $user = User::find($id);
		return '{"user_type":"' . $user->type . '"}';
    }

    /**
     * Get user password.
     *
     * @param Request $request
     * @return Response
     */
    public function getUserPassword(Request $request) {
        $id = $request->get('id');
        $user = User::find($id);
        return '{"user_password":"' . $user->password . '"}';
    }
	
}