<?php

namespace App\Http\Controllers;

use App\Admin;
use App\User;
use App\ReportCase;
use App\Citizen;
use App\CaseCategory;
use App\CaseImage;
use App\Image;
use App\FirebaseToken;
use App\ConcernedCase;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Log;

class ReportCaseController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct() {
        //$this->middleware('auth');
        $this->middleware('jwt.auth', ['except' => [ '__hasAdminAccess', 'uploadImage']]);
    }

    /**
     * Display a listing of the resource.
     *
	 * @param Request $request
     * @param  int  $case_id
     * @return Response
     */
    public function index(Request $request, $case_id = null) {
        if ($case_id == null) {
            $param = array();
            if($request->get('case_status')) {
                $param['case_status'] = ($request->get('case_status'));
            }
            if($request->get('case_isPrivate')) {
                $param['case_isPrivate'] = ($request->get('case_isPrivate') == 1 ) ? 1 : 0;
            }
			if($request->get('agency-id')) {
                $param['agency_id'] = $request->get('agency-id');
            }
			if($request->get('cat_id')) {
                $param['cat_id'] = $request->get('cat_id');
            }
			if($request->get('citizen-id')) {
                $param['citizen_id'] = $request->get('citizen-id');
            }
			if(count($param) > 0){
                if ($request->get('no-invalid')) {
                    return ReportCase::with(['citizen', 'citizen.image', 'cat', 'images', 'concerned_cases'])->where('case_status', '!=', '2')->where($param)->orderBy('case_id', 'desc')->paginate(10);
                } else {
                    return ReportCase::with(['citizen', 'citizen.image', 'cat', 'images', 'concerned_cases'])->where($param)->orderBy('case_id', 'desc')->paginate(10);
                }	
			} else {
				return ReportCase::with(['citizen', 'citizen.image', 'cat', 'images', 'concerned_cases'])->orderBy('case_id', 'desc')->paginate(10);
			}
        } else {
            return $this->show($case_id);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        $user = User::find($request->input('citizen_id'));
        $citizen = Citizen::where('user_id', $user->id)->first();
        //$category = DB::table('case_category')->where('cat_id', $request->input('cat'))->get();
        $category = CaseCategory::where('cat_id', $request->input('cat'))->first();
        $case = new ReportCase;

        if ($request->input('cat')) {
            $category_id = $request->input('cat');
            $case->cat_id = $category_id['cat_id'];
            if ($category) {
                $case->agency_id = $category->agency_id;
            }
        }

        $case->citizen_id = $citizen->citizen_id; //hide a field that contains the citizen_id
        //$case->case_datetime =  ""; --on db automatic current_timestamp default value
        $case->case_description = $request->input('case_description');
        $case->case_name = $category->cat_name; //$request->input('case_name');
        $case->case_address = $request->input('case_address');
        $case->case_loc_long = $request->input('case_loc_long');
        $case->case_loc_lat = $request->input('case_loc_lat');
        $case->case_noOfConcerns = 0;
        $case->case_isPrivate = $request->input('case_isPrivate');
        $case->case_status = $request->input('case_status');
        $case->case_adminNote = "";
        $case->save();

        $user_firebase = FirebaseToken::where('user_id', $user->id)->first();

        if ($user_firebase) {
            $message = "Reported case is subject for review.";
            $this->sendNotification($user_firebase->token, $message, $case->case_id, 1);
        }

        return array("status" => "success", "message" => "Report case record successfully created.", "case_id" => $case->case_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $case_id
     * @return Response
     */
    public function show($case_id) {
        return ReportCase::with(['citizen', 'citizen.image', 'cat', 'images', 'agency', 'concerned_cases', 'case_image'])->where('case_id', $case_id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $case_id
     * @return Response
     */
    public function update(Request $request, $case_id) {

        $case = ReportCase::find($case_id);
        //$category = DB::table('case_category')->where('cat_name', $request->input('case.case_category'))->value('cat_id');

        if ($request->input('cat')) {
            $category = $request->input('cat');
            $case->cat_id = $category['cat_id'];
        }
        
        if ($request->input('agency')) {
            $agency = $request->input('agency');
            $case->agency_id = $agency['agency_id'];
        }
        
        //$case->case_datetime =  "";       -> cant be updated???
        $case->case_description = $request->input('case_description');
        $case->case_name = $request->input('case_name');
        $case->case_address = $request->input('case_address');
        $case->case_loc_long = $request->input('case_loc_long');
        $case->case_loc_lat = $request->input('case_loc_lat');

        if ($request->input('case_noOfConcerns')) {
            $case->case_noOfConcerns = $request->input('case_noOfConcerns');
        }
        if ($request->input('case_isPrivate')) {
            $case->case_isPrivate = $request->input('case_isPrivate');
        }
        if ($request->input('case_status')) {
            $case->case_status = $request->input('case_status');
        }
        if ($request->input('case_adminNote')) {
            $case->case_adminNote = $request->input('case_adminNote');
        }

        $case->save();

        $citizen = Citizen::where('citizen_id', $case->citizen_id)->first();
        $user_firebase = FirebaseToken::where('user_id', $citizen->user_id)->first();

        if ($user_firebase) {
            $message = "One of your reported case has been updated by an admin or agency.";
            $this->sendNotification($user_firebase->token, $message, $case->case_id);
        }

        return array("status" => "success", "message" => "Report case record successfully updated.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $case_id
     * @param  Request  $request
     * @return Response
     */
    public function destroy(Request $request, $case_id) {
        $case = ReportCase::find($case_id);
        $case->delete();

        return array("status" => "success", "message" => "Report case record successfully deleted.");
    }

    /**
     * Add or subtract no of concerns
     * set_type values must be '1' for add,  or '2' fpr subtact
     *
     * @param  int  $case_id
     * @param  Request  $request
     * @return Response
     */
    public function setNoOfConcern(Request $request, $case_id) {
        //$case_id = json_decode($request->json('case_id'));
        $case = ReportCase::find($case_id);
        $setType = json_decode($request->json('set_type'));
        $citizen = Citizen::find(Input::get('citizen_id'));

        if ($setType == "1") {
            $existConcernedCase = ConcernedCase::where('citizen_id', $citizen->citizen_id)->where('case_id', $case_id)->first();
            if (!$existConcernedCase) {
                $case->case_noOfConcerns = $case->case_noOfConcerns + 1;
                $concernedCase = New ConcernedCase;
                $concernedCase->citizen_id = $citizen->citizen_id;
                $concernedCase->case_id =  $case_id;
                $concernedCase->save();
            }
            
        } elseif ($setType == '2')  {
            $concernedCase = ConcernedCase::where('citizen_id', $citizen->citizen_id)->where('case_id', $case_id)->first();
            if ($concernedCase) {
                $case->case_noOfConcerns = $case->case_noOfConcerns - 1;
                $concernedCase->delete();
            }
        }

        $case->save();

        return array("status" => "success", "message" => "Successfully setting no of concerns of report case #" . $case->case_id, "case_noOfConcerns" => $case->case_noOfConcerns, "setype" => $setType);
    }

    /**
     * Set if report case isPrivate or not
     * set_private - 0 is public, 1 is private
     *
     * @param  int  $case_id
     * @param  Request  $request
     * @return Response
     */
    public function setIsPrivate(Request $request, $case_id) {
        $case_id = json_decode($request->json('id'));
        $case = ReportCase::find($case_id);
        $case->case_isPrivate = json_decode($request->json('setIsPrivate'));
        $case->save();

        return array("status" => "success", "message" => "Successfully setting the privacy of this case.");

    }

    /**
     * Set if report case status
     * set_status values must be 1 or 0 (1 is open, 0 is resolved)
     *
     * @param  int  $case_id
     * @param  Request  $request
     * @return Response
     */
    public function setStatus(Request $request, $case_id) {
        $case_id = json_decode($request->json('id'));
        $case = ReportCase::find($case_id);
        $set_status = json_decode($request->json('setStatus'));
        $case->case_status = $set_status;
        $case->save();
        $message = "";

        if ($set_status == 1) {
            $message = "Your reported case is marked open by an admin or agency.";
        } elseif ($set_status == 2) {
            $message = "Your reported case is marked invalid by an admin or agency.";
        } elseif ($set_status == 3) {
            $message = "Your reported case is marked resolved by an admin or agency.";
        } elseif ($set_status == 4) {
            $message = "Your reported case is marked In-progress by an admin or agency.";
        } 

        $citizen = Citizen::where('citizen_id', $case->citizen_id)->first();
        $user_firebase = FirebaseToken::where('user_id', $citizen->user_id)->first();

        if ($user_firebase) {
            $this->sendNotification($user_firebase->token, $message, $case->case_id);
        }

        return array("status" => "success", "message" => "Successfully setting the status of this case.");
    }

    /**
     * Upload case image
     * 
     *
     * @param  int  $case_id
     * @param  Request  $request
     * @return Response
     */
    public function uploadImage(Request $request, $case_id) {
        $file = array('image' => Input::file('file'));
        $admin_uploaded = 0;

        if($request->get('admin-upload')) {
            $admin_uploaded = $request->get('admin-upload');
        }
        
        $rules = array('image' => 'required', 'image' => 'mimes:jpeg,bmp,png,jpg', 'max' => '10000'); //mimes:jpeg,bmp,png and for max size max:10000
        $validator = Validator::make($file, $rules);

        if ($validator->fails()) {
          return array("status" => "error", "message" => "Error image file type or image is too big.");
        } else {
            if (Input::file('file')->isValid()) {
                $destinationPath = env('UPLOAD_IMAGES', '../storage/app/public/img');
                $extension = Input::file('file')->getClientOriginalExtension();
                $fileName = str_random(15);
                $filesize = Input::file('file')->getSize();
                $fileOrigName = Input::file('file')->getClientOriginalName();
                Input::file('file')->move($destinationPath,  $fileName.'.'.$extension); 

                $image = new Image;
                $image->img_directory = $destinationPath . '/';
                $image->img_name = $fileName;
                $image->img_description = $fileOrigName;
                $image->img_ext = $extension;
                $image->img_size = $filesize;
                $image->save();

                $caseImage = new CaseImage;
                $caseImage->case_id = $case_id;
                $caseImage->img_id = $image->img_id;
                $caseImage->admin_uploaded = $admin_uploaded;
                $caseImage->save();

                $case = ReportCase::where('case_id', $case_id)->first();
                $citizen = Citizen::where('citizen_id', $case->citizen_id)->first();
                $user_firebase = FirebaseToken::where('user_id', $citizen->user_id)->first();

                if ($user_firebase) {
                    $message = "An admin added a photo on your report case.";
                    $this->sendNotification($user_firebase->token, $message, $case_id);
                }

                return array("status" => "success", "message" => "Image successfully uploaded.");
            }
            else {
                return array("status" => "success", "message" => "Uploaded file is not valid.");
            }
        }
    }


    /**
     * Get report case status
     *
     * @param  int  $case_id
     * @param  Request  $request
     * @return Response
     */
    public function getstatus(Request $request, $case_id) {
        $case = ReportCase::find($case_id);
        return $case->case_status;
    }

    /**
     * Get case_isPrivate
     *
     * @param  int  $case_id
     * @param  Request  $request
     * @return Response
     */
    public function getIsPrivate(Request $request, $case_id) {
        $case = ReportCase::find($case_id);
        return $case->case_isPrivate;
    }

    /**
     * Get reported cases from a specific citizen.
     *
     * @param  int  $citizen_id
     * @return Response
     */
    public function getCasesFromCitizen($citizen_id) {
        return ReportCase::with(['citizen', 'cat'])->where('citizen_id', $citizen_id)->orderBy('case_id', 'desc')->get();
    }

    /**
     * Get CASES COUNT
     *
     * @param  int  $citizen_id
     * @param  Request  $request
     * @return Response
     */
    public function getReportsCount(Request $request, $agency_id) {
        //$agency_id = $request->get('agency_id');]

        if ($agency_id !=0 ) {
            $count = DB::table('report_case')
                     ->select(DB::raw('count(*) as case_count, case_status'))
                     ->where('case_status', '1')
                     ->where('agency_id', $agency_id)
                     ->groupBy('case_status')
                     ->get();
        } else {
            $count = DB::table('report_case')
                     ->select(DB::raw('count(*) as case_count, case_status'))
                     ->where('case_status', '1')
                     ->groupBy('case_status')
                     ->get();
        }
        return $count;
        return array("count" => $count);
    }

    /**
     * Get all case reports.
     *
     * @param  Request  $request
     * @return Response
     */
    public function getAllCaseReports(Request $request) {
        return ReportCase::with(['citizen', 'cat'])->orderBy('case_id', 'desc')->get();
    }

    /**
     * Send Notification
     *
     * @param  string $token
     * @param  string $message
     * @return Response
     */
    private function sendNotification($token, $message, $caseID, $delay = 0) {
        $path_to_firebase_cm = "https://fcm.googleapis.com/fcm/send";
            $fields = array(
                 "to" => $token,
                 "data" => array(
                     "alertTitle" => "FixDC",
                     "alertMessage" => $message ,
                     "alertType" => "Reminder",
                     "case_id" => $caseID,
                     "delay" => $delay,
                 ),
                "notification" => array(
                    "click_action" => "FCM_PLUGIN_ACTIVITY",
                    'title' => "FixDC",
                    'body' => $message ,
                    "sound" => "default",
                    "icon" =>"fcm_push_icon"
                )
             );

            $headers = array(
                 'Authorization: key=AAAAiRJtRXk:APA91bE--5y4p5MnlllKiccrxPaeP3PvP1rF-IhWcvW8UNbr58Q8wzWyt3YPEcANadzICwXVLI967jpKuH77Cnd_GdL7BGRhMUbgffPdJQSyootDnXUroXfNaDkfRvA6OufZg1UV1YNx',
                 'Content-Type: application/json'
             );

             $ch = curl_init();

             curl_setopt( $ch, CURLOPT_URL, $path_to_firebase_cm );
             curl_setopt( $ch, CURLOPT_POST, true );
             curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
             curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
             curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
             curl_setopt( $ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
             curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ));

             $result = curl_exec( $ch );
             curl_close( $ch );

             $fb_response[] = json_decode( $result, TRUE );
    }

    private function __checkAccess($level = "admin") {
        $user = json_decode(User::getAuthenticatedUser());

        if (!$user->type) {
            redirect('/public/#/auth');
        } else {
            if ($user->type != $level) {
                redirect('/public/#/citizens/list');
            }
        }
    }

    private function __hasAdminAccess() {
        $user = User::getAuthenticatedUser();
        
        if (!isset($user['user']['attributes']['type'])) {
            return false;
        } else {
            if ($user['user']['attributes']['type'] == "admin") {
                return true;
            }
        }
        return false;
    }

}

?>