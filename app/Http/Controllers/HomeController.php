<?php

namespace App\Http\Controllers;

use App\ReportCase;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['checkAccess', 'hasAccess', '__hasAdminAccess']]);
        $this->middleware('agent');
    }

    /**
     * get the total case for each status
	 *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $data = array();
        $agency_id = false;

        if ($request->get('agency-id')) {
            $agency_id = $request->get('agency-id');
        }
        
        if (!$agency_id) {
            $data['case'] = DB::table('report_case')
                     ->select(DB::raw('count(*) as case_count, case_status'))
                     ->groupBy('case_status')
                     ->get();

            $data['total_cases'] = DB::table('report_case')
                     ->select(DB::raw('count(*) as case_count'))
                     ->get();
            
            $data['report_issues'] = DB::table('report_case')
                     //->select(DB::raw('count(*) as case_cat, (SELECT count(*) as resolved from report_case where case_status = 1) as resolved, (SELECT count(*) as unresolved from report_case where case_status = 0) as unresolved'))
                     ->join('case_category', 'report_case.cat_id', '=', 'case_category.cat_id')
                     ->select('case_category.cat_name', DB::raw('count(*) as total'), DB::raw('count(case when (case_status = 1 OR case_status = 2 OR case_status = 4) then 1 else null end) as unresolved'), 
                                DB::raw('count(case when case_status = 3 then 1 else null end) as resolved'))
                     ->groupBy('report_case.cat_id')
                     ->get();

            $data['case_report_list'] = ReportCase::with(['citizen', 'cat'])->orderBy('case_id', 'desc')->get();

        } else {

            $data['case'] = DB::table('report_case')
                     ->select(DB::raw('count(*) as case_count, case_status'))
                     ->where('agency_id', $agency_id)
                     ->groupBy('case_status')
                     ->get();

            $data['total_cases'] = DB::table('report_case')
                     ->select(DB::raw('count(*) as case_count'))
                     ->where('agency_id', $agency_id)
                     ->get();

            $data['report_issues'] = DB::table('report_case')
                     //->select(DB::raw('count(*) as case_cat, (SELECT count(*) as resolved from report_case where case_status = 1) as resolved, (SELECT count(*) as unresolved from report_case where case_status = 0) as unresolved'))
                     ->join('case_category', 'report_case.cat_id', '=', 'case_category.cat_id')
                     ->select('case_category.cat_name', DB::raw('count(*) as total'), DB::raw('count(case when (case_status = 1 OR case_status = 2 OR case_status = 4) then 1 else null end) as unresolved'), 
                                DB::raw('count(case when case_status = 3 then 1 else null end) as resolved'))
                     ->where('report_case.agency_id', $agency_id)
                     ->groupBy('report_case.cat_id')
                     ->get();

            $data['case_report_list'] = ReportCase::with(['citizen', 'cat'])
                     ->where('agency_id', $agency_id)
                     ->orderBy('case_id', 'desc')
                     ->get();
        }

        $data['total_citizens'] = DB::table('citizen')
                ->select(DB::raw('count(*) as citizen_count'))
                ->get();

        $res = array();
        for($x = -15 ; $x <= 0; $x++) {
            $res[date("Y-m-d", strtotime($x."days"))] = array('label'=>date("M j", strtotime($x."days")), 'value'=>'0');
        }

        if (!$agency_id) {
            $info = DB::table('report_case')
                     ->select(DB::raw('SUBSTRING(case_datetime, 1, 10) AS dates'), (DB::raw('count(*) as total')))
                     ->where("case_datetime", ">=", date('Y-m-d H:i:s', strtotime('-20months')))
                     ->groupBy('dates')
                     ->get();

        } else {
            $info = DB::table('report_case')
                     ->select(DB::raw('SUBSTRING(case_datetime, 1, 10) AS dates'), (DB::raw('count(*) as total')))
                     ->where("case_datetime", ">=", date('Y-m-d H:i:s', strtotime('-20months')))
                     ->where('agency_id', $agency_id)
                     ->groupBy('dates')
                     ->get();
        }

        $reports_count = array();
        if(count($info) > 0) {
            foreach ($info as $row) {
                $res[$row->dates] = array('label'=>date("M j", strtotime($row->dates)), 'value'=>$row->total);
            }
            
            foreach ($res as $key=>$value) {
                $reports_count[] = $value;
            }
        }

        $data['reports_count'] = $reports_count;

        $res = array();
        for($x = -15 ; $x <= 0; $x++) {
            $res[date("Y-m-d", strtotime($x."days"))] = array('label'=>date("M j", strtotime($x."days")), 'value'=>'0');
        }

        $info = DB::table('users')
                     ->select(DB::raw('SUBSTRING(created_at, 1, 10) AS dates'), (DB::raw('count(*) as total')))
                     ->where("created_at", ">=", date('Y-m-d H:i:s', strtotime('-15days')))
                     ->where("type", "citizen")
                     ->groupBy('dates')
                     ->get();

        $citizens_count = array();
        if(count($info) > 0) {
            foreach ($info as $row) {
                $res[$row->dates] = array('label'=>date("M j", strtotime($row->dates)), 'value'=>$row->total);
            }

            foreach ($res as $key=>$value) {
                $citizens_count[] = $value;
            }
        }

        $data['citizens_count'] = $citizens_count;

        $res = array();
        for($x = -15 ; $x <= 0; $x++) {
            $res[date("Y-m-d", strtotime($x."days"))] = array('label'=>date("M j", strtotime($x."days")), 'value'=>'0');
        }

        $info = DB::table('users')
                     ->select(DB::raw('SUBSTRING(created_at, 1, 10) AS dates'), (DB::raw('count(*) as total')))
                     ->where("created_at", ">=", date('Y-m-d H:i:s', strtotime('-15days')))
                     ->wherein('type', ['admin','agent'])
                     ->groupBy('dates')
                     ->get();

        $users_count = array();
        if(count($info) > 0) {
            foreach ($info as $row) {
                $res[$row->dates] = array('label'=>date("M j", strtotime($row->dates)), 'value'=>$row->total);
            }
            
            foreach ($res as $key=>$value) {
                $users_count[] = $value;
            }
        }

        $data['users_count'] = $users_count;

        $recent_citizens = DB::table('citizen')
                ->leftjoin('users', 'citizen.user_id', '=', 'users.id')
                ->select(DB::raw('`citizen`.*, `users`.*'))
                ->orderBy('citizen_id', 'desc')
                ->limit(5)
                ->get();

        $data['recent_citizens'] = $recent_citizens;


		return $data;	 
    }
	

}
