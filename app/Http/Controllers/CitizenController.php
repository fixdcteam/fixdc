<?php

namespace App\Http\Controllers;

use App\Citizen;
use App\User;
use App\Image;
use App\FirebaseToken;

use Faker\Provider\zh_CN\DateTime;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Mail\NewCitizen;
use App\Mail\SendNote;

class CitizenController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
        $this->middleware('jwt.auth', ['except' => [ '__hasAdminAccess', 'getCitizenDetails', 'store', 'update', 'uploadImage']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param  int  $citizen_id
     * @return Response
     */
    public function index(Request $request, $citizen_id = null) {
        if (!$this->__hasAdminAccess() ) {
            return array("status" => "error", "message" => "Failed to get citizen records. Access denied.");
        }

        if ($citizen_id == null) {
			$param = "";
			if($request->get('isActive')) {
				$isActive = ($request->get('isActive')) == 1 ? 1 : 0;
                $param .= "`users`.`isActive` = " . $isActive;
            }
			if($request->get('citizen_isBlocked')) {
                if ($param != "") { $param .= " AND "; }
				$citizen_isBlocked = ($request->get('citizen_isBlocked')) == 1 ? 1 : 0;
				$param .= "`citizen`.`citizen_isBlocked` = " . $citizen_isBlocked;
            }
			if($request->get('search')) {
				$search = $request->get('search');
				if ($param != "") { $param .= " AND "; }
                $param .= "(`citizen`.`citizen_firstName` LIKE '%".$search."%' OR `citizen`.`citizen_lastName` LIKE '%".$search."%')"; //, array('%'.$search.'%','%'.$search.'%');
            }
			
			if ($param != "") {
				if ($request->get('with_invalid')) {
					$case = DB::table('report_case')
						->rightjoin('citizen', 'report_case.citizen_id', '=', 'citizen.citizen_id')
						->leftjoin('users', 'citizen.user_id', '=', 'users.id')
                        ->leftjoin('image', 'citizen.img_id', '=', 'image.img_id')
						->select(DB::raw('`citizen`.*, `users`.*, `report_case`.`cat_id`, `image`.`img_directory`, `image`.`img_name`, `image`.`img_ext`, count(case when `report_case`.`case_status` = 2 then 1 else null end) as count'))
						->whereRaw($param)
                        ->where('users.deleted', '0')
						->groupBy('citizen.citizen_id')
						->havingRaw('count(case when `report_case`.`case_status` = 2 then 1 else null end) >= 1')
						->paginate(10);
				} else {
					$case = DB::table('citizen')
						->leftjoin('users', 'citizen.user_id', '=', 'users.id')
                        ->leftjoin('image', 'citizen.img_id', '=', 'image.img_id')
						->select(DB::raw('`citizen`.*, `users`.*, `image`.`img_directory`, `image`.`img_name`, `image`.`img_ext`'))
						->whereRaw($param)
                        ->where('users.deleted', '0')
						->paginate(10);
				}
				
			} else {
				if ($request->get('with_invalid')) {
					$case = DB::table('report_case')
						->rightjoin('citizen', 'report_case.citizen_id', '=', 'citizen.citizen_id')
						->leftjoin('users', 'citizen.user_id', '=', 'users.id')
                        ->leftjoin('image', 'citizen.img_id', '=', 'image.img_id')
						->select(DB::raw('`citizen`.*, `users`.*, `report_case`.`cat_id`, `image`.`img_directory`, `image`.`img_name`, `image`.`img_ext`, count(case when `report_case`.`case_status` = 2 then 1 else null end) as count'))
                        ->where('users.deleted', '0')
						->groupBy('citizen.citizen_id')
						->havingRaw('count(case when `report_case`.`case_status` = 2 then 1 else null end) >= 1')
						->paginate(10);
				} else {
					$case = DB::table('citizen')
						->leftjoin('users', 'citizen.user_id', '=', 'users.id')
                        ->leftjoin('image', 'citizen.img_id', '=', 'image.img_id')
						->select(DB::raw('`citizen`.*, `users`.*, `image`.`img_directory`, `image`.`img_name`, `image`.`img_ext`'))
                        ->where('users.deleted', '0')
						->paginate(10);
				}
			}
			return $case;

        } else {
            if ($request->get('field')) {
                return $this->show($citizen_id, $request->get('field'));
            } else {return $this->show($citizen_id); }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        try {

            $user = new User;
            $user->username = $request->input('username'); //(strtolower($request->input('citizen_firstName'). '.' . $request->input('citizen_lastName')));
            $user->email = $request->input('email');
            $user->password = bcrypt( $request->input('password') );
            $user->type = 'citizen';
            $user->isActive = 0;
            $user->save();

            $citizen = new Citizen;
            $citizen->user_id =  $user->id;
            if ($request->input('citizen_firstName')) {
                $citizen->citizen_firstName = $request->input('citizen_firstName');
                $citizen->citizen_lastName = $request->input('citizen_lastName');
                $citizen->citizen_gender = $request->input('citizen_gender');
                $citizen->citizen_DOB = $request->input('citizen_DOB');
            }
            
            //$citizen->citizen_dateTimeReg = $request->input('citizen_dateTimeReg'); -->current timestamp??
            $citizen->citizen_isBlocked = 0;
            $citizen->save();

            //Mail::to($user->email)->send(new NewCitizen($citizen, $user->email));

            return array("status" => "success", 
                         "message" => "Citizen record successfully created.", 
                         "user_id" => $user->id, 
                         "citizen_id" => $citizen->citizen_id, 
                         "email" => $user->email, 
                         "password" => $user->password
            );
        } catch (Exception $e) {
            return array("status" => "error", 
                         "message" => "Email or username has already been used. Please try another."
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $citizen_id
     * @return Response
     */
    public function show($citizen_id, $field='citizen_id') {
        return Citizen::with(['user', 'image'])->where($field, $citizen_id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $citizen_id
     * @return Response
     */
    public function update(Request $request, $citizen_id) {
        $citizen = Citizen::find($citizen_id);
        $user = User::find($citizen->user_id);

        $firstName = $citizen->citizen_firstName;

        $user->email = $request->input('user.email');
        $user->username = $request->input('user.username');
        if ($request->input('user.password')) {
            $user->password = bcrypt( $request->input('user.password') );
        }
        $user->save();

        $citizen->citizen_firstName = $request->input('citizen_firstName');
        $citizen->citizen_lastName = $request->input('citizen_lastName');
        $citizen->citizen_gender = $request->input('citizen_gender');
        $citizen->citizen_DOB = $request->input('citizen_DOB'); 
        $citizen->save();

        if (!$firstName || $firstName == "") {
            $token = hash("sha256", time());

            DB::table('password_resets')->insert(
                ['email' => $user->email, 'token' => $token]
            );

            Mail::to($user->email)->send(new NewCitizen($citizen, $user->email, $token));
        }
        

        return array("status" => "success", "message" => "Citizen record successfully updated.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $citizen_id
     * @return Response
     */
    public function destroy(Request $request, $citizen_id) {
        $citizen = Citizen::find($citizen_id);
        $user = User::find($citizen->user_id);
        $citizen->delete();
        $user->deleted = 1;
        $user->save();

        return array("status" => "success", "message" => "Citizen record successfully deleted.");
    }

    /**
     * Deactivate or activate citizen
     * id is the citizen id
     * deactivate value is boolean. Deactivate is '1'.
     *
     * @param  int  $citizen_id
     * @param  Request  $request
     * @return Response
     */
    public function deactivate(Request $request, $citizen_id) {
        if (!$this->__hasAdminAccess()) {
            return array("status" => "error", "message" => "Failed to deactivate/activate citizen. Access denied.");
        }

        $id = json_decode($request->json('id'));
        $deactivate = json_decode($request->json('deactivate'));
        $citizen = Citizen::find($id);
        $user = User::find($citizen->user_id);

        if ($deactivate) {
            $user->isActive = 0;
        } else {
            $user->isActive = 1;
        }
        $user->save();

        return array("status" => "success", "message" => "Successfully setting citizen isActive.");
    }

    /**
     * Block/unblock the citizen record
     * block values is boolean. block==true
     *
     * @param  int  $citizen_id
     * @param  Request  $request
     * @return Response
     */
    public function block(Request $request, $citizen_id) {
        if (!$this->__hasAdminAccess()) {
            return array("status" => "error", "message" => "Failed to block/unblock citizen. Access denied.");
        }
        $id = json_decode($request->json('id'));
        $block = json_decode($request->json('block'));
        $citizen = Citizen::find($id);

        if ($block) {
            $citizen->citizen_isBlocked = 1;
        } else {
            $citizen->citizen_isBlocked = 0;
        }
        $citizen->save();

        return array("status" => "success", "message" => "Successfully setting citizen's block status.");
    }

    /**
     * Sends a note to a citizen, for now an email
     * $id is the id of the citizen.
     *
     * @param  int  $citizen_id
     * @param  Request  $request
     * @return Response
     */
    public function sendNote(Request $request, $id) {
        if (!$this->__hasAdminAccess()) {
            return array("status" => "error", "message" => "Failed to send note to citizen. Access denied.");
        }

        $citizen = Citizen::find($id);
        $user = User::find($citizen->user_id);
        $user_firebase = FirebaseToken::where('user_id', $citizen->user_id)->first();

        if ($user_firebase) {

            $path_to_firebase_cm = "https://fcm.googleapis.com/fcm/send";
            $fields = array(
                 "to" => $user_firebase->token,
                 "data" => array(
                     "alertTitle" => "Warning",
                     "alertMessage" => "This is a reminder note from the Admin of FixDC that you had already posted multiple ambiguous reports. If we ever receive an ambigous report from you again, we will be considering blocking your account. ",
                     "alertType" => "Warning"
                 ),
                "notification" => array(
                    "click_action" => "FCM_PLUGIN_ACTIVITY",
                    'title' => "FixDC",
                    'body' => "This is a reminder note from the Admin of FixDC that you had already posted multiple ambiguous reports.",
                    "sound" => "default",
                    "icon" =>"fcm_push_icon"
                )
             );

            $headers = array(
                 'Authorization: key=AAAAiRJtRXk:APA91bE--5y4p5MnlllKiccrxPaeP3PvP1rF-IhWcvW8UNbr58Q8wzWyt3YPEcANadzICwXVLI967jpKuH77Cnd_GdL7BGRhMUbgffPdJQSyootDnXUroXfNaDkfRvA6OufZg1UV1YNx',
                 'Content-Type: application/json'
             );

             $ch = curl_init();

             curl_setopt( $ch, CURLOPT_URL, $path_to_firebase_cm );
             curl_setopt( $ch, CURLOPT_POST, true );
             curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
             curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
             curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
             curl_setopt( $ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
             curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ));

             $result = curl_exec( $ch );
             curl_close( $ch );

             $fb_response[] = json_decode( $result, TRUE );

        } else {
            Mail::to($user->email)->send(new SendNote($citizen));
        }
        
        //

        return array("status" => "success", "message" => "Successfully sent a note to citizen");
    }

    /**
     * Upload citizen profile pic
     * 
     *
     * @param  int  $case_id
     * @param  Request  $request
     * @return Response
     */
    public function uploadImage(Request $request, $user_id) {
        $file = array('image' => Input::file('file'));

        $rules = array('image' => 'required', 'image' => 'mimes:jpeg,bmp,png,jpg', 'max' => '10000'); //mimes:jpeg,bmp,png and for max size max:10000
        $validator = Validator::make($file, $rules);

        if ($validator->fails()) {
          return array("status" => "error", "message" => "Error image file type or image is to big.");
        } else {
            if (Input::file('file')->isValid()) {
                $destinationPath = env('UPLOAD_AVATARS', '../storage/app/public/avatars');
                $extension = Input::file('file')->getClientOriginalExtension();
                $fileName = str_random(15);
                $filesize = Input::file('file')->getSize();
                $fileOrigName = Input::file('file')->getClientOriginalName();
                Input::file('file')->move($destinationPath,  $fileName.'.'.$extension); 

                $image = new Image;
                $image->img_directory = $destinationPath . '/';
                $image->img_name = $fileName;
                $image->img_description = $fileOrigName;
                $image->img_ext = $extension;
                $image->img_size = $filesize;
                $image->save();

                $citizen = Citizen::where('user_id', $user_id)->first();

                $image_old = Image::find($citizen->img_id);
                if ($image_old) {
                    Storage::delete('public/avatars/' . $image_old->img_name . '.' . $image_old->img_ext);
                    $image_old->delete();
                }
                

                $citizen->img_id = $image->img_id;
                $citizen->save();

                return array("status" => "success", "message" => "Image successfully uploaded.");

            } else {
                return array("status" => "error", "message" => "Uploaded file is not valid.");
            } 
        }

        return array("status" => "error", "message" => "Upload failed.");
    }

    /**
     * Get the active status of citizen
     *
     * @param  int  $citizen_id
     * @param  Request  $request
     * @return Response
     */
    public function getCitizenIsActive(Request $request, $citizen_id) {
        $citizen = Citizen::find($citizen_id);
        $user = User::find($citizen->user_id);
        return $user->isActive;
    }

    /**
     * Get if the citizen is blocked or not
     *
     * @param  int  $citizen_id
     * @param  Request  $request
     * @return Response
     */
    public function getCitizenisBlocked(Request $request, $citizen_id) {
        $citizen = Citizen::find($citizen_id);
        return $citizen->citizen_isBlocked;
    }

    /**
     * Get citizen details from citizen requesting
     *
     * @param  int  $citizen_id
     * @param  Request  $request
     * @return Response
     */
    public function getCitizenDetails(Request $request, $citizen_id) {
        if ($request->get('field')) {
            $field = $request->get('field');
        } else {
            return $field = $request->get('citizen_id');
        } 
            return Citizen::with(['user', 'image'])->where($field, $citizen_id)->first();
    }

    private function __hasAdminAccess() {
        $user = User::getAuthenticatedUser();
        //var_dump($user['user']['attributes']['type']);
        
        if (!isset($user['user']['attributes']['type'])) {
            return false;
        } else {
            if ($user['user']['attributes']['type'] == "admin") {
                return true;
            }
        }
        return false;
    }



}


?>