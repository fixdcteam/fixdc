<?php

namespace App\Http\Controllers;

use App\User;
use App\Agent;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewAgent;

class AgentController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct() {
        //$this->middleware('auth');
        //$this->middleware('jwt.auth');
        $this->middleware('jwt.auth', ['except' => ['__hasAdminAccess', '__hasSuperAdminAccess']]);
    }

    /**
     * Display a listing of the resource.
     * agent-user-id -> user id of the agent to get agent details
     * search -> search term
     *
     * @param Request $request
     * @param  int  $agent_id
     * @return Response
     */
    public function index(Request $request, $agent_id = null) {
        if (!$this->__hasAdminAccess() && $agent_id == null) {
            return array("status" => "error", "message" => "Failed to fetch agency list. Access denied.");
        }

        if ($agent_id == null) {
            $param = array();
            $paramSearch = "";
            $search = "";
            if($request->get('search')) {
                $search = $request->get('search');
            }

            if($request->get('agent-user-id')) {
                $agent = Agent::where('user_id', $request->get('agent-user-id'))->first();
                $param['agency_id'] = $agent->agency_id;
                return Agent::with(['user', 'agency'])
                    ->whereRaw("agency_id = ? AND (agent_firstName LIKE ? OR agent_lastName LIKE ?)", array($param['agency_id'], '%'.$search.'%','%'.$search.'%'))
                    ->orderBy('agent_id', 'asc')
                    ->paginate(10);
            }

            if($request->get('search') || $request->get('agent-user-id')){
                return Agent::with(['user', 'agency'])
                    ->whereRaw("agent_firstName LIKE ? OR agent_lastName LIKE ?", array('%'.$search.'%','%'.$search.'%'))
                    ->orderBy('agent_id', 'asc')
                    ->paginate(10);
            } else {
                return Agent::with(['user', 'agency'])->orderBy('agent_id', 'asc')->paginate(10);
            }

        } else {
			if ($request->get('field')) {
                return $this->show($agent_id, $request->get('field'));
            } else {return $this->show($agent_id); }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        if (!$this->__hasAdminAccess()) {
            return array("status" => "error", "message" => "Failed to create new agent record. Access denied.");
        }

        $user = new User;
        $agency = $request->input('agency');

        $user->username = (strtolower($request->input('agent_firstName'). '.' . $request->input('agent_lastName')));
        $user->email = $request->input('user.email');
        //$user->password = bcrypt( $request->input('user.password') );
        $user->password = bcrypt( '123456' );
        $user->type = 'agent';
        $user->isActive = 1;
        $user->save();

        $agent = new Agent;
        $agent->user_id =  $user->id;
        $agent->agency_id = $agency['agency_id'];
        $agent->agent_firstName = $request->input('agent_firstName');
        $agent->agent_lastName = $request->input('agent_lastName');
        $agent->agent_gender = $request->input('agent_gender');
        $agent->agent_cont1 = $request->input('agent_cont1');
        $agent->agent_cont2 = $request->input('agent_cont2');
        $agent->agent_posTitle = $request->input('agent_posTitle');
        $agent->agent_sysPos = $request->input('agent_sysPos');
        $agent->agent_isAdmin = 0;

        Mail::to($user->email)->send(new NewAgent($agent, $user->email));

        $agent->save();

        return array("status" => "success", "message" => "Agent record successfully created with id ". $agent->agent_id . 
                     ". Email has been sent to the agent which includes the details of the account.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $agent_id
     * @return Response
     */
    public function show($agent_id, $field='agent_id') {
        return agent::with(['user', 'agency'])->where($field, $agent_id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {
        if (!$this->__hasAdminAccess()) {
            return array("status" => "error", "message" => "Failed to update agent record. Access denied.");
        }

        if ($request->input('field')) {
            $agent = Agent::where($request->input('field'), $id)->first();
            $user = User::find($agent->user_id);
        } else {
            $agent = Agent::find($id);
            $user = User::find($agent->user_id);
        }

        $agency = $request->input('agency');

        $user->email = $request->input('user.email');
        $user->username = $request->input('user.username');
        if ($request->input('user.password')) {
            $user->password = bcrypt( $request->input('user.password') );
        }
        $user->save();

        $agent->agency_id = $agency['agency_id'];
        $agent->agent_firstName = $request->input('agent_firstName');
        $agent->agent_lastName = $request->input('agent_lastName');
        $agent->agent_gender = $request->input('agent_gender');
        $agent->agent_cont1 = $request->input('agent_cont1');
        $agent->agent_cont2 = $request->input('agent_cont2');
        $agent->agent_posTitle = $request->input('agent_posTitle');
        $agent->agent_sysPos = $request->input('agent_sysPos');
        $agent->save();

        return array("status" => "success", "message" => "Agent record successfully updated.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $agent_id
     * @return Response
     */
    public function destroy(Request $request, $agent_id) {
        $agent = Agent::find($agent_id);
        $user = User::find($agent->user_id);
        $agent->delete();
        $user->deleted = 1;
        $user->save();

        return array("status" => "success", "message" => "Agent record successfully deleted.");
    }

    /**
     * Deactivate or activate agent
     * id is the agent id
     * deactivate value is boolean. Deactivate is '1'.
     *
     * @param  int $agent_id
     * @param  Request  $request
     * @return Response
     */
    public function deactivate(Request $request, $agent_id) {
        if (!$this->__hasAdminAccess()) {
            return array("status" => "error", "message" => "Failed to deactivate agent. Access denied.");
        }

        $id = json_decode($request->json('id'));
        $deactivate = json_decode($request->json('deactivate'));
        $agent = Agent::find($id);
        $user = User::find($agent->user_id);
        $message = "";
        if ($deactivate) {
            $user->isActive = 0;
            $message = "Agent is successfully deactivated.";
        } else {
            $user->isActive = 1;
            $message = "Agent is successfully activated.";
        }
        $user->save();

        return array("status" => "success", "message" => $message);
    }

    /**
     * Set agent as an admin to an agency or not
     * id is the agent id
     * makeAdmin value is boolean. 1 is to make admin
     *
     * @param  int $agent_id
     * @param  Request  $request
     * @return Response
     */
    public function makeAdmin(Request $request, $agent_id) {
        if (!$this->__hasAdminAccess()) {
            return array("status" => "error", "message" => "Failed to set agent as an admin. Access denied.");
        }

        $id = json_decode($request->json('id'));
        $makeAdmin = json_decode($request->json('makeAdmin'));
        $agent = Agent::find($id);
        $agent->agent_isAdmin = $makeAdmin;
        $agent->save();
        if ($makeAdmin) {
            return array("status" => "success", "message" => "Success setting agent as an admin.");
        } else {
            return array("status" => "success", "message" => "Success removing admin rights of agent.");
        }

    }

    /**
     * Get agent_isActive
     *
     * @param  int  $agent_id
     * @param  Request  $request
     * @return Response
     */
    public function getAgentIsActive(Request $request, $agent_id) {
        $agent = Agent::find($agent_id);
        return $agent->agent_isActive;
    }
	
	/**
     * Get agent's agency_id
     *
     * @param  int  $user_id
     * @param  Request  $request
     * @return Response
     */
    public function getAgencyID(Request $request, $user_id) {
        $agent = Agent::where('user_id', $user_id)->first();
        return '{"agency_id":"' . $agent->agency_id . '"}';
    }
	
    private function __hasAdminAccess() {
        $user = User::getAuthenticatedUser();
        $agent = Agent::where("user_id", $user['user']['attributes']['id'])->first();
        //var_dump($user['user']['attributes']);
        
        if (!isset($user['user']['attributes']['type'])) {
            return false;
        } else {
            if ($user['user']['attributes']['type'] == "admin" || $agent->agent_isAdmin == 1) {
                return true;
            }
        }
        return false;
    }

    private function __hasSuperAdminAccess() {
        $user = User::getAuthenticatedUser();
        //var_dump($user['user']['attributes']['type']);
        
        if (!isset($user['user']['attributes']['type'])) {
            return false;
        } else {
            if ($user['user']['attributes']['type'] == "admin") {
                return true;
            }
        }
        return false;
    }
	


}


?>