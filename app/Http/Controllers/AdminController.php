<?php

namespace App\Http\Controllers;

use App\Admin;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewAdmin;

class AdminController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct() {
        //$this->middleware('auth');
        $this->middleware('jwt.auth', ['except' => ['checkAccess', 'hasAccess', '__hasAdminAccess']]);
        $this->middleware('admin');

    }

    /**
     * Check access.
     *
     * @param int  $id
     * @param Request  $request
     * @return Response
     */
    public function checkAccess(Request $request, $id) {
        return $this->hasAccess($id);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request, $id = null) {
        if (!$this->__hasAdminAccess()) {
            return array("status" => "error", "message" => "Failed to get admin record. Access denied.");
        }

        if ($id == null) {
            if($request->get('search')){
                return Admin::with(['user'])->where("first_name", "LIKE", "%{$request->get('search')}%")->orderBy('id', 'asc')->paginate(10);//->get();
            } else {
                //return Admin::orderBy('id', 'asc')->get();
                return Admin::with(['user'])->orderBy('id', 'asc')->paginate(10);
            }
        } else {
            if ($request->get('field')) {
                return $this->show($id, $request->get('field'));
            } else {return $this->show($id); }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        if (!$this->__hasAdminAccess()) {
            return array("status" => "error", "message" => "Failed to create new admin record. Access denied.");
        }

        $user = new User;
        $user->username = (strtolower($request->input('first_name'). '.' . $request->input('last_name')));
        $user->email = $request->input('user.email');
        //$user->password = bcrypt( $request->input('user.password') );
        $user->password = bcrypt( '123456' );
        $user->type = 'admin';
        $user->save();

        $admin = new Admin;
        $admin->user_id =  $user->id;
        $admin->first_name = $request->input('first_name');
        $admin->last_name = $request->input('last_name');
        $admin->save();

        Mail::to($user->email)->send(new NewAdmin($admin, $user->email));
        
        return array("status" => "success", 
                     "message" => "Admin record successfully created. Email has been sent to the new user containing the account details.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id, $field='id') {
        if (!$this->__hasAdminAccess()) {
            return array("status" => "error", "message" => "Failed to get admin record. Access denied.");
        }

        return Admin::with(['user'])->where($field, $id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {
        if (!$this->__hasAdminAccess()) {
            return array("status" => "error", "message" => "Failed to update admin record. Access denied.");
        }

        if ($request->input('field')) {
            $admin = Admin::where($request->input('field'), $id)->first();
            $user = User::find($admin->user_id);
        } else {
            $admin = Admin::find($id);
            $user = User::find($admin->user_id);
        }
        //$user->username = $request->input('user.username');
        $user->email = $request->input('user.email');
        if (null !== $request->input('user.password')) {
            $user->password = bcrypt( $request->input('user.password') );
        }
        $user->save();

        $admin->first_name = $request->input('first_name');
        $admin->last_name = $request->input('last_name');
        $admin->save();

        return array("status" => "success", 
                     "message" => "Admin record successfully updated.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request, $id) {
        if (!$this->__hasAdminAccess()) {
            return array("status" => "error", "message" => "Failed to delete admin record. Access denied.");
        }

        $admin = Admin::find($id);
        $user = User::find($admin->user_id);
        $admin->delete();
        $user->deleted = 1;
		$user->save();

        return array("status" => "success", "message" => "Admin record successfully deleted.");
    }

    /**
     * Deactivate or activate citizen
     * id is the admin id
     * deactivate value is boolean. Deactivate is '1'.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return Response
     */
    public function deactivate(Request $request, $id) {
        if (!$this->__hasAdminAccess()) {
            return array("status" => "error", "message" => "Failed to update admin record. Access denied.");
        }
        $id = json_decode($request->json('id'));
        $deactivate = json_decode($request->json('deactivate'));
        $admin = Admin::find($id);
        $user = User::find($admin->user_id);

        if ($deactivate) {
            $user->isActive = 0;
            $msg = 'deactivated';
        } else {
            $user->isActive = 1;
            $msg = 'activated';
        }

        $user->save();

        return array("status" => "success", "message" => "Admin record successfully " . $msg . ".");
    }


    /**
     * Check access.
     *
     * @param int  $id
     * @return Response
     */
    public function hasAccess($id = null) {
        $user = User::find($id);
        if ($user->type != 'admin') {
            return 'false';
        } else {
            return 'true';
        }
    }

    private function __hasAdminAccess() {
        $user = User::getAuthenticatedUser();
        //var_dump($user['user']['attributes']['type']);
        
        if (!isset($user['user']['attributes']['type'])) {
            return false;
        } else {
            if ($user['user']['attributes']['type'] == "admin") {
                return true;
            }
        }
        return false;
    }
}


?>