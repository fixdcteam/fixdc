<?php

namespace App\Http\Controllers;

use App\Admin;
use App\User;
use App\ReportCase;
use App\CaseCategory;
use App\Agent;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CaseCategoryController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct() {
        //$this->middleware('auth');
        $this->middleware('jwt.auth', ['except' => [ '__hasAdminAccess']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $cat_id
     * @param Request $request
     * @return Response
     */
    public function index(Request $request, $cat_id = null) {
        if ($cat_id == null) {
            $param_or = array();
            if ($request->get('agency-id')) {
                return CaseCategory::with(['agency'])
                    ->where('cat_deleted', '0')
                    ->whereIn('agency_id', ['0', $request->get('agency-id')])
                    ->orderBy('cat_id', 'asc')
                    ->get();
            } else {
                return CaseCategory::with(['agency'])
                    ->where('cat_deleted', '0')
                    //->where('agency_id', '0')
                    ->orderBy('cat_id', 'asc')
                    ->get();
            }
            //return CaseCategory::with(['agency'])->orderBy('cat_id', 'asc')->get();
        } else {
            return $this->show($cat_id);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        if (!$this->__hasAdminAccess()) {
            return array("status" => "error", "message" => "Failed to create new category. Access denied.");
        }

        $category = new CaseCategory;
        $category->cat_name = $request->input('cat_name');
        if ($request->get('agency-id')) {
            $category->agency_id = $request->get('agency-id');
        }

        $category->save();
        return array("status" => "success", "message" => "Case category record successfully created with id ". $category->cat_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $cat_id
     * @return Response
     */
    public function show($cat_id) {
        return CaseCategory::with(['agency'])->where('cat_id', $cat_id)->first();
        //return CaseCategory::with(['agency'])->where('cat_id', $cat_id)->orderBy('cat_id', 'asc')->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $cat_id
     * @return Response
     */
    public function update(Request $request, $cat_id) {
        if (!$this->__hasAdminAccess()) {
            return array("status" => "error", "message" => "Failed to update category. Access denied.");
        }      

        $category = CaseCategory::find($cat_id);

        if ($request->input('agency')) {
            $agency_id = $request->input('agency');
            $category->agency_id = $agency_id['agency_id'];
        }

        //$category->cat_code = $request->input('cat_code');
        $category->cat_name = $request->input('cat_name');
        $category->save();

        return array("status" => "success", "message" => "Case category record successfully updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $cat_id
     * @return Response
     */
    public function destroy(Request $request, $cat_id) {
        if (!$this->__hasAdminAccess()) {
            return array("status" => "error", "message" => "Failed to delete category. Access denied.");
        }  

        $category = CaseCategory::find($cat_id);
        $category->cat_deleted = '1';
        $category->save();

        return array("status" => "success", "message" => "Case category record successfully deleted.");
    }

    private function __hasAdminAccess() {
        $user = User::getAuthenticatedUser();
        $agent = Agent::where("user_id", $user['user']['attributes']['id'])->first();
        
        if (!isset($user['user']['attributes']['type'])) {
            return false;
        } else {
            if ($user['user']['attributes']['type'] == "admin" || $agent->agent_isAdmin == 1) {
                return true;
            }
        }

        return false;
    }

}


?>