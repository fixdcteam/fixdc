<?php

namespace App\Http\Controllers;

use App\Agency;
use App\User;
use App\Agent;
use Illuminate\Http\Request;
//use Illuminate\Routing\Controller;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AgencyController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct() {
        //$this->middleware('auth');
        $this->middleware('jwt.auth', ['except' => [ '__hasAdminAccess', '__hasSuperAdminAccess']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param  int  $agency_id
     * @return Response
     */
    public function index(Request $request, $agency_id = null) {
        if (!$this->__hasSuperAdminAccess() && !$request->get('all')  && $agency_id == null) {
            return array("status" => "error", "message" => "Failed to fetch agency list. Access denied.");
        }
        if ($agency_id == null) {
            if($request->get('all')) {
                return Agency::where('agency_deleted', '0')->orderBy('agency_id', 'asc')->get();
            }
            if($request->get('search')){
                return Agency::where("agency_name", "LIKE", "%{$request->get('search')}%")->where("agency_deleted", "0")->orderBy('agency_id', 'asc')->paginate(10);
            } else {
                return Agency::where('agency_deleted', '0')->orderBy('agency_id', 'asc')->paginate(10);
            }
        } else {
            return $this->show($agency_id);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        if (!$this->__hasSuperAdminAccess()) {
            return array("status" => "error", "message" => "Failed to create new agency record. Access denied.");
        }

        $agency = new Agency;

        $agency->agency_name = $request->input('agency_name');
        $agency->agency_address = $request->input('agency_address');
        $agency->agency_telNo1 = $request->input('agency_telNo1');
        $agency->agency_telNo2 = $request->input('agency_telNo2');
        $agency->agency_email = $request->input('agency_email');
        $agency->save();

        return array("status" => "success", "message" => "Agency record successfully created.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $agency_id
     * @return Response
     */
    public function show($agency_id) {
        return Agency::where("agency_id", $agency_id)->first();
        //$model = User::where('votes', '>', 100)->firstOrFail();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $agency_id
     * @return Response
     */
    public function update(Request $request, $agency_id) {
        if (!$this->__hasAdminAccess()) {
            return array("status" => "error", "message" => "Failed to update agency record. Access denied.");
        }

        $agency = Agency::findorFail($agency_id);

        $agency->agency_name = $request->input('agency_name');
        $agency->agency_address = $request->input('agency_address');
        $agency->agency_telNo1 = $request->input('agency_telNo1');
        $agency->agency_telNo2 = $request->input('agency_telNo2');
        $agency->agency_email = $request->input('agency_email');
        $agency->save();

        return array("status" => "success", "message" => "Agency record successfully updated.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $agency_id
     * @return Response
     */
    public function destroy($agency_id) {
        if (!$this->__hasAdminAccess()) {
            return array("status" => "error", "message" => "Failed to delete agency record. Access denied.");
        }
        $agency = Agency::find($agency_id);
        $agency->agency_deleted = 1;
        $agency->save();

        return array("status" => "success", "message" => "Agency record successfully deleted.");
    }
    
    private function __hasAdminAccess() {
        $user = User::getAuthenticatedUser();
        $agent = Agent::where("user_id", $user['user']['attributes']['id'])->first();
        //var_dump($user['user']['attributes']);
        
        if (!isset($user['user']['attributes']['type'])) {
            return false;
        } else {
            if ($user['user']['attributes']['type'] == "admin" || $agent->agent_isAdmin == 1) {
                return true;
            }
        }
        return false;
    }

    private function __hasSuperAdminAccess() {
        $user = User::getAuthenticatedUser();
        //var_dump($user['user']['attributes']['type']);
        
        if (!isset($user['user']['attributes']['type'])) {
            return false;
        } else {
            if ($user['user']['attributes']['type'] == "admin") {
                return true;
            }
        }
        return false;
    }
}


?>