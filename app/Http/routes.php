<?php
/*
    use Illuminate\Http\Request;
    use App\Mail\NewAdmin;

    $API_URL = 'api/v1';

    Route::get('/', function () {
        return view('app');
    });

    Route::auth();
/*
    Route::get('/home', 'HomeController@index');

    Route::get('/tasks', 'TaskController@index');
    Route::post('/task', 'TaskController@store');
    Route::delete('/task/{task}', 'TaskController@destroy');
/*
    Route::get('/employees/fuck', function () {
        return view('employees.index');
    });*/

/*
    Route::group(['prefix' => 'api'], function() {
        Route::resource('authenticate', 'AuthenticateController', ['only' => ['index']]);
        Route::post('authenticate', 'AuthenticateController@authenticate');
        Route::get('authenticate/user', 'AuthenticateController@getAuthenticatedUser');
    });

    Route::get('protected', ['middleware' => ['auth', 'admin'], function() {
        return "this page requires that you be logged in and an Admin";
    }]);

    Route::group(['middleware' => ['web']], function () {
        Route::resource('admins', 'AdminController');
    });

    Route::get($API_URL . '/admins/{id?}', 'AdminController@index');
    Route::post($API_URL . '/admins', 'AdminController@store');
    Route::post($API_URL . '/admins/{id}', 'AdminController@update');
    Route::delete($API_URL . '/admins/{id}', 'AdminController@destroy');
    Route::post($API_URL . '/admins/deactivate/{id?}', 'AdminController@deactivate');
    Route::get($API_URL . '/admins/check-access/{id?}', 'AdminController@checkAccess');

    Route::get($API_URL . '/citizens/{id?}', 'CitizenController@index');
    Route::post($API_URL . '/citizens', 'CitizenController@store');
    Route::post($API_URL . '/citizens/{id}', 'CitizenController@update');
    Route::delete($API_URL . '/citizens/{id}', 'CitizenController@destroy');
    Route::post($API_URL . '/citizens/deactivate/{id}', 'CitizenController@deactivate');
    Route::post($API_URL . '/citizens/block/{id?}', 'CitizenController@block');
    Route::get($API_URL . '/citizens/getIsActive/{id?}', 'CitizenController@getCitizenIsActive');
    Route::get($API_URL . '/citizens/getIsBlocked/{id?}', 'CitizenController@getCitizenisBlocked');

    Route::get($API_URL . '/reportcases/{id?}', 'ReportCaseController@index');
    Route::post($API_URL . '/reportcases', 'ReportCaseController@store');
    Route::post($API_URL . '/reportcases/{id}', 'ReportCaseController@update');
    Route::delete($API_URL . '/reportcases/{id}', 'ReportCaseController@destroy');
    Route::post($API_URL . '/reportcases/setNoOfConcern/{id}', 'ReportCaseController@setNoOfConcern');
    Route::post($API_URL . '/reportcases/setIsPrivate/{id}', 'ReportCaseController@setIsPrivate');
    Route::post($API_URL . '/reportcases/setStatus/{id}', 'ReportCaseController@setStatus');
    Route::get($API_URL . '/reportcases/getstatus/{id}', 'ReportCaseController@getstatus');
    Route::get($API_URL . '/reportcases/getIsPrivate/{id}', 'ReportCaseController@getIsPrivate');
    Route::get($API_URL . '/reportcases/getCasesFromCitizen/{id}', 'ReportCaseController@getCasesFromCitizen');

    Route::get($API_URL . '/agents/{id?}', 'AgentController@index');
    Route::post($API_URL . '/agents', 'AgentController@store');
    Route::post($API_URL . '/agents/{id}', 'AgentController@update');
    Route::delete($API_URL . '/agents/{id}', 'AgentController@destroy');
    Route::post($API_URL . '/agents/deactivate/{id}', 'AgentController@deactivate');
    Route::get($API_URL . '/agents/getAgentIsActive/{id}', 'AgentController@getAgentIsActive');

    Route::get($API_URL . '/agencies/{id?}', 'AgencyController@index');
    Route::post($API_URL . '/agencies', 'AgencyController@store');
    Route::post($API_URL . '/agencies/{id}', 'AgencyController@update');
    Route::delete($API_URL . '/agencies/{id}', 'AgencyController@destroy');

    Route::get($API_URL . '/categories/{id?}', 'CaseCategoryController@index');
    Route::post($API_URL . '/categories', 'CaseCategoryController@store');
    Route::post($API_URL . '/categories/{id}', 'CaseCategoryController@update');
    Route::delete($API_URL . '/categories/{id}', 'CaseCategoryController@destroy');


    Route::get($API_URL . '/send-mail', function (Request $request) {
        if ($request->get('to')) {
            Mail::to($request->get('to'))->send(new NewAdmin);
            return true;
            //return view('welcome');
        }
    });






    Route::get($API_URL . '/employees/{id?}', 'Employees@index');
    Route::post($API_URL . '/employees', 'Employees@store');
    Route::post($API_URL . '/employees/{id}', 'Employees@update');
    Route::delete($API_URL . '/employees/{id}', 'Employees@destroy');



