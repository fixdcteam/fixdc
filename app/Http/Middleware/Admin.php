<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\AuthenticateController;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class Admin {

    public function handle($request, Closure $next)
    {
        /*$authenticate = new AuthenticateController;
        $user = json_decode($authenticate->getAuthenticatedUser());
        //if ( Auth::check() && Auth::user()->isAdmin() )
        if ($user['type'] == 'admin') {
            return $next($request);
        }
        return redirect('/');
        */

        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        // the token is valid and we have found the user via the sub claim
        //return response()->json(compact('user'));
        $user1 = json_encode(compact('user'));
        $user2 = json_decode($user1);
        if ($user2->user->type == 'admin') {
            return $next($request);
        } else {
            return response()->json(['user_not_admin'], 404);
            //return $user1;
        }


    }

}