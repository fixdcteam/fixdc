<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    protected $table = 'agency';
    protected $primaryKey = 'agency_id';
    public $timestamps = false;

    protected $fillable = array('agency_id', 'agency_name', 'agency_address', 'agency_teNo1	', 'agency_teNo2', 'agency_email', 'agency_deleted');

    public function case_category() {
        return $this->hasMany('App\CaseCategory');
    }

    public function agent() {
        return $this->hasMany('App\Agent');
    }

    public function report_case() {
        return $this->hasMany('App\ReportCase');
    }




}

