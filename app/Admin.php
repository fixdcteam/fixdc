<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admin extends Model
{
	use SoftDeletes;
	
    protected $table = 'admin';
    protected $primaryKey = 'id';
    protected $foreignKey = 'user_id';
    public $timestamps = false;

    protected $fillable = array('id', 'user_id', 'first_name', 'last_name', 'deleted_at');
	
	protected $dates = ['deleted_at'];


    public function user() {
        return $this->belongsTo('App\User');
    }


}

