<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

class Image extends Model
{
    protected $table = 'image';
    protected $primaryKey = 'img_id';
    //protected $foreignKey = 'agency_id';
    public $timestamps = false;

    protected $fillable = array('img_id', 'img_directory', 'img_name', 'img_description', 'img_ext', 'img_size');

    public function case_image() {
        return $this->hasOne('App\CaseImage');
    }

    public function citizen() {
        return $this->hasMany('App\Citizen');
    }


}

