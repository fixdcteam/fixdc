<!-- resources/views/citizens/edit-citizen.blade.php -->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="loading-loader pull-right">Loading...</div>
                    <div class="error-loading-container pull-right">Error loading. Please reload.</div>
                    <h2 id="page-header">
                        <a href="" id="btn-back" class="link" ng-click="go('/citizens/list')"> <img src="img/back1600.png" > Registered Citizens </a> 
                        <% form_title %>Citizen Details
                    </h2>
                </div>
                <div class="panel-body">
                    <div style="max-width:700px">
                        <form name="frmCitizens" class="form-horizontal" novalidate="">

                            <div class="form-group error">
                                <label for="firstName" class="col-sm-3 control-label">First Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control has-error" id="firstName" name="firstName" placeholder="First Name" value="<% citizen_firstName %>"
                                           ng-model="citizen.citizen_firstName" ng-required="true">
                                    <span class="help-inline" ng-show="frmCitizens.firstName.$invalid && frmCitizens.firstName.$touched">Please enter first name (required).</span>
                                </div>
                            </div>

                            <div class="form-group error">
                                <label for="lastName" class="col-sm-3 control-label">last Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control has-error" id="lastName" name="lastName" placeholder="Last Name" value="<% citizen_lastName %>"
                                           ng-model="citizen.citizen_lastName" ng-required="true">
                                    <span class="help-inline" ng-show="frmCitizens.lastName.$invalid && frmCitizens.lastName.$touched">Please enter last name (required).</span>
                                </div>
                            </div>

                            <div class="form-group error">
                                <label for="gender" class="col-sm-3 control-label">Gender</label>
                                <div class="col-sm-9">
                                    <label class="btn btn-default">
                                        <input name="gender" value="male" type="radio" ng-model="citizen.citizen_gender" >Male
                                    </label>
                                    <label class="btn btn-default">
                                        <input name="gender" value="female" type="radio" ng-model="citizen.citizen_gender">Female
                                    </label>

                                </div>
                            </div>

                            <div class="form-group error">
                                <label for="birthdate" class="col-sm-3 control-label">Date of Birth</label>
                                <div class="col-sm-9">
                                    <input type="date" class="form-control has-error" id="birthdate" name="birthdate" placeholder="Birth date"
                                           ng-model="citizen.citizen_DOB" ng-required="true">
                                    <span class="help-inline" ng-show="frmCitizens.citizen_DOB.$invalid && frmCitizens.citizen_DOB.$touched">Birth date field is required</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="mail" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="<% email %>"
                                           ng-model="citizen.user.email" ng-required="true">
                                            <span class="help-inline"
                                                  ng-show="frmCitizens.email.$invalid && frmCitizens.email.$touched">Valid Email field is required</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="username" class="col-sm-3 control-label">User Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="<% username %>"
                                           ng-model="citizen.user.username" ng-required="true">
                                        <span class="help-inline"
                                              ng-show="frmCitizens.citizen_username.$invalid && frmCitizens.citizen_username.$touched">Contact number field is required</span>
                                </div>
                            </div>

                            <!-- <div class="form-group error">
                                <label for="password" class="col-sm-3 control-label">Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control has-error" id="password" name="password" placeholder="Password" value="<% password %>"
                                           ng-model="citizen.user.password" ng-required="true">
                                                            <span class="help-inline"
                                                                  ng-show="frmCitizens.password.$invalid && frmCitizens.password.$touched">Password field is required</span>
                                </div>
                            </div> -->


                        </form>

                        <div class="modal-footer">
                            <button type="button" class="btn-custom btn-info-custom" id="btn-save" ng-click="save()" ng-disabled="frmCitizens.$invalid">Save changes</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
