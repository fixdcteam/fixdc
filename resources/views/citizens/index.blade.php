<!-- resources/views/employees/index.blade.php -->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div  class="col-sm-6 pull-left">
                            <div class="pull-left" style="margin-right:20px"><h2 id="page-header"><% headerTitle %></h2></div>
                            <div class="loading-loader pull-left" style="margin-top:30px" >Loading....</div>
                            <div class="error-loading-container pull-left" style="margin-top:30px">Error loading. Please reload.</div>
                        </div>
                        <div class="col-sm-6 pull-right">
                            <div class="pull-right" style="padding-top:18px">
								<button id="btn-add" class="btn-custom btn-success-custom" ng-click="pageChanged(currentPage)"> Refresh </button>
                                <button id="btn-add" class="btn btn-success-custom" ng-click="toggle('add', '0')" style="display:none;">Curva eng eng > </button>
                            </div>
                            <div class="pull-right box-tools" style="display:inline-table; padding-top:20px; margin-right:7px">
                                <div class="input-group">
                                    <input type="text" class="form-control  ng-valid ng-dirty" placeholder="Search" ng-keyup="$event.keyCode == 13 ? searchDB() : ''" ng-model="searchText" name="table_search" title="" tooltip="" data-original-title="Min character length is 3">
                                    <span class="input-group-addon">Search</span>
                                    <span class="input-group-btn-custom" style="display:none">
                                        <button type="submit" class="btn-custom btn-info-custom">Search</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
					<span class="filter-label">Showing:</span>
					<div class="btn-group">
                        <label class="btn-custom btn-outline btn-outline-success" btn-radio="''" ng-change="applyFilter()" ng-model="paramWithInvalid"> All </label>
                        <label class="btn-custom btn-outline btn-outline-success" btn-radio="'&with_invalid=1'" ng-change="applyFilter()" ng-model="paramWithInvalid"> Citizens with invalid reports </label>
                    </div>
					<span class="filter-label">Status: </span>
					<div class="btn-group">
                        <label class="btn-custom btn-outline btn-outline-success" btn-radio="''" ng-change="applyFilter()" ng-model="paramStatus"> All </label>
                        <label class="btn-custom btn-outline btn-outline-success" btn-radio="'&isActive=1'" ng-change="applyFilter()" ng-model="paramStatus"> Active </label>
						<label class="btn-custom btn-outline btn-outline-success" btn-radio="'&isActive=2'" ng-change="applyFilter()" ng-model="paramStatus"> Inactive </label>
                    </div>
					<span class="filter-label">Blocked: </span>
					<div class="btn-group">
                        <label class="btn-custom btn-outline btn-outline-success" btn-radio="''" ng-change="applyFilter()" ng-model="paramBlocked"> All </label>
                        <label class="btn-custom btn-outline btn-outline-success" btn-radio="'&citizen_isBlocked=2'" ng-change="applyFilter()" ng-model="paramBlocked"> Allowed </label>
						<label class="btn-custom btn-outline btn-outline-success" btn-radio="'&citizen_isBlocked=1'" ng-change="applyFilter()" ng-model="paramBlocked"> Blocked </label>
					</div>
                    <div style="margin-top:20px;">
						<% citizens.data %>
                        <table class="table table-bordered pagin-table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th ng-if="paramWithInvalid != ''"># Invalid Reports</th>
                                <th>Signup</th>
                                <th>Options</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr dir-paginate="citizen in citizens | itemsPerPage:10" total-items="totalItems">
                                <td><% citizen.citizen_firstName %> <% citizen.citizen_lastName %></td>
                                <td><% citizen.email %></td>
                                <td ng-if="paramWithInvalid != ''"> <b> <% citizen.count %> </b> <button class="btn-custom btn-sm btn-outline btn-outline-default" ng-click="sendNote(citizen.citizen_id)" style="float:right;">Send Note</button></td>
                                <!--<td><% citizen.citizen_address %></td>-->
                                <!--<td><% citizen.citizen_dateTimeReg | date: 'MM-dd-yyyy' %></td>-->
                                <td> <span ng-bind="dateString(citizen.citizen_dateTimeReg)"> </span> <% dateObject | date: "MMM d, y" %> </td> 
                                <td>
                                    <button class="btn-custom btn-sm btn-outline btn-outline-default" ng-click="go('/citizens/edit/' + citizen.citizen_id)">Edit</button>
									<button class="btn-custom btn-sm btn-outline btn-outline-default" ng-click="go('/citizens/cases/' + citizen.citizen_id + '/' + citizen.citizen_firstName + '-' + citizen.citizen_lastName)">Reported cases</button>
                                    <button class="btn-custom btn-sm btn-danger-custom" ng-click="confirmDelete(citizen.citizen_id)">Delete</button>
                                </td> <td>
                                    <button class="btn-custom btn-sm btn-success-custom" ng-click="confirmDeactivate(citizen.citizen_id, citizen.isActive)" ng-if="citizen.isActive">Active</button>
                                    <button class="btn-custom btn-sm btn-danger-custom"  ng-click="confirmDeactivate(citizen.citizen_id, citizen.isActive)" ng-if="!citizen.isActive">Deactivated</button>
                                    <button class="btn-custom btn-sm btn-success-custom" ng-click="confirmBlocked(citizen.citizen_id, citizen.citizen_isBlocked)" ng-if="!citizen.citizen_isBlocked">Allowed</button>
                                    <button class="btn-custom btn-sm btn-danger-custom" ng-click="confirmBlocked(citizen.citizen_id, citizen.citizen_isBlocked)" ng-if="citizen.citizen_isBlocked">Blocked</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <dir-pagination-controls on-page-change="pageChanged(newPageNumber)" template-url="../resources/views/templates/dirPagination.html" > </dir-pagination-controls>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="SendNoteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel"><% modal_title %></h4>
            </div>
            <div class="modal-body">
                <% modal_content %>

            </div>
            <div class="modal-footer">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>
