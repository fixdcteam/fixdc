<!-- resources/views/admins/edit-admin.blade.php -->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="loading-loader pull-right">Loading...</div>
                    <div class="error-loading-container pull-right">Error loading. Please reload.</div>
                    <h2 id="page-header">Admin Profile</h2>
                </div>
                <div class="panel-body">
                    <div style="max-width:700px">
                        <form name="frmAdmin" class="form-horizontal" novalidate="">

                            <div class="form-group error">
                                <label for="First Name" class="col-sm-3 control-label">First Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control has-error" id="first_name" name="first_name" placeholder="First Name" value="<% first_name %>"
                                           ng-model="admin.first_name" ng-required="true">
                                    <span class="help-inline" ng-show="frmAdmin.first_name.$invalid && frmAdmin.first_name.$touched">Please enter the First Name (required).</span>
                                </div>
                            </div>

                            <div class="form-group error">
                                <label for="Last Name" class="col-sm-3 control-label">Last Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control has-error" id="last_name" name="last_name" placeholder="Last Name" value="<% last_name %>"
                                           ng-model="admin.last_name" ng-required="true">
                                                                <span class="help-inline"
                                                                      ng-show="frmAdmin.last_name.$invalid && frmAdmin.last_name.$touched">Please enter the Last Name (required).</span>
                                </div>
                            </div>

                            <div class="form-group error">
                                <label for="Email" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control has-error" id="email" name="email" placeholder="Email" value="<% email %>"
                                           ng-model="admin.user.email" ng-required="true">
                                    <span class="help-inline" ng-show="frmAdmin.email.$invalid && frmAdmin.email.$touched">Valid email address is required.</span>
                                </div>
                            </div>
                        </form>
                        <div class="modal-footer">
                            <button type="button" class="btn-custom btn-info-custom" id="btn-save" ng-click="save()" ng-disabled="frmAdmin.$invalid">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



