<!-- resources/views/admins/index.blade.php -->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div  class="col-sm-6 pull-left">
                            <div class="pull-left" style="margin-right:20px"><h2 id="page-header"><% headerTitle %></h2></div>
                            <div class="loading-loader pull-left" style="margin-top:30px" >Loading....</div>
                            <div class="error-loading-container pull-left" style="margin-top:30px">Error loading. Please reload.</div>
                        </div>
                        <div class="col-sm-6 pull-right">
                            <div class="pull-right" style="padding-top:18px">
                                <button id="btn-add" class="btn-custom btn-success-custom" ng-click="pageChanged(currentPage)"> Refresh </button>
                                <button class="btn-custom btn-success-custom" ng-click="toggle('add', '0')" style="margin-bottom:0px">New Admin</button>
                            </div>
                            <div class="pull-right box-tools" style="display:inline-table; padding-top:20px; margin-right:7px">
                                <div class="input-group">
                                    <input type="text" class="form-control ng-valid ng-dirty" placeholder="Search" ng-keyup="$event.keyCode == 13 ? searchDB() : ''" ng-model="searchText" name="table_search" title="" tooltip="" data-original-title="Min character length is 3">
                                    <span class="input-group-addon">Search</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div>
                        <!-- Table-to-load-the-data Part -->
                        <table class="table table-bordered pagin-table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th width="135px">Options</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <!--<tr ng-repeat="admin in admins.data"> -->

                            <tr dir-paginate="admin in admins | itemsPerPage:10" total-items="totalItems">
                                <td><% admin.id %></td>
                                <td><% admin.first_name %> <% admin.last_name %></td>
                                <td><% admin.user.username %></td>
                                <td><% admin.user.email %></td>
                                <td>
                                    <button class="btn-custom btn-sm btn-outline btn-outline-default" ng-click="toggle('edit', admin.id)">Edit</button>
                                    <button class="btn-custom btn-sm btn-danger-custom" ng-click="confirmDelete(admin.id)">Delete</button>
                                </td> <td>
                                    <button class="btn-custom btn-sm btn-success-custom" ng-click="confirmDeactivate(admin.id, admin.user.isActive)" ng-if="admin.user.isActive">Active</button>
                                    <button class="btn-custom btn-sm btn-danger-custom" ng-click="confirmDeactivate(admin.id, admin.user.isActive )" ng-if="!admin.user.isActive">Deactivated</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <dir-pagination-controls on-page-change="pageChanged(newPageNumber)" template-url="../resources/views/templates/dirPagination.html" > </dir-pagination-controls>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
