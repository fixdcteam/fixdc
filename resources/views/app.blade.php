<!DOCTYPE html>
<html lang="en" ng-app="fixDCApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>FixDC</title>

    </head>

<link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?= asset('css/bootstrap.css') ?>">
<link rel="stylesheet" href="<?= asset('css/style.css') ?>">
<link rel="stylesheet" href="<?= asset('css/angular-bootstrap-lightbox.css') ?>">
<link rel="stylesheet" href="<?= asset('css/angular-ui-notification.min.css') ?>">
<link rel="stylesheet" href="<?= asset('css/font-open-sans.css') ?>">

<!-- Application Dependencies -->

<script src="<?= asset('app/lib/angular/angular.js') ?>"></script>
<script src="<?= asset('app/lib/packages/lodash.js') ?>"></script>
<script src="<?= asset('app/lib/angular/angular-simple-logger.js') ?>"></script>

<script src="<?= asset('app/lib/packages/dirPagination.js') ?>"></script>
<script src="<?= asset('app/lib/packages/satellizer.js') ?>"></script>
<!--<script src="<?= asset('app/lib/packages/fusioncharts.js') ?>"></script> -->
<script type="text/javascript" src="http://static.fusioncharts.com/code/latest/fusioncharts.js"></script>
<script type="text/javascript" src="http://static.fusioncharts.com/code/latest/themes/fusioncharts.theme.ocean.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDf231hHSTohz2KFcbG-AwMKt7HEthQhEg"></script>

<script src="<?= asset('app/lib/angular/angular-animate.min.js') ?>"></script>
<script src="<?= asset('app/lib/angular/angular-route.min.js') ?>"></script>
<script src="<?= asset('app/lib/angular/angular-ui-notification.min.js') ?>"></script>
<script src="<?= asset('app/lib/angular/angular-ui-router.min.js') ?>"></script>
<script src="<?= asset('app/lib/angular/ui-bootstrap-tpls-0.12.0.min.js') ?>"></script>
<script src="<?= asset('app/lib/angular/angular-bootstrap-lightbox.js') ?>"></script>
<script src="<?= asset('app/lib/angular/angular-google-maps.min.js') ?>"></script>
<script src="<?= asset('app/lib/angular/angular-fusioncharts.min.js') ?>"></script>

<script src="<?= asset('js/jquery.min.js') ?>"></script>
<script src="<?= asset('js/bootstrap.min.js') ?>"></script>

<!-- Application Scripts -->
<script src="<?= asset('app/app.js') ?>"></script>
<script src="<?= asset('app/route.js') ?>"></script>
<script src="<?= asset('app/controllers/employees.js') ?>"></script>
<script src="<?= asset('app/controllers/citizens.js') ?>"></script>
<script src="<?= asset('app/controllers/auth.js') ?>"></script>
<script src="<?= asset('app/controllers/user.js') ?>"></script>
<script src="<?= asset('app/controllers/admin.js') ?>"></script>
<script src="<?= asset('app/controllers/agents.js') ?>"></script>
<script src="<?= asset('app/controllers/cases.js') ?>"></script>
<script src="<?= asset('app/controllers/agencies.js') ?>"></script>

<script src="<?= asset('app/services.js') ?>"></script>

<style type="text/css">

	.angular-google-map-container { height: 400px; }

    
	
    .navbar {
        background-color: #ee9644;
        //background-image: linear-gradient(to bottom, #337ab7, #337ab7, white);
        background-repeat: repeat-x;
        margin-bottom: 0px;
    }

    .dropdown-menu {
        background-clip: padding-box;
        background-color: #ee9644;
        border: 1px solid rgba(0, 0, 0, 0.2);
        border-radius: 6px 6px 6px 6px;
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        display: none;
        float: left;
        left: 0;
        list-style: none outside none;
        margin: 2px 0 0;
        min-width: 160px;
        padding: 5px 0;
        position: absolute;
        top: 100%;
        z-index: 1000;
    }

    .btn-group.open .btn.dropdown-toggle {
        background-color: red;
    }

    .btn-group.open .btn.dropdown-toggle {
        background-color:lime;
    }

    .navbar .nav li.dropdown.open > .dropdown-toggle,
    .navbar .nav li.dropdown.active > .dropdown-toggle,
    .navbar .nav li.dropdown.open.active > .dropdown-toggle {
        color:white;
        background-color: #bb6311;
    }

    .navbar .nav li.active > a {
        color: #fff;
        float: none;
        text-decoration: none;
        text-shadow: 0 0px 0 #ffffff;
        background-color: #bb6311;
        //background-image: linear-gradient(to bottom, green, green, white);
    }

    .navbar .nav > li > a {
        color: #fff;
        float: none;
        text-decoration: none;
        text-shadow: 0 0px 0 #ffffff;
    }

    .navbar .brand {
        display: block;
        float: left;
        padding: 10px 20px 10px;
        margin-left: -20px;
        font-size: 20px;
        font-weight: 200;
        color: white;
        text-shadow: 0 0px 0 #ffffff;
    }

    .navbar .nav > li > a:focus,
    .navbar .nav > li > a:hover {
        color: white;
        text-decoration: none;
        background-color: transparent;
        background-color: #bb6311;

        //background-image: linear-gradient(to bottom, green, green, white);
    }

    .navbar .nav > li.active > a:focus,
    .navbar .nav > li.active > a:hover {
        color: white;
        text-decoration: none;
        background-color: transparent;
        background-color: #bb6311;
        //background-image: linear-gradient(to bottom, green, green, white);
    }

    .navbar-text {
        margin-bottom: 0;
        line-height: 40px;
        color: white;
    }

    .dropdown-menu li > a {
        display: block;
        padding: 3px 20px;
        clear: both;
        font-weight: normal;
        line-height: 20px;
        color: white;
        white-space: nowrap;
    }

    .navbar-link {
        color: white;
        background-color: #bb6311;
    }

    .navbar-link:hover {
        color: white;
    }


</style>

<body id="bootstrap-overrides-body" ng-controller="AuthController as auth" style="background-color: #f5f5f5; ">
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('../') }}" style="padding-top: 10px;">
                <img src="img/logo-inline-name/white-300px.png" style="max-height: 30px;" />
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav" ng-if="authenticated">
                <li ng-class="{ active: auth.isActive('/home')}"><a href="{{ url('/#/home') }}">Home</a></li>
                <li ng-class="{ active: auth.isActive('/cases/list')}"><a href="{{ url('/#/cases/list') }}">Report Cases</a></li>
                <li ng-class="{ active: auth.isActive('/citizens/list')}"><a href="{{ url('/#/citizens/list') }}" ng-if="isAdminUser == 1">Citizens</a></li>
                <li ng-class="{ active: auth.isActive('/agents/list')}"><a href="{{ url('/#/agents/list') }}" ng-if="isAdminUser == 1">Agents</a></li>
                <li ng-class="{ active: auth.isActive('/agents/list')}"><a href="{{ url('/#/agents/list') }}" ng-if="isAgentAdmin == 1">Agents</a></li>
                <li ng-class="{ active: auth.isActive('/agencies/list')}"><a href="{{ url('/#/agencies/list') }}" ng-if="isAdminUser == 1">Agencies</a></li>
                <li ng-class="{ active: auth.isActive('/admins/list')}"><a href="{{ url('/#/admins/list') }}" ng-if="isAdminUser == 1">Admins</a></li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                    <li><a href="{{ url('/#/auth') }}" ng-if="!authenticated">Login</a></li>

                    <li class="dropdown" ng-if="authenticated">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                              <% adminUser %> <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/#/admins/profile') }}" ng-if="isAdminUser == 1"><i ></i>My Profile</a></li>
                            <li><a href="{{ url('/#/agents/profile') }}" ng-if="isAdminUser != 1"><i ></i>My Profile</a></li>
                            <li><a href="{{ url('/#/agencies/profile') }}" ng-if="isAgentAdmin == 1"><i ></i>Agency Profile</a></li>
							<li><a href="{{ url('/#/account-settings') }}"><i ></i>Account Settings</a></li>
                            <li><a href="" ng-click="auth.logout()"><i ></i>Logout</a></li>
                        </ul>
                    </li>
            </ul>
        </div>
    </div>
</nav>

<div data-ng-view="" id="ng-view" class="slide-animation" ng-animate="{enter: 'enter-animation', leave: 'leave-animation'}"> </div>

<div class="container" style="padding: 0px;">
    <div ui-view class="slide-animation" ng-animate="{enter: 'enter-animation', leave: 'leave-animation'}"></div>
</div>


</body>
</html>
