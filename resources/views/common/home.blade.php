
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Administration Area for FixDC</h2></div>

                <div class="panel-body">
                    <br />
					<div class="row">

						<div class="col-lg-3 col-xs-6">
							<div class="small-box bg-aqua">
								<div class="inner">
									<h3><%  unResolvedCases %></h3>
									<p>Total Unresolved Cases</p>
								</div>
							<div class="icon">
								<i class="ion ion-bag"></i>
							</div>
							<a href="#" class="small-box-footer" ng-click="go('/cases/list')">More info <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div><!-- ./col -->

						<div class="col-lg-3 col-xs-6">
							<div class="small-box bg-green">
							<div class="inner">
							  <h3><%  caseStatus[1].case_count %></h3>
							  <p>Total Resolved Cases</p>
							</div>
							<div class="icon">
							  <i class="ion ion-stats-bars"></i>
							</div>
							<a href="#" class="small-box-footer" ng-click="go('/cases/list')">More info <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div><!-- ./col -->


						<div class="col-lg-3 col-xs-6">
						  <!-- small box -->
							<div class="small-box bg-yellow">
							<div class="inner">
							  <h3><%  caseTotal[0].case_count %></h3>
							  <p>Total Cases</p>
							</div>
							<div class="icon">
							  <i class="ion ion-person-add"></i>
							</div>
							<a href="#" class="small-box-footer" ng-click="go('/cases/list')">More info <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div><!-- ./col -->


						<div class="col-lg-3 col-xs-6">
						  <!-- small box -->
							<div class="small-box bg-red">
							<div class="inner">
							  <h3><%  citizenTotal[0].citizen_count %></h3>
							  <p>Total Citizens</p>
							</div>
							<div class="icon">
							  <i class="ion ion-pie-graph"></i>
							</div>
							<a href="#" class="small-box-footer" ng-click="go('/citizens/list')" ng-if="isAdminUser == 1">More info <i class="fa fa-arrow-circle-right"></i></a>
							<a href="#" class="small-box-footer" ng-if="isAdminUser != 1"> No Info Available <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div><!-- ./col -->


					</div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Charts</div>

                <div class="panel-body">
                    <br />
					<div class="row">
						<div class="col-lg-7 col-xs-12">
							<fusioncharts 
							    width=100%
							    height="400"
							    type="line"
							    datasource="<% latestReportCount %>"
						    ></fusioncharts>
						</div>
						<div class="col-lg-5 col-xs-12">
							<div class="row">
								<fusioncharts 
								    width=100% 
								    height="200"
								    type="line"
								    datasource="<% latestCitizenSignups %>"
							    ></fusioncharts>
							</div>
							<div class="row">
								<fusioncharts 
								    width=100% 
								    height="200"
								    type="line"
								    datasource="<% latestUsersSignup %>"
							    ></fusioncharts>
							</div>
							
						</div>




					</div>
                </div>
            </div>
        </div>
    </div>
</div>




<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Report Cases</div>
                <div class="panel-body">
                    <div>
                        <table class="table table-bordered pagin-table">
                            <thead>
                            <tr>
                                <th>Category</th>
                                <th>Resolved</th>
                                <th>Unresolved</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="cat in reportIssues"> 
                                <td><% cat.cat_name %></td>
                                <td><% cat.resolved %></td>
                                <td><% cat.unresolved %></td>
                                <td><% cat.total %></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
					
                </div>
            </div>
        </div>
		
		<div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Recently Registered Citizens</div>
                <div class="panel-body">
                    <div>
                        <table class="table table-bordered pagin-table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="rc in recentCitizens"> 
                                <td><% rc.citizen_firstName %> <% rc.citizen_lastName %></td>
                                <td><% rc.email %> </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
					
                </div>
            </div>
        </div>
    </div>
</div>



<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Map</div>

                <div class="panel-body">
					<div class="row"  >
						<ui-gmap-google-map center="map.center" zoom="map.zoom" > 
							<ui-gmap-window coords="map.options.markers.selected.coords" show="windowOptions.show" options="windowOptions" closeClick="closeClick()">
				                <div>Hello</div>
				            </ui-gmap-window>
							<ui-gmap-markers models="markers" coords="'coords'" options="'options'" events="'events'" idkey="'id'" type="'cluster'" typeOptions="map.options.cluster">
							</ui-gmap-markers>
						</ui-gmap-google-map>




					</div>
                </div>
            </div>
        </div>
    </div>
</div>
