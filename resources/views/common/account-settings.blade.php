<!-- resources/views/admins/account-settings.blade.php -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 id="page-header">Account Settings</h2>
                </div>
                <div class="panel-body">
                    <div style="max-width:700px">
                        <h4>Change Password</h4>
                        <form name="frmChangePassword" class="form-horizontal" novalidate="">

                            <div class="form-group error">
                                <label for="First Name" class="col-sm-3 control-label">Current Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control has-error" id="current_password" name="current_password" placeholder="password"
                                               ng-model="user.current_password" ng-required="true">
                                    <span class="help-inline" ng-show="frmChangePassword.current_password.$invalid && frmChangePassword.current_password.$touched">Please enter you current password.</span>
                                </div>
                            </div>

                            <div class="form-group error">
                                <label for="Last Name" class="col-sm-3 control-label">New Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control has-error" id="new_password" name="new_password" placeholder="password"
                                           ng-model="user.password" ng-required="true">
                                    <span class="help-inline" ng-show="frmChangePassword.new_password.$invalid && frmAdmin.new_password.$touched">Please enter your new password.</span>
                                </div>
                            </div>

                            <div class="form-group error">
                                <label for="Last Name" class="col-sm-3 control-label">Confirm New Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control has-error" id="confirm_password" name="confirm_password" placeholder="password"
                                           ng-model="user.confirm_password" ng-required="true">
                                    <span class="help-inline" ng-show="frmChangePassword.confirm_password.$invalid && frmChangePassword.confirm_password.$touched">Please re-enter your new password for confirmation.</span>
                                </div>
                            </div>

                        </form>


                        <div class="modal-footer">
                            <button type="button" class="btn-custom btn-info-custom" id="btn-save" ng-click="updatePassword()" ng-disabled="frmChangePassword.$invalid">Save changes</button>
                        </div>

                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        <h4 class="modal-title" id="myModalLabel"><% modal_title %></h4>
                                    </div>
                                    <div class="modal-body">
                                        <% modal_content %>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>