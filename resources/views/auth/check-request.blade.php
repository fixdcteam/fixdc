<div class="col-sm-4 col-sm-offset-4">
    <div class="well">
        <h3>Checking token</h3>
        <span ng-if="auth.loginError"><% auth.loginErrorText %></span>
    </div>
</div>