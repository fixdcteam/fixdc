<div class="col-sm-4 col-sm-offset-4">
    <div class="well">
        <h3>Forgot password</h3>
        <p>Enter your email address below to reset your password. We will send you a link via email that will allow you to update your password within the next few minutes. Don't forget to check your junk mail too.</p>
        <span ng-if="auth.loginError"><% auth.loginErrorText %></span>
        <form>
            <div class="form-group">
                <input type="email" class="form-control" placeholder="Email" ng-model="auth.email">
            </div>
            <button class="btn btn-primary" ng-click="auth.forgotPassword(0)">Reset Password</button>
        </form>
    </div>
</div>