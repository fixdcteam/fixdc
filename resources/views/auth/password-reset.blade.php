<div class="col-sm-4 col-sm-offset-4">
    <div class="well">
        <h3>Password Reset</h3>
        <span ng-if="auth.loginError"><% auth.loginErrorText %></span>
        <form>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Enter your new password" ng-model="auth.resetCredentials.password">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Retype password" ng-model="auth.resetCredentials.retypePassword">
            </div>
            <button class="btn btn-primary" ng-click="auth.resetPassword()">Submit</button>

        </form>
    </div>
</div>