<div class="col-sm-4 col-sm-offset-4">
    <div class="well">
        <h3>Please check your email</h3>
        <p>We sent you a link on the email to update your password.</p>
        <span ng-if="auth.loginError"><% auth.loginErrorText %></span>
    </div>
</div>