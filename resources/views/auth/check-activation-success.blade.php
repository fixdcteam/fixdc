<div class="col-sm-4 col-sm-offset-4">
    <div class="well">
        <h3>Success</h3>
        <p>Your account is now activated. You can log in on the FixDC Mobile app.</p>
        <span ng-if="auth.loginError"><% auth.loginErrorText %></span>
    </div>
</div>