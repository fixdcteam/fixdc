<img src="img/logo-inline-name/orange-300px.png" style="max-height: 100px; margin:20px auto 0px auto; display:block;" />
<div class="col-sm-4 col-sm-offset-4"> <br />
    <div class="well">
        <h3>Log In</h3>
        <span ng-if="auth.loginError" style="color:red;"><% auth.loginErrorText %></span>
        <form>
            <div class="form-group">
                <input type="email" class="form-control" placeholder="Email" ng-model="auth.email">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Password" ng-model="auth.password">
            </div>
            <button class="btn btn-primary" ng-click="auth.login()">Log In</button> &nbsp;
            <a href="#" ng-click="auth.go('/auth/forgot-password')">Reset Password</a>
        </form>
    </div>
</div>