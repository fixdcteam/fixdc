<div class="col-sm-4 col-sm-offset-4">
    <div class="well">
        <h3>Your Account was deactivated</h3>
        <p>Please check your email.</p>
        <p>We sent you a link on the email to update your password.</p>
        <span ng-if="auth.loginError"><% auth.loginErrorText %></span>
    </div>
</div>