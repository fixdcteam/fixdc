<!-- resources/views/agencies/edit-agency.blade.php -->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="loading-loader pull-right">Loading...</div>
                    <div class="error-loading-container pull-right">Error loading. Please reload.</div>
                    <h2 id="page-header">
                        <% form_title %>Agency Profile
                    </h2>
                </div>
                <div class="panel-body">
                    <div style="max-width:700px">
                        <form name="frmAgency" class="form-horizontal" novalidate="">

                            <div class="form-group error">
                                <label for="name" class="col-sm-3 control-label">Agency Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control has-error" id="name" name="name" placeholder="Agency Name" value="<% agency_name %>"
                                           ng-model="agency.agency_name" ng-required="true">
                                    <span class="help-inline" ng-show="frmAgency.name.$invalid && frmAgency.name.$touched">Please enter the agency name (required).</span>
                                </div>
                            </div>

                            <div class="form-group error">
                                <label for="address" class="col-sm-3 control-label">Address</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control has-error" id="address" name="address" placeholder="Address" value="<% agency_address %>"
                                           ng-model="agency.agency_address" ng-required="true">
                                    <span class="help-inline" ng-show="frmAgency.address.$invalid && frmAgency.address.$touched">Please enter the full address</span>
                                </div>
                            </div>

                            <div class="form-group error">
                                <label for="telno1" class="col-sm-3 control-label">Contact Number</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control has-error" id="telno1" name="telno1" placeholder="Contact Number" value="<% agency_telNo1 %>"
                                           ng-model="agency.agency_telNo1" ng-pattern="/^[0-9,-]{7,11}$/" ng-required="true">
                                    <span class="help-inline" ng-show="frmAgency.telno1.$invalid && frmAgency.telno1.$touched">Please enter a valid Contact Number</span>
                                </div>
                            </div>

                            <div class="form-group error">
                                <label for="telno2" class="col-sm-3 control-label">Alternative Contact</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control has-error" id="telno2" name="telno2" placeholder="Alternative Contact" value="<% agency_telNo2 %>"
                                           ng-model="agency.agency_telNo2" ng-pattern="/^[0-9,-]{7,11}$/"  >
                                    <span class="help-inline" ng-show="frmAgency.telno2.$invalid && frmAgency.telno2.$touched">Enter a valid Contact Number</span>
                                </div>
                            </div>

                            <div class="form-group error">
                                <label for="email" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control has-error" id="email" name="email" placeholder="Email" value="<% agency_email %>"
                                           ng-model="agency.agency_email" ng-required="true">
                                    <span class="help-inline" ng-show="frmAgency.email.$invalid && frmAgency.email.$touched">Please enter a valid email address.</span>
                                </div>
                            </div>

                        </form>


                        <div class="modal-footer">
                            <button type="button" class="btn-custom btn-info-custom" id="btn-save" ng-click="save()" ng-disabled="frmAgency.$invalid">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
