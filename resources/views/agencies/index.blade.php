<!-- resources/views/agencies/index.blade.php -->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div  class="col-sm-6 pull-left">
                            <div class="pull-left" style="margin-right:20px"><h2 id="page-header"><% headerTitle %></h2></div>
                            <div class="loading-loader pull-left" style="margin-top:30px" >Loading....</div>
                            <div class="error-loading-container pull-left" style="margin-top:30px">Error loading. Please reload.</div>
                        </div>
                        <div class="col-sm-6 pull-right">
                            <div class="pull-right" style="padding-top:18px">
                                <button id="btn-add" class="btn-custom btn-success-custom " ng-click="pageChanged(currentPage)"> Refresh </button>
                                <button id="btn-add" class="btn-custom btn-success-custom " ng-click="toggle('add', '0')">New Agency</button>
                            </div>
                            <div class="pull-right box-tools" style="display:inline-table; padding-top:20px; margin-right:7px">
                                <div class="input-group">
                                    <input type="text" class="form-control ng-valid ng-dirty" placeholder="Search" ng-keyup="$event.keyCode == 13 ? searchDB() : ''" ng-model="searchText" name="table_search" title="" tooltip="" data-original-title="Min character length is 3">
                                    <span class="input-group-addon">Search</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div>
                        <table class="table table-bordered pagin-table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr dir-paginate="agency in agencies | itemsPerPage:10" total-items="totalItems">
                                <td><% agency.agency_name %> </td>
                                <td><% agency.agency_email %></td>
                                <td><% agency.agency_address %></td>
                                <td>
                                    <button class="btn-custom btn-sm btn-outline btn-outline-default" ng-click="toggle('edit', agency.agency_id)">Edit</button>
                                    <button class="btn-custom btn-sm btn-danger-custom" ng-click="confirmDelete(agency.agency_id)">Delete</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <dir-pagination-controls on-page-change="pageChanged(newPageNumber)" template-url="../resources/views/templates/dirPagination.html" > </dir-pagination-controls>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
