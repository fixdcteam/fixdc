<!-- resources/views/employees/add-employee.blade.php -->

<h2><button id="btn-back" class="btn btn-primary btn-xs" ng-click="go('/employees/fufu')"> < Employee List </button> Add New Employee</h2>

<form name="frmEmployees" class="form-horizontal" novalidate="">

    <div class="form-group error">
        <label for="inputEmail3" class="col-sm-3 control-label">Name</label>
        <div class="col-sm-9">
            <input type="text" class="form-control has-error" id="name" name="name" placeholder="Fullname" value="<%name%>"
                   ng-model="employee.name" ng-required="true">
                                        <span class="help-inline"
                                              ng-show="frmEmployees.name.$invalid && frmEmployees.name.$touched">Name field is required</span>
        </div>
    </div>

    <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
        <div class="col-sm-9">
            <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="<%email%>"
                   ng-model="employee.email" ng-required="true">
                                        <span class="help-inline"
                                              ng-show="frmEmployees.email.$invalid && frmEmployees.email.$touched">Valid Email field is required</span>
        </div>
    </div>

    <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Contact Number</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="contact_number" name="contact_number" placeholder="Contact Number" value="<%contact_number%>"
                   ng-model="employee.contact_number" ng-required="true">
                                    <span class="help-inline"
                                          ng-show="frmEmployees.contact_number.$invalid && frmEmployees.contact_number.$touched">Contact number field is required</span>
        </div>
    </div>

    <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Position</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="position" name="position" placeholder="Position" value="<%position%>"
                   ng-model="employee.position" ng-required="true">
                                    <span class="help-inline"
                                          ng-show="frmEmployees.position.$invalid && frmEmployees.position.$touched">Position field is required</span>
        </div>
    </div>

</form>

<div class="modal-footer">
    <button type="button" class="btn btn-primary" id="btn-save" ng-click="save(modalstate, id)" ng-disabled="frmEmployees.$invalid">Save changes</button>
</div>