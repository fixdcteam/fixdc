<h3>Hi {{ $__admin->first_name }} {{ $__admin->last_name }},</h3>

<p>You have been added as an admin of FixDC. Please visit <a href="{{ url($__app_url) }}" >{{ $__app_url }}</a> to log in using the following credentials</p>
<br />
<p>Email: {{ $__email }}</p>
<p>Password: 123456</p>
<br />
<p>Please change your password immediately.</p>
<br />
<p>The FixDC Team</p>