<h2>FixDC admin sent you a note.</h2>

<p>Hi {{ $citizen->citizen_firstName }}, </p>
<br />
<p>This is a reminder note from the Admin of FixDC that you had already posted multiple ambiguous reports. </p>
<br />
<p>If we ever receive an ambigous report from you again, we will be blocking your account. </p>
<br />
<p>Please contact any agency admin to this may be a mistake. </p>
<br />
<p>The FixDC Team</p>