<h1>Hi {{ $__citizen->citizen_firstName }} {{ $__citizen->citizen_lastName }},</h1>

<p>Thank you for creating account in FixDC. </p>
<br />
<p>Your account is not yet activated. You can't access the app the next time you log in. You need to activate your account. </p>
<br />
<p><b>Please click this link to activate your account.: </b> <a href="{{ $__app_url }}" >{{ $__app_url }} </a>  </p>
<br />
<p>You can also copy the link an paste it on your browser if link does not work. </p>
<br />
<p>The FixDC Team</p>