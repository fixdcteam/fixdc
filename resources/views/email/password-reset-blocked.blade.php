<h2>Your account was deactivated</h2>

<p>Hi,</p>
<p>You recently tried to log in to your acccount 5 times with a wrong password. To protect your account, we had deactivated it.
But dont worry, you can reactivate your account by changing your password. 
</p>
<br />
<p><b>Please click this link to create a new password: </b> <a href="{{ $app_url }}" >{{ $app_url }} </a>  </p>
<br />
<p>You can also copy the link an paste it on your browser if link does not work. </p>
<br />

<p>The FixDC Team</p>