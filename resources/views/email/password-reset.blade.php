<h2>You requested your password be reset</h2>

<p>Hi,</p>
<p>You recently requested to have your password reset. </p>
<br />
<p><b>Please click this link to create a new password: </b> <a href="{{ $app_url }}" >{{ $app_url }} </a>  </p>
<br />
<p>You can also copy the link an paste it on your browser if link does not work. </p>
<br />
<p>Please ignore this email if you did not request the password reset </p>
<br />
<p>The FixDC Team</p>