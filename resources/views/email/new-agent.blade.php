<h3>Hi {{ $__agent->agent_firstName }} {{ $__agent->agent_lastName }},</h3>

<p>You have been added as an agent of FixDC. Please visit <a href="{{ url($__app_url) }}" >{{ $__app_url }}</a> to log in using the following credentials</p>
<br />
<p>Email: {{ $__email }}</p>
<p>Password: 123456</p>
<br />
<p>Please change your password immediately.</p>
<br />
<p>The FixDC Team</p>