<!-- resources/views/agents/index.blade.php -->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div  class="col-sm-6 pull-left">
                            <div class="pull-left" style="margin-right:20px"><h2 id="page-header">Agent List</h2></div>
                            <div class="loading-loader pull-left" style="margin-top:30px" >Loading....</div>
                            <div class="error-loading-container pull-left" style="margin-top:30px">Error loading. Please reload.</div>
                        </div>
                        <div class="col-sm-6 pull-right">
                            <div class="pull-right" style="padding-top:18px">
                                <button id="btn-add" class="btn-custom btn-success-custom " ng-click="pageChanged(currentPage)"> Refresh </button>
                                <button id="btn-add" class="btn-custom btn-success-custom " ng-click="toggle('add', '0')">New Agent > </button>
                            </div>
                            <div class="pull-right box-tools" style="display:inline-table; padding-top:20px; margin-right:7px">
                                <div class="input-group">
                                    <input type="text" class="form-control ng-valid ng-dirty" placeholder="Search" ng-keyup="$event.keyCode == 13 ? searchDB() : ''" ng-model="searchText" name="table_search" title="" tooltip="" data-original-title="Min character length is 3">
                                    <span class="input-group-addon">Search</span>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div>
                        <table class="table table-bordered pagin-table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Agency</th>
                                <th width="135px">Options</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr dir-paginate="agent in agents | itemsPerPage:10" total-items="totalItems">
                                <td><% agent.agent_firstName %> <% agent.agent_lastName %></td>
                                <td><% agent.user.email %></td>
                                <td><% agent.agency.agency_name %></td>
                                <td>
                                    <button class="btn-custom btn-sm btn-outline btn-outline-default" ng-click="toggle('edit', agent.agent_id)">Edit</button>
                                    <button class="btn-custom btn-sm btn-danger-custom" ng-click="confirmDelete(agent.agent_id)">Delete</button>
                                </td> <td>
                                    <button class="btn-custom btn-sm btn-success-custom " ng-click="makeAdmin(agent.agent_id, agent.agent_isAdmin)" ng-if="agent.agent_isAdmin">Agency Admin</button>
                                    <button class="btn-custom btn-sm btn-info-custom " ng-click="makeAdmin(agent.agent_id, agent.agent_isAdmin)" ng-if="!agent.agent_isAdmin">Agent</button>
                                    <button class="btn-custom btn-sm btn-success-custom" ng-click="confirmDeactivate(agent.agent_id, agent.user.isActive)" ng-if="agent.user.isActive">Active</button>
                                    <button class="btn-custom btn-sm btn-danger-custom" ng-click="confirmDeactivate(agent.agent_id, agent.user.isActive )" ng-if="!agent.user.isActive">Deactivated</button>

                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <dir-pagination-controls on-page-change="pageChanged(newPageNumber)" template-url="../resources/views/templates/dirPagination.html" > </dir-pagination-controls>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel"><% modal_title %></h4>
            </div>
            <div class="modal-body">
                <% modal_content %>
            </div>
            <div class="modal-footer">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>