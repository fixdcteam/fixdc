<!-- resources/views/agents/edit-agent.blade.php -->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="loading-loader pull-right">Loading...</div>
                    <div class="error-loading-container pull-right">Error loading. Please reload.</div>
                    <h2 id="page-header">
                        <a href="" id="btn-back" class="link" ng-click="go('/agents/list')"> <img src="img/back1600.png" > Agent List </a> 
                        <% form_title %>Update Agent Record
                    </h2>
                </div>
                <div class="panel-body">
                    <div style="max-width:700px">
                        <form name="frmAgent" class="form-horizontal" novalidate="">

                            <div class="form-group error">
                                <label for="First Name" class="col-sm-3 control-label">First Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control has-error" id="first_name" name="first_name" placeholder="First Name" value="<% agent_firstName %>"
                                           ng-model="agent.agent_firstName" ng-required="true">
                                    <span class="help-inline" ng-show="frmAgent.first_name.$invalid && frmAgent.first_name.$touched">Please enter the first name (required).</span>
                                </div>
                            </div>

                            <div class="form-group error">
                                <label for="Last Name" class="col-sm-3 control-label">Last Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control has-error" id="last_name" name="last_name" placeholder="Last Name" value="<% agent_lastName %>"
                                           ng-model="agent.agent_lastName" ng-required="true">
                                    <span class="help-inline" ng-show="frmAgent.last_name.$invalid && frmAgent.last_name.$touched">Please enter the last name (required).</span>
                                </div>
                            </div>

                            <div class="form-group error">
                                <label for="gender" class="col-sm-3 control-label">Gender</label>
                                <div class="col-sm-9">
                                    <label class="btn btn-default">
                                        <input name="gender" value="male" type="radio" ng-model="agent.agent_gender" >Male
                                    </label>
                                    <label class="btn btn-default">
                                        <input name="gender" value="female" type="radio" ng-model="agent.agent_gender">Female
                                    </label>

                                </div>
                            </div>

                            <div class="form-group error">
                                <label for="Contact Number" class="col-sm-3 control-label">Mobile Number</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control has-error" id="contact_number" name="contact_number" placeholder="Mobile Number" value="<% agent_cont1 %>"
                                           ng-model="agent.agent_cont1" ng-Maxlength=11 ng-MinLength=7 ng-pattern="/^09[0-9]{9}$/" ng-required="true">
                                    <span class="help-inline" ng-show="frmAgent.contact_number.$invalid && frmAgent.contact_number.$touched">Please enter a valid contact number.</span>
                                </div>
                            </div>

                            <div class="form-group error">
                                <label for="Alternative Contact" class="col-sm-3 control-label">Alternative Contact</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control has-error" id="alternative_contact" name="alternative_contact" placeholder="Alternative Contact" value="<% agent_cont2 %>"
                                           ng-model="agent.agent_cont2" ng-Maxlength=11 ng-MinLength=7 ng-pattern="/^[0-9,-]{7,11}$/" >
                                    <span class="help-inline" ng-show="frmAgent.alternative_contact.$invalid && frmAgent.alternative_contact.$touched">Please enter a valid contact number.</span>
                                </div>
                            </div>

                            <div class="form-group error">
                                <label for="agency" class="col-sm-3 control-label">Agency</label>
                                <div class="col-sm-9">
                                    <select class="form-control has-error" name="agency" id="agency" ng-model="agent.agency" ng-options="agency.agency_name for agency in agencyData.agencies track by agency.agency_id" ng-if="isAdminUser">
                                    </select>
                                    <span class="help-inline" ng-show="frmAgent.agency.$invalid && frmAgent.agency.$touched">Please select an agency.</span>
                                 </div>
                            </div>

                            <div class="form-group error">
                                <label for="Position" class="col-sm-3 control-label">Position</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control has-error" id="position" name="position" placeholder="Position Title" value="<% agent_posTitle %>"
                                           ng-model="agent.agent_posTitle">
                                    <span class="help-inline" ng-show="frmAgent.position.$invalid && frmAgent.position.$touched">Please enter the position (required).</span>
                                </div>
                            </div>

                            <div class="form-group error">
                                <label for="System Position" class="col-sm-3 control-label">System Position</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control has-error" id="system_position" name="system_position" placeholder="System Position" value="<% agent_sysPos %>"
                                           ng-model="agent.agent_sysPos">
                                    <span class="help-inline" ng-show="frmAgent.system_position.$invalid && frmAgent.system_position.$touched">Please enter the system position (required).</span>
                                </div>
                            </div>

                            <div class="form-group error">
                                <label for="Email" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control has-error" id="email" name="email" placeholder="Email" value="<% email %>"
                                           ng-model="agent.user.email" ng-required="true">
                                    <span class="help-inline" ng-show="frmAgent.email.$invalid && frmAgent.email.$touched">Please enter a valid email address.</span>
                                </div>
                            </div>

                            <!--<div class="form-group error">
                                <label for="password" class="col-sm-3 control-label">Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control has-error" id="password" name="password" placeholder="Password" value="<% password %>"
                                           ng-model="agent.user.password" ng-required="true">
                                    <span class="help-inline" ng-show="frmAgent.password.$invalid && frmAgent.password.$touched">Please enter a password (required).</span>
                                </div>
                            </div> -->

                        </form>

                        <div class="modal-footer">
                            <button type="button" class="btn-custom btn-info-custom" id="btn-save" ng-click="save()" ng-disabled="frmAgent.$invalid">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel"><% modal_title %></h4>
            </div>
            <div class="modal-body">
                <% modal_content %>
            </div>
            <div class="modal-footer">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>