<!-- resources/views/cases/index.blade.php -->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="loading-loader pull-right">Loading...</div>
                    <div class="error-loading-container pull-right">Error loading. Please reload.</div>
                    <h2 id="page-header"><% headerTitle %><button class="btn-custom btn-success-custom pull-right" ng-click="go('/cases/manage-categories')"> Manage Categories </button></h2>
                </div>
                <div class="panel-body">
				<span class="filter-label">Filter results: </span>
                    <div class="btn-group">
                        <label class="btn-custom btn-outline btn-outline-success" btn-radio="'&case_status=1'" ng-change="applyFilter()" ng-model="paramStatus"> Open (<% openReportsCount %>) </label>
                        <label class="btn-custom btn-outline btn-outline-success" btn-radio="'&case_status=4'" ng-change="applyFilter()" ng-model="paramStatus"> In Progress </label>
                        <label class="btn-custom btn-outline btn-outline-success" btn-radio="'&case_status=3'" ng-change="applyFilter()" ng-model="paramStatus"> Closed </label>
						<label class="btn-custom btn-outline btn-outline-success" btn-radio="'&case_status=2'" ng-change="applyFilter()" ng-model="paramStatus"> Invalid </label>
                        <label class="btn-custom btn-outline btn-outline-success" btn-radio="''" ng-change="applyFilter()" ng-model="paramStatus"> All </label>
                    </div>

                    <div class="btn-group">
                        <label class="btn-custom btn-outline btn-outline-primary" btn-radio="'&case_isPrivate=2'" ng-change="applyFilter()" ng-model="paramPrivacy"> Public </label>
                        <label class="btn-custom btn-outline btn-outline-primary" btn-radio="'&case_isPrivate=1'" ng-change="applyFilter()" ng-model="paramPrivacy"> Private </label>
                        <label class="btn-custom btn-outline btn-outline-primary" btn-radio="''" ng-change="applyFilter()" ng-model="paramPrivacy"> All </label>
                    </div>
					<div class="btn-group">
						<span class="filter-label">Category:  <select class="has-error" name="category" id="category" ng-model="catData.model" ng-change="applyFilter()" style="padding-top:7px; padding-bottom:7px;">
							<option value="" selected="selected">All</option>
							<option ng-repeat="cat in catData.categories" value="&cat_id=<% cat.cat_id %>"><% cat.cat_name %></option>
                        </select> </span>
					</div> 

                    <div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Report Cases</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr dir-paginate="case in cases | itemsPerPage:10" total-items="totalItems">
                                <td>
                                    <a href="" ng-click="toggle('edit', case.case_id)">
                                    <div class="row">
                                        <div class="col-md-6" >
                                            <strong><% case.citizen.citizen_firstName %> <% case.citizen.citizen_lastName %> <span ng-if="case.citizen.citizen_firstName == null"> A Concerned Citizen </span> </strong> reports
                                            <strong> <% case.cat.cat_name %>  </strong> at <strong> <% case.case_address %> </strong>
                                            <p class="text-muted">
                                                <span class="label btn-info-custom" ng-if="!case.case_isPrivate"> Shared with public</span>
                                                <span class="label bg-gray" ng-if="case.case_isPrivate"> Shared privately</span>
                                                <span class="label btn-success-custom" ng-if="case.case_status == 3"> RESOLVED</span>
                                                <span class="label btn-warning-custom" ng-if="case.case_status == 1"> CASE OPEN</span>
                                                <span class="label btn-royal-custom" ng-if="case.case_status == 4"> IN-PROGRESS</span>
												<span class="label btn-danger-custom" ng-if="case.case_status == 2"> INVALID</span> -
                                                <span ng-bind="dateString(case.case_datetime)"> </span> <% dateObject | date: "MMM d, y 'at' h:mm a" %>
                                            </p>
                                            <p><% case.case_name %></p>
                                            <p><% case.case_description %></p>

                                        </div>
                                        <div class="col-md-6">
                                            <img ng-src="<% case.images[0].img_directory %><% case.images[0].img_name %>.<% case.images[0].img_ext %>" height="100" />
                                        </div>
                                    </div>
                                    </a>
                                </td>

                            </tr>
                            </tbody>
                        </table>
                        <dir-pagination-controls on-page-change="pageChanged(newPageNumber)" template-url="../resources/views/templates/dirPagination.html" > </dir-pagination-controls>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>