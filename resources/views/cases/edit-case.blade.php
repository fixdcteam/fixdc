<!-- resources/views/cases/edit-case.blade.php -->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="loading-loader pull-right">Loading...</div>
                    <div class="error-loading-container pull-right">Error loading. Please reload.</div>
                    <!--<h2><button id="btn-back" class="" ng-click="go('/cases/list')"> < Report Cases </button> <% form_title %>Case Details</h2>-->
                    <h2 id="page-header">
                        <a href="" id="btn-back" class="link" ng-click="go('/cases/list')"> <img src="img/back1600.png" > FixDC Reports </a> 
                        <% form_title %>Case Details
                    </h2>
                </div>
                <div class="panel-body">
                    <div>
                        <strong> <% case.cat.cat_name %>  </strong> at <strong> <% case.case_address %> </strong>
                        <p class="text-muted">
                            <span class="label btn-info-custom" ng-if="!case.case_isPrivate"> Shared with public</span>
                            <span class="label bg-gray" ng-if="case.case_isPrivate"> Shared privately</span>
                            <span class="label btn-success-custom" ng-if="case.case_status == 3"> RESOLVED</span>
                            <span class="label btn-royal-custom" ng-if="case.case_status == 4"> IN-PROGRESS</span>
                            <span class="label btn-warning-custom" ng-if="case.case_status == 1"> CASE OPEN</span>
							<span class="label btn-danger-custom" ng-if="case.case_status ==2 "> INVALID</span> -
                            <span ng-bind="dateString(case.case_datetime)"> </span> <% dateObject | date: "MMM d, y 'at' h:mm a" %>
                        </p>
                    </div>

                        <button type="button" class="btn-custom btn-info-custom" ng-click="setStatus(case.case_id, '4')" ng-if="case.case_status == 1">Mark as in-Progress</button>
                        <button type="button" class="btn-custom btn-info-custom" ng-click="setStatus(case.case_id, '3')" ng-if="case.case_status == 4">Mark as resolved</button>
                        <button type="button" class="btn-custom btn-info-custom" ng-click="setStatus(case.case_id, '1')" ng-if="case.case_status != 1">Reopen case</button>
						<button type="button" class="btn-custom btn-danger-custom" ng-click="setStatus(case.case_id, '2')" ng-if="case.case_status == 1">Mark as invalid case</button>
                        <button type="button" class="btn-custom btn-info-custom" ng-click="setIsPrivate(case.case_id, case.case_isPrivate)" ng-if="!case.case_isPrivate">Make as private case</button>
                        <button type="button" class="btn-custom btn-info-custom" ng-click="setIsPrivate(case.case_id, case.case_isPrivate)" ng-if="case.case_isPrivate">Make as public case</button>
                        <input type="file" name="case-photo" id="case-photo" class="image-upload" required style="/*display:none;*/" ng-model="photo" onchange="angular.element(this).scope().uploadImage(this.files, angular.element(this).scope().case.case_id)" />
                        <label for="case-photo" class="btn-custom btn-info-custom">Upload Image</label>

                    <div class="row" style="margin-top:20px;">
                        <div class="col-sm-6">
                            <form name="frmCase" class="form-horizontal" novalidate="">
                                <div class="form-group error">
                                    <label for="category" class="col-sm-3 control-label">Category</label>
                                    <div class="col-sm-9">
                                        <select class="form-control has-error" name="category" id="category" ng-model="case.cat" ng-options="cat.cat_name for cat in categoryData.categories track by cat.cat_id"> 
                                        </select>
                                        <span class="help-inline" ng-show="frmCase.category.$invalid && frmCase.category.$touched">Category field is required</span>
                                    </div>
                                </div>

                                <div class="form-group error">
                                    <label for="name" class="col-sm-3 control-label">Subject</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control has-error" id="name" name="name" placeholder="Name" value="<% case_name %>"
                                               ng-model="case.case_name" ng-required="true">
                                        <span class="help-inline" ng-show="frmCase.name.$invalid && frmCase.name.$touched">Name field is required</span>
                                    </div>
                                </div>

                                <div class="form-group error">
                                    <label for="description" class="col-sm-3 control-label">Description</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control has-error" rows="5" id="description" name="description" placeholder="Description" value="<% case_description %>"
                                               ng-model="case.case_description" ng-required="true"></textarea>
                                        <span class="help-inline" ng-show="frmCase.description.$invalid && frmCase.description.$touched">Description field is required</span>
                                    </div>
                                </div>

                                <div class="form-group error">
                                    <label for="category" class="col-sm-3 control-label">Agency assigned </label>
                                    <div class="col-sm-9">
                                        <select class="form-control has-error" name="agency" id="agency" ng-model="case.agency" ng-options="agency.agency_name for agency in agencyData.agencies track by agency.agency_id" ng-disabled="<% agencyBoxDisabled %>"> 
                                        </select>
                                        <span class="help-inline" ng-show="frmCase.agency.$invalid && frmCase.agency.$touched">Category field is required</span>
                                    </div>
                                </div>

                                <div class="form-group error">
                                    <label for="note" class="col-sm-3 control-label">Note </label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control has-error" rows="5" id="note" name="note" placeholder="Note" value="<% case_adminNote %>"
                                               ng-model="case.case_adminNote" ng-required="true"></textarea>
                                        <span class="help-inline" ng-show="frmCase.note.$invalid && frmCase.note.$touched">Admin Note field is required</span>
                                    </div>
                                </div>

                                <div class="form-group error" style="display: none;">
                                    <label for="name" class="col-sm-3 control-label">citizen id</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" class="form-control has-error" id="citizen_id" name="citizen_id" placeholder="citizen" 
                                               ng-model="case.citizen_id" ng-required="true">
                                        <span class="help-inline" ng-show="frmCase.name.$invalid && frmCase.name.$touched">Name field is required</span>
                                    </div>
                                </div>

                            </form>
                            <div class="modal-footer">
                                <button type="button" class="btn-custom btn-info-custom" id="btn-save" ng-click="save()" ng-disabled="frmAgent.$invalid">Save changes</button>
                            </div>
                        </div>

                        <div class="col-sm-6">
							<div class="row">
								<div class="col-sm-12">
									<ul class="gallery gallery1">
										<li ng-repeat="image in case.images">
											<a href="" ng-click="openImageViewer($index)">
												<img ng-src="<% image.thumbUrl %>"  class="img-thumbnail" ><!--height="100" style="margin: 3px 3px 3px 3px;"/> -->
											</a>
										</li>
									</ul>
								</div>
								
								<div class="col-sm-12">
									<ui-gmap-google-map center="map.center" zoom="map.zoom"> 
										<ui-gmap-marker coords="marker.coords" options="marker.options" events="marker.events" idkey="marker.id">
										</ui-gmap-marker>
									</ui-gmap-google-map>
								</div>
							</div>
                            
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="imageviewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header" style="background-color: #bb6311; color: #fff; border-color: #bb6311">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff;"><span aria-hidden="true" >X</span></button>
        <h5 class="modal-title" id="myModalLabel"><% imageModalName %></h5>
    </div>
    <div class="modal-body">
        <div class=lightbox-image-container>
            <img ng-src="<% imageModalSrc %>" style="max-height: 600px; max-width: 900px;"/>
        </div>
    </div>
</div>