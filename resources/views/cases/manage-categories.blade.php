<!-- resources/views/admins/edit-admin.blade.php -->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="loading-loader pull-right">Loading...</div>
                    <div class="error-loading-container pull-right">Error loading. Please reload.</div>
                    <h2 id="page-header">Case Categories</h2>
                </div>
                <div class="panel-body">
                    <div style="max-width:700px">
                        <h4>Add New Category</h4>

                        <form name="frmNewCategory" class="form-horizontal" novalidate="">

                            <div class="form-group error">
                                <div class="col-sm-9">
                                    <input type="text" class="form-control has-error" id="cat_name" name="cat_name" placeholder="Category"
                                               ng-model="cat.cat_name" ng-required="true"> 
                                    <span class="help-inline" ng-show="frmNewCategory.cat_name.$invalid && frmNewCategory.cat_name.$touched">Please enter the category name.</span>
                                </div>
                                <button type="button" class="btn-custom btn-info-custom" id="btn-save" ng-click="saveCategory()" ng-disabled="frmNewCategory.$invalid">Save</button>
                            </div>

                        </form>

                        <div class="modal-footer">
                        </div>

                        <h4>Saved Categories</h4>

                        <div>
                        	<ul ng-repeat="category in categoryData.categories" style="list-style-type:none;font-size:15px;">
                        	<li><% category.cat_name %> 
                        		<button class="btn-custom btn-sm btn-outline btn-outline-default" ng-click="updateCategoryModal(category)">Edit</button>
                                <button class="btn-custom btn-sm btn-danger-custom" ng-click="confirmDeleteCategory(category.cat_id)" ng-hide="category.agency_id==0 && agencyID!=null">Delete</button>
                            </li>
                        	</ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel"><% modal_title %></h4>
            </div>
            <div class="modal-body">
                <% modal_content %>
            </div>
            <div class="modal-footer">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="updateCategoryModal" tabindex="-1" role="dialog" aria-labelledby="updateCategoryModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="updateCategoryModalLabel"><% modal_title %></h4>
            </div>
            <div class="modal-body">
            <form name="frmUpdateCategory" class="form-horizontal" novalidate="">
                <div class="form-group error">
                    <label for="category" class="col-sm-3 control-label">Name </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control has-error" id="cat_name" name="cat_name" placeholder="Category"
                                   ng-model="catForUpdate.cat_name" ng-required="true"> 
                        <span class="help-inline" ng-show="frmUpdateCategory.cat_name.$invalid && frmUpdateCategory.cat_name.$touched">Please enter the category name.</span>
                    </div>
                </div>

                <div class="form-group error">
                    <label for="category" class="col-sm-3 control-label">Agency assigned </label>
                    <div class="col-sm-8">
                        <select class="form-control has-error" name="agency" id="agency" ng-model="catForUpdate.agency" ng-options="agency.agency_name for agency in agencyData.agencies track by agency.agency_id" ng-disabled="<% agencyBoxDisabled %>"> 
                        </select>   
                        <span class="help-inline" ng-show="frmUpdateCategory.agency.$invalid && frmUpdateCategory.agency.$touched">Category field is required</span>
                    </div>
                </div>

            </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn-custom btn-info-custom" id="btn-save" ng-click="updateCategory()" ng-disabled="frmUpdateCategory.$invalid">Save</button>
                <button type="button" class="btn-custom btn-info-custom" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>