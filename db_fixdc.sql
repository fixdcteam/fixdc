-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 04, 2016 at 08:56 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_fixdc`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `user_id`, `first_name`, `last_name`, `deleted_at`) VALUES
(1, 1, 'Jacob', 'Cadelina', NULL),
(3, 5, 'Brezelle', 'Dizon', '2016-09-26 06:43:23'),
(4, 9, 'Jaysons', 'Santiago', NULL),
(5, 12, 'hahaha', 'katawa', NULL),
(6, 13, 'Vice', 'Ganda', NULL),
(7, 15, 'Vice1', 'Ganda', NULL),
(8, 29, 'sample2', 'sample2', NULL),
(9, 30, 'sample3', 'sample3', NULL),
(10, 31, 'Mail1', 'Mail1', NULL),
(11, 33, 'Mail2', 'Mail2', NULL),
(12, 35, 'Mail3', 'Mail3', NULL),
(13, 36, 'Mail4', 'Mail4', NULL),
(14, 38, 'maik5', 'mail5', NULL),
(15, 39, 'mail6', 'mail6', NULL),
(16, 40, 'mail7', 'mail7', NULL),
(17, 41, 'mail', '9', NULL),
(18, 42, 'mail', '10', NULL),
(19, 43, 'mail', '11', NULL),
(20, 51, 'mail', '12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `agency`
--

CREATE TABLE `agency` (
  `agency_id` int(11) NOT NULL,
  `agency_name` varchar(50) NOT NULL,
  `agency_address` varchar(150) NOT NULL,
  `agency_telNo1` varchar(10) NOT NULL,
  `agency_telNo2` varchar(10) DEFAULT NULL,
  `agency_email` varchar(150) NOT NULL,
  `agency_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agency`
--

INSERT INTO `agency` (`agency_id`, `agency_name`, `agency_address`, `agency_telNo1`, `agency_telNo2`, `agency_email`, `agency_deleted`) VALUES
(1, 'Lola Agency', 'Matina Crossing, Davao City', '123321', '1234567', 'lola_agency@gmail.com', 0),
(3, 'Kawatanss Agency', 'Ecolandss, Davao Cities', '12345678', '12345678', 'kawatan@agency.coms', 0),
(4, 'Wakswaks Agency', 'Bajada, Davao City', '1234567', '1234567', 'wakwak@agency.com', 0),
(5, 'CENRO West', 'Bangkal Davao City', '0908995699', '0955456565', 'doctor@agency.com', 0),
(6, 'Davao Light', 'C. Bangoy St., Davao City', '0908994599', '0908994599', 'jaccadz1@gmail.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `agency_contact`
--

CREATE TABLE `agency_contact` (
  `contact_id` int(11) NOT NULL,
  `agency_id` int(11) NOT NULL,
  `agent_no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `agent`
--

CREATE TABLE `agent` (
  `user_id` int(11) NOT NULL,
  `agency_id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  `agent_firstName` varchar(50) NOT NULL,
  `agent_lastName` varchar(50) NOT NULL,
  `agent_gender` varchar(6) NOT NULL,
  `agent_cont1` varchar(11) NOT NULL,
  `agent_cont2` varchar(11) NOT NULL,
  `agent_posTitle` varchar(50) NOT NULL,
  `agent_sysPos` varchar(50) NOT NULL,
  `agent_isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agent`
--

INSERT INTO `agent` (`user_id`, `agency_id`, `agent_id`, `agent_firstName`, `agent_lastName`, `agent_gender`, `agent_cont1`, `agent_cont2`, `agent_posTitle`, `agent_sysPos`, `agent_isAdmin`, `deleted_at`) VALUES
(18, 3, 1, 'Vice3', 'Ganda3', 'Male', '123456', '123456', 'manager', 'admin', 1, NULL),
(28, 4, 2, 'Langlang', 'Sibal', 'female', '09464563464', '09464563464', 'CEO', 'admin', 0, NULL),
(22, 1, 3, 'Vice6', 'Ganda6', 'Male', '123456', '123456', 'manager', 'admin', 0, NULL),
(50, 5, 5, 'Agent', 'x44', 'male', '09123456789', '09123456789', 'wa', 'wa', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `article_id` int(11) NOT NULL,
  `agency_id` int(11) NOT NULL,
  `article_title` varchar(50) NOT NULL,
  `article_type` varchar(50) NOT NULL,
  `article_content` varchar(50) NOT NULL,
  `article_banner` varchar(50) NOT NULL,
  `article_status` varchar(50) NOT NULL,
  `article_dateTimePub` datetime NOT NULL,
  `article_lastUpdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `case_category`
--

CREATE TABLE `case_category` (
  `cat_id` int(11) NOT NULL,
  `agency_id` int(11) NOT NULL,
  `cat_code` int(11) NOT NULL,
  `cat_name` varchar(50) NOT NULL,
  `cat_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `case_category`
--

INSERT INTO `case_category` (`cat_id`, `agency_id`, `cat_code`, `cat_name`, `cat_deleted`) VALUES
(1, 1, 0, 'Lubak yo ang dalan', 0),
(2, 2, 2, 'Guba sa poste', 0),
(3, 1, 1, 'Lubak sa dalan', 0),
(4, 3, 0, 'Tumba ang puno sa dalan', 0);

-- --------------------------------------------------------

--
-- Table structure for table `case_image`
--

CREATE TABLE `case_image` (
  `case_img_id` int(11) NOT NULL,
  `case_id` int(11) NOT NULL,
  `img_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `case_image`
--

INSERT INTO `case_image` (`case_img_id`, `case_id`, `img_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 4, 3),
(4, 4, 4),
(5, 4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `citizen`
--

CREATE TABLE `citizen` (
  `citizen_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `citizen_firstName` varchar(50) NOT NULL,
  `citizen_lastName` varchar(50) NOT NULL,
  `citizen_gender` varchar(6) NOT NULL,
  `citizen_DOB` date NOT NULL,
  `citizen_dateTimeReg` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `citizen_isBlocked` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `citizen`
--

INSERT INTO `citizen` (`citizen_id`, `user_id`, `citizen_firstName`, `citizen_lastName`, `citizen_gender`, `citizen_DOB`, `citizen_dateTimeReg`, `citizen_isBlocked`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 7, 'Jacobs', 'Cadelinas', 'male', '0000-00-00', '2016-07-30 15:22:48', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 8, 'Jayson', 'Santiago', 'male', '0000-00-00', '2016-07-30 15:25:56', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(3, 10, 'Gia Mae', 'Quijada', 'female', '0000-00-00', '2016-07-30 15:29:10', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-09-27 04:40:36'),
(4, 11, 'Marjorit', 'mendoza', 'female', '0000-00-00', '2016-07-30 15:29:55', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(5, 14, 'Vice ganda', 'Citizen', 'male', '2016-09-04', '2016-08-06 14:31:35', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(6, 53, 'Citizen2', 'patrol', 'male', '2016-03-09', '2016-09-05 23:21:34', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(7, 54, 'Citizen3', 'patrol', 'male', '2016-04-07', '2016-09-05 23:22:34', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `concerned_case`
--

CREATE TABLE `concerned_case` (
  `concern_id` int(11) NOT NULL,
  `citizen_id` int(11) NOT NULL,
  `case_no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `email`, `contact_number`, `position`, `created_at`, `updated_at`) VALUES
(1, 'Brezalissss', 'brezalis@gmail.com', '09123456789', 'CEO', '2016-07-17 08:05:25', '2016-07-20 16:19:09'),
(3, 'fds', 'fgdg@fxfgsd.com', '346546', 'dgdf', '2016-07-20 13:08:37', '2016-07-20 13:08:37'),
(7, 'gfd', 'gdf@gfd.com', '7765476456', 'dfgdf', '2016-07-20 15:18:51', '2016-07-20 15:18:51');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `img_id` int(11) NOT NULL,
  `img_directory` varchar(250) NOT NULL,
  `img_name` varchar(100) NOT NULL,
  `img_ext` varchar(5) NOT NULL,
  `img_size` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`img_id`, `img_directory`, `img_name`, `img_ext`, `img_size`) VALUES
(1, '../storage/app/public/img/', 'lubak_sa_dalan', 'jpg', 100),
(2, '../storage/app/public/img/', 'canal', 'jpg', 100),
(3, '../storage/app/public/img/', 'streetlights1', 'jpg', 1),
(4, '../storage/app/public/img/', 'streetlights2', 'jpg', 1),
(5, '../storage/app/public/img/', 'streetlights3', 'jpg', 2);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_06_27_143149_create_tasks_table', 1),
('2016_07_01_032037_create_employees_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `report_case`
--

CREATE TABLE `report_case` (
  `case_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `citizen_id` int(11) NOT NULL,
  `agency_id` int(11) NOT NULL,
  `case_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `case_description` varchar(260) NOT NULL,
  `case_name` varchar(150) NOT NULL,
  `case_address` varchar(250) NOT NULL,
  `case_loc_long` varchar(50) NOT NULL,
  `case_loc_lat` varchar(50) NOT NULL,
  `case_noOfConcerns` int(11) NOT NULL DEFAULT '0',
  `case_isPrivate` tinyint(1) NOT NULL DEFAULT '0',
  `case_status` tinyint(1) NOT NULL DEFAULT '1',
  `case_adminNote` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `report_case`
--

INSERT INTO `report_case` (`case_id`, `cat_id`, `citizen_id`, `agency_id`, `case_datetime`, `case_description`, `case_name`, `case_address`, `case_loc_long`, `case_loc_lat`, `case_noOfConcerns`, `case_isPrivate`, `case_status`, `case_adminNote`) VALUES
(1, 1, 1, 0, '2016-08-03 21:55:20', 'Wow ha dugay na kayu ni nga daot dri sa dalan lubak na kayu! ayuha na ni', 'Lubak sa dalan. hahaha', 'Matina, Davao City', '0.0000000', '0.0000000', 11, 0, 1, 'This is right.! alright'),
(2, 2, 2, 0, '2016-08-03 22:35:10', 'dfsh', 'fdsag', '', '', '', 2, 1, 0, 'gfdshsgf'),
(3, 4, 3, 5, '2016-08-24 23:27:31', 'pistot di kaagi akong ford.! nag atang ning puno sa dalan!', 'sabagal sa dalan!', 'Bajada, Davao City', '0.0000000', '0.0000000', 0, 0, 1, 'pls.'),
(4, 2, 3, 6, '2016-08-24 23:27:55', 'guba ang poste pistot', 'gdgf', 'Bajada, Davao City', '0.0000000', '0.0000000', 0, 1, 1, 'ayuhon pa ni boss');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('admin','citizen','agent','') COLLATE utf8_unicode_ci NOT NULL,
  `isActive` int(1) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `type`, `isActive`, `deleted`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'jacob.cadelina', 'jaccadz1@gmail.com', '$2y$10$DHotCZSVA9yTf96z5EiNJew/oIaLHFIjWpN1lu9DxgdL025CcZ38e', 'admin', 1, 0, 'A1JA2pCa6FesaCET66u9SVJrNJCBNch5Fl4Q2vhqlFbiC3AcfuECe5g6mTZm', '2016-07-17 08:04:59', '2016-10-01 05:42:06'),
(5, 'brezelle.dizon', 'wakwakss@gmail.com', '$2y$10$Rz6T/zJIy.N37dPpwhre2ORT2PDGCtOPaJesHwbNOmoezlqRLaMGe', 'admin', 1, 1, NULL, '2016-07-29 22:42:53', '2016-09-26 06:43:23'),
(7, 'jacob.cadelina', 'wahaha@gmail.com', '$2y$10$sINFekbYzSHKbfjUO8uzi.0BXG/5AUlRQgZNeo.1NBPDf64fy0YSm', 'citizen', 1, 0, NULL, '2016-07-29 23:22:48', '2016-09-01 07:42:39'),
(8, 'jayson.santiago', 'brezelle1@gmail.com', '$2y$10$16KfMqOkEXcmt7ObcI6ABOhgKtdTa4KZxhW4a0TWPX8DiTE/K5Zeq', 'citizen', 0, 0, NULL, '2016-07-29 23:25:56', '2016-08-13 19:52:04'),
(9, 'jayson.santiago', 'jayson1@gmail.com', '$2y$10$C7ttJhCFlEzjSAcK..f1Bu06SWcScOBeoaUDL.yVuEB9X/G6R5bJm', 'admin', 1, 0, NULL, '2016-07-29 23:26:57', '2016-07-30 03:13:24'),
(10, 'gia mae.quijada', 'gia@wakwak.com', '$2y$10$v/8ph32rX88jUT/5cjZFeuEhgweeGdWDg/WSvPTih.QR5xTh.0P/O', 'citizen', 1, 1, NULL, '2016-07-29 23:29:10', '2016-09-27 04:40:37'),
(11, 'marjorit.mendoza', 'mar@jorit.com', '$2y$10$Jk4xUOqYAZLSe8FBz.ksGu/Q9yjT42N8CYTD2UzNg0rAXvsrQSYr.', 'citizen', 1, 0, NULL, '2016-07-29 23:29:55', '2016-08-07 04:31:37'),
(12, 'hahaha.katawa', 'hahaha@katawa.com', '$2y$10$Dolhqfyhm4o6PeBqkkXmW.0a4TBwLqEb0seutUE5mtZ.i0qRXaRzu', 'admin', 1, 0, NULL, '2016-07-29 23:31:15', '2016-08-07 04:30:34'),
(13, 'vice.ganda', 'vice@ganda.com', '$2y$10$/TC65NV3wTa6hDsEmhBby.k7syHNRAfv3YtZX5F/EqJJcc7.YhyQu', 'admin', 1, 0, NULL, '2016-08-05 22:30:38', '2016-08-07 04:30:54'),
(14, 'vice ganda.citizen', 'vice@gandacitizen.com', '$2y$10$65K2mOCNhxg636sYjMRj3OSNvk80/v1ydkWjLKpG.KKfUOMpM7pfa', 'citizen', 0, 0, NULL, '2016-08-05 22:31:35', '2016-09-03 16:27:30'),
(15, 'vice1.ganda', 'vice@ganda1.com', '$2y$10$BapBssB/xsAYrbPZBl6uPeT04kQO5FpOuAspG2WvbNSL2ZYhcxbFS', 'admin', 0, 0, NULL, '2016-08-05 22:32:25', '2016-08-29 04:33:31'),
(16, 'vice.ganda', 'vice@ganda2.com', '$2y$10$2bCXtyRuAbiowtPGk2f0uekN0kVosmHLJOxIuuNT4EHEG6Tb5pu6G', 'agent', 0, 0, NULL, '2016-08-05 22:33:10', '2016-08-05 22:33:10'),
(18, '', 'vice3@gmail.com', '$2y$10$rLgqMtIzKTJdC/B0ivrCSec8ugandSR.g4bKrLhQfXcsxnx3oBBr6', 'agent', 1, 0, NULL, '2016-08-05 22:35:59', '2016-10-02 06:55:11'),
(20, 'vice5.ganda5', 'vice@ganda5.com', '$2y$10$T9UU4BnHsymk5N0v94uleeTFoH31GKK8jJq7BJFamnI7EDGKSWfzO', 'agent', 0, 0, NULL, '2016-08-05 22:46:45', '2016-08-05 22:46:45'),
(22, 'vice6.ganda6', 'vice@ganda6.com', '$2y$10$axysdYoRXmP3MyCpimiXF.HxqH/Zr477NGbTizmnWNpm4VaWqhxcS', 'agent', 0, 0, NULL, '2016-08-05 22:47:29', '2016-10-02 06:21:59'),
(28, 'langlang.sibal', 'langlang@gmail.com', '$2y$10$qZOEbgqvpZd33YGyLQDRGerThTsNTwvnwhY3FuPO3kvBRROguiGay', 'agent', 1, 0, NULL, '2016-08-11 15:06:18', '2016-08-11 15:06:18'),
(29, 'sample2.sample2', 'sample2@gmail.com', '$2y$10$ChKkUXauk.SlxdH612YCIeVmUIAovtkr7GUFGawOH/HhfMZbQpUXC', 'admin', 0, 0, NULL, '2016-08-27 19:28:04', '2016-08-27 19:28:04'),
(30, 'sample3.sample3', 'sample3@gmail.com', '$2y$10$9b1JlfCmGimYv7RxuyaMUupSa8hsPJMTCe.aW4eKhiGF6XEmDzCtC', 'admin', 0, 0, NULL, '2016-08-27 19:28:23', '2016-08-27 19:28:23'),
(31, 'mail1.mail1', 'mail1@gmail.com', '$2y$10$6IKwt0fIrXgYfRal53b.lOk9tU9yEoHjogzifDqjcv2CDJKVsA7lS', 'admin', 0, 0, NULL, '2016-09-05 05:49:56', '2016-09-05 05:49:56'),
(33, 'mail2.mail2', 'mail2@gmail.com', '$2y$10$Ux8h/Dy5ZL1IrwjpCsd8SO8mP/F8H7u3fp5YZZ1cw0FXsYPmKtZbe', 'admin', 0, 0, NULL, '2016-09-05 05:51:11', '2016-09-05 05:51:11'),
(35, 'mail3.mail3', 'mail3@gmail.com', '$2y$10$OtnKMb/oN0dqpkQvjLMIvelx/UoV/MsjgMzqV96KPcRpKKtpjOIn2', 'admin', 0, 0, NULL, '2016-09-05 06:00:04', '2016-09-05 06:00:04'),
(36, 'mail4.mail4', 'mail4@gmail.com', '$2y$10$KCpV0qcF1Wl0bzee7T8Rx.DqHbUTKQcbaPd/7QNR2sXfq/k3kkcku', 'admin', 0, 0, NULL, '2016-09-05 06:12:23', '2016-09-05 06:12:23'),
(38, 'maik5.mail5', 'mail5@gmail.com', '$2y$10$DosglTQjoxC/6arqyYEbG.t48KA3m3CRjNpBl1fOaKtDsnrCMcgr6', 'admin', 0, 0, NULL, '2016-09-05 06:14:34', '2016-09-05 06:14:34'),
(39, 'mail6.mail6', 'mail6@gmail.com', '$2y$10$p.7wvDzCdK.qsYUR8h0uVeAfoxJou70aSgbWoPENCoVEKOGVJrPmK', 'admin', 0, 0, NULL, '2016-09-05 06:19:18', '2016-09-05 06:19:18'),
(40, 'mail7.mail7', 'mail7@gmail.com', '$2y$10$TlfeBFRFYsUgi.xxmEwNvOeMu8vgKUzh2hsj90jjgw9/xOTHwWz3O', 'admin', 0, 0, NULL, '2016-09-05 06:27:17', '2016-09-05 06:27:17'),
(41, 'mail.9', 'mail9@gmail.com', '$2y$10$ac7Rhi8EbAi4bBmSnY9AuOJFpIfXWqdxaW/BvES7fSdQ/rzdUFazW', 'admin', 0, 0, NULL, '2016-09-05 06:31:30', '2016-09-05 06:31:30'),
(42, 'mail.10', 'mail10@gmail.com', '$2y$10$Z0olNFJL/LtLdGWv3w6DaedzD/dX6XgIZfFOV0GQWM3klLOHlsk..', 'admin', 0, 0, NULL, '2016-09-05 06:44:41', '2016-09-05 06:44:41'),
(43, 'mail.11', 'mail11@gmail.com', '$2y$10$3ln82vTmqwf3QzK0ZQ7OQufZmyHHzABp.pgXVXUt8VEm/jhseTeCS', 'admin', 0, 0, NULL, '2016-09-05 06:46:27', '2016-09-05 06:46:27'),
(44, 'agent.x44', 'agent@x44.com', '$2y$10$Rz6T/zJIy.N37dPpwhre2ORT2PDGCtOPaJesHwbNOmoezlqRLaMGe', 'agent', 1, 0, NULL, '2016-09-05 07:03:08', '2016-09-05 07:03:08'),
(45, 'agent.x44', 'agent@x442.com', '$2y$10$rrp7TCUjO42jXJoTwOyz7./AikTPQHPVR5W78etR..9A.LUs7ixem', 'agent', 1, 0, NULL, '2016-09-05 07:03:31', '2016-09-05 07:03:31'),
(47, 'agent.x44', 'agent@x443.com', '$2y$10$/IKfUhXUcb.puu8FIF2qJufQQLB4i8BGxthUddaLJFirM/M29ht8G', 'agent', 1, 0, NULL, '2016-09-05 07:04:14', '2016-09-05 07:04:14'),
(48, 'agent.x44', 'agent@x445.com', '$2y$10$6N1Jnha6g.kfyGXvitnuw.yCvjWYh/km4XWwKm6bTF7CD06R2d1jS', 'agent', 1, 0, NULL, '2016-09-05 07:06:52', '2016-09-05 07:06:52'),
(50, 'agent.x44', 'agent@x446.com', '$2y$10$VilEvjs7f3LtcCnk6lQdvuVoMb8gcLKZb9GQuOkmvDYFBCSuvqT4K', 'agent', 1, 0, NULL, '2016-09-05 07:08:25', '2016-10-02 05:55:26'),
(51, 'mail.12', 'mail@12.com', '$2y$10$ISeaim0OMAdAbks49CqbHeAJpU7DdRtk8Fx43qIAH6tQTmJv75xxC', 'admin', 0, 0, NULL, '2016-09-05 07:10:03', '2016-09-05 07:10:03'),
(52, 'citizen1.patrol', 'citizen1@patrol.com', '$2y$10$EqGHBvdlWW/xi2tvLXdQdeSGzIk5s3yzvg.AqDSBaB/nNo.iTVSp2', 'citizen', 0, 0, NULL, '2016-09-05 07:20:02', '2016-09-05 07:20:02'),
(53, 'citizen2.patrol', 'citizen2@patrol.com', '$2y$10$dSdPBX5lqu.Lv5UKsC5Mhe.o.pm3OJebSUrWsTCU7gvDS4alWRZJq', 'citizen', 1, 0, NULL, '2016-09-05 07:21:34', '2016-09-28 05:45:22'),
(54, 'citizen3.patrol', 'citizen3@patrol.com', '$2y$10$o81J3nyc/0Al4wVZpCo6QulKgY8oVBD9TKuNVtRtQTfXHvm9.Wtaa', 'citizen', 1, 0, NULL, '2016-09-05 07:22:34', '2016-09-05 07:22:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `agency`
--
ALTER TABLE `agency`
  ADD PRIMARY KEY (`agency_id`);

--
-- Indexes for table `agency_contact`
--
ALTER TABLE `agency_contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `agent`
--
ALTER TABLE `agent`
  ADD PRIMARY KEY (`agent_id`);

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`article_id`);

--
-- Indexes for table `case_category`
--
ALTER TABLE `case_category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `case_image`
--
ALTER TABLE `case_image`
  ADD PRIMARY KEY (`case_img_id`);

--
-- Indexes for table `citizen`
--
ALTER TABLE `citizen`
  ADD PRIMARY KEY (`citizen_id`);

--
-- Indexes for table `concerned_case`
--
ALTER TABLE `concerned_case`
  ADD PRIMARY KEY (`concern_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_name_unique` (`name`),
  ADD UNIQUE KEY `employees_email_unique` (`email`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`img_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `report_case`
--
ALTER TABLE `report_case`
  ADD PRIMARY KEY (`case_id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `agency`
--
ALTER TABLE `agency`
  MODIFY `agency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `agency_contact`
--
ALTER TABLE `agency_contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `article_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `case_category`
--
ALTER TABLE `case_category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `case_image`
--
ALTER TABLE `case_image`
  MODIFY `case_img_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `citizen`
--
ALTER TABLE `citizen`
  MODIFY `citizen_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `concerned_case`
--
ALTER TABLE `concerned_case`
  MODIFY `concern_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `img_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `report_case`
--
ALTER TABLE `report_case`
  MODIFY `case_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
